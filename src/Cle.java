
public class Cle extends Item
{

	//pour savoir quelle porte ouvre la cle
	private int typeCle;
	
	/**
	 * Constructeur de la classe Cle
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param _typeCle, pour connaitre le type de porte que l'on peut ouvrir avec cette cle
	 */
	public Cle(String nom, double x, double y, double width, double height, int _typeCle)
	{
		//Item : nom, positionX, positionY, largeur, hauteur, c'est un obstacle ?
		//quelle cle c'est ? , sa valeur marchande
		super(nom, x, y, width, height, false, Images.Entites.Cle.val(), 0);
		typeCle=_typeCle;
		genererTexteDescriptif();

	}
	
	/**
	 * cree un texte descriptif pour savoir quelle porte ouvre la clef
	 */
	public void genererTexteDescriptif()
	{
		texteDescriptif[0] = "Categorie : Clef";
		texteDescriptif[1] = "Permet de deverrouiller la porte ";
		if(typeCle==Maps.Map.Foret.val())
			texteDescriptif[1] += "de la  Foret ";
		else if(typeCle==Maps.Map.Chateau.val())
			texteDescriptif[1] += "du      Chateau ";
		else if(typeCle==Maps.Map.Marais.val())
			texteDescriptif[1] += "des Marais ";
		else if(typeCle==Maps.Map.Montagnes.val())
			texteDescriptif[1] += "des Monstagnes ";
		else if(typeCle==Maps.Map.Grange.val())
			texteDescriptif[1] += "de la     grange ";
		else if(typeCle==Maps.Map.Base.val())
			texteDescriptif[1]+= "du hub";		
	}
	
	public int getTypeCle()
	{
		return typeCle;
	}

}
