/**
 * Ceci est une classe permettant de retenir quelles touches sont appuyées, de maniere
 * a avoir un deplacement fluide et non saccade.
 */

import java.util.HashMap;
 
public class Keys
{
	public enum Touches
	{
		  a(65), b(66), c(67), d(68), e(69), f(70), g(71), h(72), i(73), j(74),
		  k(75), l(76), m(77), n(78), o(79), p(80), q(81), r(82), s(83), t(84),
		  u(85), v(86), w(87), x(88), y(89), z(90), returnKey(10), leftKey(37),
		  upKey(38), rightKey(39), downKey(40), majKey(16), escape(27);
		
	  private int valeur;
	  private Touches(int val)
	  {
	    this.valeur = val;
	  }
	  public int val()
	  {
		  return valeur;
	  }
	}

	private static HashMap<Integer, Boolean> keys = new HashMap<Integer,Boolean>();
	private static HashMap<Integer, Boolean> previousKeys = new HashMap<Integer,Boolean>();
	
	public static void setKey(int key, boolean value)
	{
		keys.put(key, value);
	}
	
	public static boolean getKey(int key)
	{
		if(!keys.containsKey(key))
			keys.put(key, false);
		return keys.get(key);
	}
	
	public static boolean getPreviousKey(int key)
	{
		if(!previousKeys.containsKey(key))
			previousKeys.put(key, false);
		return previousKeys.get(key);
	}
	
	public static void sync()
	{
		previousKeys.putAll(keys);
	}
	
	public static HashMap<Integer,Boolean> getKeys()
	{
		return keys;
	}
	public static HashMap<Integer,Boolean> getPreviousKeys()
	{
		return previousKeys;
	}
	public static void remiseZero()
	{
		
		keys= new HashMap<Integer,Boolean>();
		previousKeys= new HashMap<Integer,Boolean>();
	}
	public static void init(HashMap<Integer,Boolean> actuel, HashMap<Integer,Boolean> precendent)
	{
		keys= new HashMap<Integer, Boolean>();
		previousKeys= new HashMap<Integer,Boolean>();
		keys.putAll(actuel);
		previousKeys.putAll(precendent);
		
	}
	
	public static boolean appuiDroite()
	{
		return (Keys.getKey(Keys.Touches.rightKey.val()) && !Keys.getPreviousKey(Keys.Touches.rightKey.val())
				|| (Keys.getKey(Keys.Touches.d.val()) && !Keys.getPreviousKey(Keys.Touches.d.val())));
	}
	public static boolean appuiGauche()
	{
		return (Keys.getKey(Keys.Touches.leftKey.val()) && !Keys.getPreviousKey(Keys.Touches.leftKey.val())
				|| (Keys.getKey(Keys.Touches.q.val()) && !Keys.getPreviousKey(Keys.Touches.q.val()))
				|| (Keys.getKey(Keys.Touches.a.val()) && !Keys.getPreviousKey(Keys.Touches.a.val())));
	}
	public static boolean appuiHaut()
	{
		return (Keys.getKey(Keys.Touches.upKey.val()) && !Keys.getPreviousKey(Keys.Touches.upKey.val())
				|| (Keys.getKey(Keys.Touches.z.val()) && !Keys.getPreviousKey(Keys.Touches.z.val()))
				|| (Keys.getKey(Keys.Touches.w.val()) && !Keys.getPreviousKey(Keys.Touches.w.val())));
	}
	public static boolean appuiBas()
	{
		return (Keys.getKey(Keys.Touches.downKey.val()) && !Keys.getPreviousKey(Keys.Touches.downKey.val())
				|| (Keys.getKey(Keys.Touches.s.val()) && !Keys.getPreviousKey(Keys.Touches.s.val())));
	}
}
