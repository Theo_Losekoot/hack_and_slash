
public class EntiteTimer extends EntiteAnimee
{

	private int TTL;
	
	/**
	 * Constructeur de l'EntiteTimer, une entite qui disparait avec le temps
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param isObstacle
	 * @param tempsRestant
	 * @param entite
	 */
	public EntiteTimer(String nom, double x, double y, double width, double height, boolean isObstacle, int tempsRestant, int entite)
	{
		//EntiteAnimee : nom, positionX, positionY, largeur, hauteur, obstacle?
		//c'est un etre vivant ?, quelle entite ?, routation
		super(nom, x, y, width, height, isObstacle, false, entite, 0);
		TTL=tempsRestant;
	}

	public int getTTL()
	{
		return TTL;
	}
	
	public void setTTL(int ttl)
	{
		TTL=ttl;
	}
	
	/**
	 * diminue le temps restant a vivre de l'entite, et renvoie un boolean
	 * @return true si l'entite devrait disparaitre, false sinon
	 */
	public boolean diminuerTTL()
	{
		TTL-=GameWorld.TEMPS_RAFRAICHISSEMENT;
		if(TTL<=0)
			return true;
		return false;
	}
	
	//Dessine l'entite avec la bonne image et la bonne animation
	public void dessine()
	{
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(quelleEntite, this.numeroAnimation),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();
	}


}
