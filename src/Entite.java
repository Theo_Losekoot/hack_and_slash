
public abstract class Entite implements java.lang.Comparable<Entite>
{
	
	// le nom de notre entite
	protected String nom;
	
	// la position de l'entite
	protected Position position;
	
	//pour l'affichage, connaitre la largeur et la hauteur
	protected double width, height;
	
	//pour savoir quelle image afficher
	protected int quelleEntite;

	//la hitbox de l'entite
	protected Hitbox hitbox;
	
	/**
	 * Constructeur des entites
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param isObstacle
	 * @param etreVivant
	 * @param entite
	 */
	public Entite(String nom, double x, double y, double width, double height, boolean isObstacle, boolean etreVivant, int entite)
	{
		this.nom = nom;
		position = new Position(x, y);
		hitbox = new Hitbox(width, height, x, y, isObstacle, etreVivant);
		this.width = width;
		this.height = height;
		quelleEntite=entite;
	}
	
	//quelle entite c'est ? (lapin ? fermier ? ) pour l'affichage
	public int getNumEntite()
	{
		return quelleEntite;
	}
	
	public double getHeight()
	{
		return height;
	}
	
	public double getWidth()
	{
		return width;
	}

	public String getNom()
	{
		return this.nom;
	}

	public double getX()
	{
		return position.getX();
	}

	public double getY()
	{
		return this.position.getY();
	}
	
	public Hitbox getHB()
	{
		return hitbox;
	}

	public void setHB(Hitbox hit)
	{
		Hitbox.copy(hit,hitbox);
	}

	public String toString()
	{
		return nom;
	}

	public void setPosition(Position p)
	{
		this.position = p;
	}
	
	public boolean isObstacle()
	{
		return hitbox.isObstacle();
	}
	
	//definit si on se cogne sur l'objet
	public void setIsObstacle(boolean isObstacle)
	{
		hitbox.setIsObstacle(isObstacle);
	}
	
	//met a jour es coordonnees de la hitbox
	public void updateHB()
	{
		this.hitbox.update(this.getX(), this.getY());
	}

	// dessine l'entite, aux bonnes coordonnees
	public abstract void dessine();

	//dessine l'entite avec rotation
	public abstract void dessine(double rotation);

	/**
	 * trie les entites en fonction de la position de leur hitbox, pour savoir laquelle afficher en premier ensuite
	 */
	@Override
	public int compareTo(Entite autre)
	{
		if(this.getHB().getPosYM()>autre.getHB().getPosYM())
			return -1;
		else if (this.getHB().getPosYM()<autre.getHB().getPosYM())
			return 1;
		return 0;
	}
	
	public void setEntite(int entite)
	{
		quelleEntite=entite;
	}
	public void setWidth(double _width)
	{
		width=_width;
	}
	public void setHeight(double _height)
	{
		height=_height;
	}
}
