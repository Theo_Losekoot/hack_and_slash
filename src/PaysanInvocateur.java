import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class PaysanInvocateur extends Chasseur
{
	private static double usualWidth=1.5;
	private static double usualHeight=1.5;
	private static int usualLifePoints=300;
	private static int usualAttack=20;
	private static int usualDefense=15;
	private static double usualSpeed=0.01;
	private static double usualKnockback=0.0;
	private static double usualFrequency=2;

	private ArrayList<Poule> enfants;
	private int nbEnfantsAutorises;
	private double frequenceApparition;

	/**
	 * Constructeur de paysanInvocateur, plutot explicite
	 * @param nom
	 * @param x
	 * @param y
	 * @param pDV
	 * @param pAtk
	 * @param pDef
	 * @param vitesse
	 * @param width
	 * @param height
	 * @param _recul
	 * @param nbEnfantsMax
	 * @param freqPop
	 */
	public PaysanInvocateur(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width,
			double height, double _recul, int nbEnfantsMax, double freqPop, boolean _estUnBoss)
	{
		super(nom, x, y, pDV, pAtk, pDef, vitesse, width, height, Images.Entites.PaysanInvocateur.val(), _recul, _estUnBoss);
		enfants = new ArrayList<Poule>();
		nbEnfantsAutorises=nbEnfantsMax;
		frequenceApparition=freqPop;
	}
	
	@Override
	public void jouer()
	{
		if(this.getPointsDeVie()<=0)
		{
			mourir();
			return;
		}
		updateImmunite();
		actualiserEnfants();
		apparaitreEnfants();
		seDeplacer();
		return;
	}
	
	
	/**
	 * determine le deplacement de l'invocateur
	 */
	public void augmenterVecteur()
	{
		double distanceJoueur = GameWorld.getDistanceJoueur(this.getX(), this.getY());
		Vector newNorm = new Vector(this.getVitesse());
		if(distanceJoueur<GameWorld.getWidthGrille()/3)
		{
			newNorm.setX(this.getX()-GameWorld.getJoueur().getX());
			newNorm.setY(this.getY()-GameWorld.getJoueur().getY());
		}
		else if (distanceJoueur>GameWorld.getWidthGrille()/2)
		{
			newNorm.setX(GameWorld.getJoueur().getX()-this.getX());
			newNorm.setY(GameWorld.getJoueur().getY()-this.getY());
		}
		newNorm = newNorm.normalised();
		vecteurAvancer.setX(vecteurAvancer.getX()+newNorm.getX()*this.getVitesse());
		vecteurAvancer.setY(vecteurAvancer.getY()+newNorm.getY()*this.getVitesse());
	}

	/**
	 * lance l'attaque (si il y en a une ? on sait pas encore)
	 */
	public void attaquer()
	{
		// vecteurAvancer.setX(0);
		// vecteurAvancer.setY(0);
		if (!frappeActuellement())
		{
			tempsAttaqueActuelle = 0;
		}
	}

	
	
	/**
	 * bha du coup pour l'instant c'est vide
	 */
	@Override
	public void actualiserCoup()
	{

	}
	
	/**
	 * fait jouer les enfants
	 */
	public void actualiserEnfants()
	{
		//faire jouer les enfants
		for(int i=0; i<enfants.size(); i++)
		{
			enfants.get(i).jouer();
			//et verifie s'ils sont en vie
			if(!enfants.get(i).isAlive())
			{
				enfants.remove(i--);
			}
		}
	}
	
	/**
	 * fonction pour faire apparaitre les enfants avec une certaine probabilite et jusqu'a un certain nombre
	 */
	public void apparaitreEnfants()
	{
		//chances de faire apparaitre une poule : environ toutes les 2 secondes
		// ici, 50 passages dans cette boucle par seconde
		//donc ici, 150 en 3 secondes, on va mettre 1/150 chances de faire apparaitre un enfant
		int tempsMoyen = 3 * 1000/GameWorld.TEMPS_RAFRAICHISSEMENT;
		boolean apparaitre = ((int)(ThreadLocalRandom.current().nextInt(0, tempsMoyen)%(tempsMoyen/frequenceApparition))==0);
		if(enfants.size()<nbEnfantsAutorises && apparaitre)
		{
			Poule nouvelEnfant = new Poule("pouleCherie", this.getX(), this.getY(), Poule.getUsualLifePoints(), Poule.getUsualAttack(),
					Poule.getUsualDefense(), Poule.getUsualSpeed(), Poule.getUsualWidth(),
					Poule.getUsualHeight(), Poule.getUsualKnockback(), false);
			GameWorld.addEntite(nouvelEnfant);
			GameWorld.getMonstres().add(nouvelEnfant);
			enfants.add(nouvelEnfant);
		}
	}
	
	
	/**
	 * supprime tous les enfants et appele la fonction parente
	 */
	@Override
	public void mourir()
	{
		while(enfants.size()>0)
		{
			enfants.get(0).mourir();
			enfants.remove(0);
		}
		super.mourir();
		GameWorld.getJoueur().addXp(20);
	}
	

	public void dessine()
	{
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(this.quelleEntite, this.numeroAnimation, this.vecteurAvancer, this.droite, this.frappeActuellement()),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();
		//dessine une barre de vie
		StdDraw.setPenColor(StdDraw.RED);
		if(estUnBoss)
		StdDraw.filledRectangle((double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()-this.getHeight()/2) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				(double)(this.getPointsDeVie()/PaysanInvocateur.getUsualLifePoints()/(double)5) /GameWorld.getWidthGrille(), (double)(this.getHeight()/30) /GameWorld.getHeightGrille());
	
	}
	
	
	public static double getUsualWidth()
	{
		return usualWidth;
	}
	public static double getUsualHeight()
	{
		return usualHeight;
	}

	public static int getUsualLifePoints()
	{
		return usualLifePoints;
	}

	public static int getUsualAttack()
	{
		return usualAttack;
	}

	public static int getUsualDefense()
	{
		return usualDefense;
	}

	public static double getUsualSpeed()
	{
		return usualSpeed;
	}
	public static double getUsualKnockback()
	{
		return usualKnockback;
	}
	public static double getUsualFrenquency()
	{
		return usualFrequency;
	}
}
