import java.util.concurrent.ThreadLocalRandom;

public class SelectionClasse
{

	protected static int objetSelectionne;
	protected static int[] positionCurseur;
	private static double[][] positionsDessinSurbrillance = new double[4][3];
	
	public static void interagir()
	{
		//\"annule\" le enter qu'on a fait pour entrer en conversation avec le marchant, pour ne pas selectionner le premier item
		Keys.sync();
		positionCurseur = new int[1];
		initPositionDessins();
		while(!processKeysInventory(positionCurseur))
		{
			StdDraw.clear();
			
			dessine(positionCurseur[0]);
	
			// Montre la fenetre graphique mise a jour et attends 20 millisecondes
			StdDraw.show();
		}
		Keys.sync();
	}
	
	
	
	/**
	 * reagit aux touches pressees par l'utilisateur, permet de se deplacer dans l'inventaire
	 * @param positionCurseur la postition du curseur actuellement
	 * @return true si on ferme l'inventaire
	 */
	public static boolean processKeysInventory(int[] positionCurseur)
	{

		if(Keys.getKey(Keys.Touches.returnKey.val()) && !Keys.getPreviousKey(Keys.Touches.returnKey.val()))
		{
				action(positionCurseur[0]);
				return true;
		}
		if(Keys.appuiBas())
		{
			bougerCurseur(0, positionCurseur);
		}
		if(Keys.appuiHaut())
		{
			bougerCurseur(2, positionCurseur);
		}
		if(Keys.appuiDroite())
		{
			bougerCurseur(1, positionCurseur);
		}
		if(Keys.appuiGauche())
		{
			bougerCurseur(3, positionCurseur);
		}
		
			Keys.sync();
			return false;
	}
		
	
	/*
	 * l'interface se presente comme : 
	 * 
	 * 		0(mage)
	 * 
	 * 1(archer)	2(bourrin)
	 * 
	 */
	
	/**
	 * bouge le curseur entre les trois choix possibles
	 * @param quelSens
	 * @param positionCurseur
	 */
	private  static void bougerCurseur(int quelSens, int[] positionCurseur)
	{
		//Cette fonction repond a chacun des cas en particulier, pour avoir des 
			//deplacements dans l'inventaire plus propres
			switch(quelSens)
			{
			case 0: //que faire si le curseur doit se deplacer vers le bas
				//si curseur a gauche
				if(positionCurseur[0] ==0)
					positionCurseur[0]=ThreadLocalRandom.current().nextInt(1,3);//va une fois sur deux a droite, une fois sur deux a gauche	
				return;

			case 1: //que faire si le curseur veut se deplacer vers la droite
				//si le curseur est sur ue des cases qui ne necessite que de faire +1
				if(positionCurseur[0]==1)
						positionCurseur[0]=2;
				return;
						
			case 2: //que faire si le curseur doit se deplacer vers le haut
				//si curseur a gauche
					positionCurseur[0]=0;
					return;

			case 3: //que faire si le curseur veut se deplacer vers la gauche
				//si le curseur est sur une des cases qui ne necessite que de faire -1
				if(positionCurseur[0]==2)
					positionCurseur[0]=1;
				return;
			}
	}

	/**
	 * selectionne la bonne classe
	 * @param leNumero le numero de classe
	 */
	public static void action(int leNumero)
	{
		GameWorld.getJoueur().setTypePersonnage(leNumero);
	}
	
	
	/**
	 * dessine l'inventaire, donc l'image de base, les items, et les cadres de surbrillance
	 * @param positionCurseur
	 */
	public static void dessine(int positionCurseur)
	{
		StdDraw.picture(0.5,
				0.5,
				Anim.bonneImage(Images.Entites.ChoixArbre.val()),
				1,
				1,
				0);
	
			//affiche le \"curseur\"
			if(objetSelectionne<positionsDessinSurbrillance[0].length)
			{
				//pour le type de surbrillance
				int entite;
		
				if(positionCurseur==0)
					entite=Images.Entites.SelectMage.val();
				else if(positionCurseur==1)
					entite=Images.Entites.SelectArcher.val();
				else
					entite=Images.Entites.SelectBourrin.val();
				
				StdDraw.picture(positionsDessinSurbrillance[0][positionCurseur],
						positionsDessinSurbrillance[1][positionCurseur],
						Anim.bonneImage(entite),
						positionsDessinSurbrillance[2][positionCurseur],
						positionsDessinSurbrillance[3][positionCurseur],
						0);
			}
	}
	
	
	/**
	 * 	calcule les emplacements de l'inventaire pour l'affichage, 
	 *  pour afficher les items au bons endroits et la surbrillance aussi
	 *  les valeurs sont codees dans le dur car l'inventaire est une image
	 *  et donc ces valeurs, entre 0 et 1, ne changeront jamais.
	 */
	private static void initPositionDessins()
	{
		
		positionsDessinSurbrillance = new double[4][3];
		
		//la taille des choix
		double largeurChoix = 0.12;
		double hauteurChoix = largeurChoix* 3/2;

		
		
		positionsDessinSurbrillance[0][0]=0.512;
		positionsDessinSurbrillance[1][0]=0.910;
		positionsDessinSurbrillance[2][0]=largeurChoix;
		positionsDessinSurbrillance[3][0]=hauteurChoix;
		
		positionsDessinSurbrillance[0][1]=0.33;
		positionsDessinSurbrillance[1][1]=0.44;
		positionsDessinSurbrillance[2][1]=largeurChoix;
		positionsDessinSurbrillance[3][1]=hauteurChoix;
		
		positionsDessinSurbrillance[0][2]=0.7;
		positionsDessinSurbrillance[1][2]=0.45;
		positionsDessinSurbrillance[2][2]=largeurChoix;
		positionsDessinSurbrillance[3][2]=hauteurChoix;
	}

	
}
