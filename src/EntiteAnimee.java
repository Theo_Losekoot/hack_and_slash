
public class EntiteAnimee extends Entite
{


	//pour savoir quelle image afficher dans l'animation
	
	//sert a l'animation du personnage
	protected int[] numeroAnimation;
	protected double rotationActuelle;
	protected double rotationContinue;
	
	/**
	 * Constructeur EntiteAnimee, comme entite mais avec un numero d'animation, et de la rotaton possible
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param isObstacle
	 * @param etreVivant
	 * @param entite
	 * @param rotat
	 */
	public EntiteAnimee(String nom, double x, double y, double width, double height, boolean isObstacle, boolean etreVivant, int entite, double rotat)
	{
		//Entite: nom, positionX, positionY, largeur, hauteur, ostacle? etreVivant? quelleEntite?
		super(nom, x, y, width, height, isObstacle, etreVivant, entite);
		numeroAnimation = new int[1];
		numeroAnimation[0]=0;
		rotationActuelle=0;
		rotationContinue=rotat;
	}

	public void setNumeroAnimation(int num)
	{
		numeroAnimation[0]=num;
	}
	
	public int getNumeroAnimation()
	{
		return numeroAnimation[0];
	}
	
	//Dessine l'objet avec la bonne image et orientation
	public void dessine()
	{
		//Dessine l'objet
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(quelleEntite, this.numeroAnimation),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				rotationActuelle+=rotationContinue); 
		updateVarAnim();
	}
	//Dessine l'objet avec la bonne image et orientation, avec une rotation de base
	public void dessine(double rotation)
	{
		//Dessine l'objet avec la bonne orientation
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(quelleEntite),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				rotation + (rotationActuelle+=rotationContinue)); 
		updateVarAnim();
	}
	
	/**
	 * met a jour la variable d'animation, pour que le personnage bouge
	 */
	protected void updateVarAnim()
	{
		numeroAnimation[0]++;
	}

}
