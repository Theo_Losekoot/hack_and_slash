
public class Bijoux extends Outil
{
	/**
	 * une enumeration pour les differents types de bonus, pratique
	 *
	 */
	public static enum TypesBonus
	{
		  vitesse(0),
		  pv(1),
		  mana(2),
		  attaque(3),
		  defense(4),
		  volVie(5),
		  knockback(6),
		  chance(7),
		  gainXp(8),
		  vitesseAttaque(9);
		  
		
	  private int valeur;
	  private TypesBonus(int val)
	  {
	    this.valeur = val;
	  }
	  public int val()
	  {
		  return valeur;
	  }
	}

	int quelBonus;
	/**
	 * Constructeur de bijoux
	 * @param nom
	 * @param x
	 * @param y
	 * @param bonus
	 * @param width
	 * @param height
	 * @param dequoi
	 * @param quelleEntite
	 */
	public Bijoux(String nom, double x, double y, int bonus, double width, double height, int dequoi, int quelleEntite, int val)
	{
		//Outil : nom, positionX, positionY, largeur, hauteur, quel bijou ?
		super(nom, x, y, bonus, width, height, quelleEntite, val);
		quelBonus=dequoi;
		genererTexteDescriptif();

	}
	
	/**
	 * genere un texte descriptif different pour chaque bijoux, pour afficher leur effet dans l'inventaire
	 */
	public void genererTexteDescriptif()
	{
		texteDescriptif[0] = "Categorie : Bijoux";
		texteDescriptif[1] = "+" + this.bonus;
		if(quelBonus==TypesBonus.attaque.val())
			texteDescriptif[1]+=" degats";
		else if(quelBonus==TypesBonus.chance.val())
			texteDescriptif[1]+="% de chances de  drop double thunes";
		else if(quelBonus==TypesBonus.defense.val())
			texteDescriptif[1]+=" defense";
		else if(quelBonus==TypesBonus.gainXp.val())
			texteDescriptif[1]+="% de gain d'XP";
		else if(quelBonus==TypesBonus.knockback.val())
			texteDescriptif[1]+="% de recul";
		else if(quelBonus==TypesBonus.mana.val())
			texteDescriptif[1]+=" mana";
		else if(quelBonus==TypesBonus.pv.val())
			texteDescriptif[1]+=" PV";
		else if(quelBonus==TypesBonus.vitesse.val())
			texteDescriptif[1]+="% de vitesse";
		else if(quelBonus==TypesBonus.volVie.val())
			texteDescriptif[1]+="% de vol de vie";
		else if(quelBonus==TypesBonus.vitesseAttaque.val())
			texteDescriptif[1]+="% vitesse attaque";
		else
			texteDescriptif[1]+="OUPS LOL";
	}
	
	public void setBonus(int bon)
	{
		bonus=bon;
	}
	public int getBonus()
	{
		return bonus;
	}
	
	public int getTypeBonus()
	{
		return quelBonus;
	}
}
