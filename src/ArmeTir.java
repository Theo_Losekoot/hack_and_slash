import java.util.ArrayList;

public class ArmeTir extends Arme
{
	
	private boolean armeMagique;
	//le cout en mana
	private int coutTir;
	//le nombre de boulettes tirees au cours de cette attaque. aha.
	private int nombreDeBoulettesTireesAuCoursDeCetteAttaque;
	
	/**
	 * Constructeur de Arme
	 * @param nom
	 * @param x
	 * @param y
	 * @param degats
	 * @param width
	 * @param height
	 * @param quelleEntite pour l'affichage
	 * @param _typeArme les classes pouvant porter cette arme( magicien ? bourrin ? archer ? etc)
	 * @param val la valeur de l'arme
	 * @param _armeMagique l'arme consomme du mana ? 
	 * @param _coutTir le cout de tirer une boulette
	 */
	public ArmeTir(String nom, double x, double y, int degats, double width, double height, int quelleEntite,
			ArrayList<Integer> _typeArme, int val, boolean _armeMagique, int _coutTir)
	{
		//Outil : nom, positionX, positionY, largeur, hauteur, quelle arme?
		super(nom, x, y, degats, width, height, quelleEntite, _typeArme, val);

		armeMagique=_armeMagique;
		coutTir=_coutTir;
		nombreDeBoulettesTireesAuCoursDeCetteAttaque=0;
	}
	
	/**
	 * genere le texte descriptiif pour l'inventaire
	 */
	public void genererTexteDescriptif()
	{
		texteDescriptif[0] = "Categorie : ArmeTir";

		texteDescriptif[1] = "+" + this.getDegats() + " degats                               ";
		
		if(classesPouvantLaPorter!=null)
		{
			texteDescriptif[1] += "Classe possible :    " ;
			if (classesPouvantLaPorter.contains(Integer.valueOf(0)))
				texteDescriptif[1] += "     Mage           ";
			if (classesPouvantLaPorter.contains(1))
				texteDescriptif[1] += "     archer         ";
			if (classesPouvantLaPorter.contains(2))
				texteDescriptif[1] += "     Bourrin        ";
			if (classesPouvantLaPorter.contains(3))
				texteDescriptif[1] += "     Peon(tous)     ";
		}

	}
	
	public void setDegats(int deg)
	{
		bonus=deg;
	}
	public int getDegats()
	{
		return bonus;
	}

	/**
	 * Tire 3 petites boulettes 
	 */
	@Override
	public void attaque2(Joueur joueur)
	{

		if(armeMagique)
		{
			joueur.dureeAttaque=GameWorld.TEMPS_RAFRAICHISSEMENT*12;
			double dureeAttaqueTotale=(double)joueur.dureeAttaque/joueur.getVitesseAttaque();
			//tire 3 projectiles
			if(( (joueur.tempsAttaqueActuelle/dureeAttaqueTotale) >= (double)nombreDeBoulettesTireesAuCoursDeCetteAttaque/3))
			{
				nombreDeBoulettesTireesAuCoursDeCetteAttaque++;
				//projectiles petits et un peu puissants
				GameWorld.addEntite(new Sort("nomdeProjectile", getX(), getY(), 999,(int)(joueur.getAttaqueTotale()/1.5),999,0.05,
						1,1,1, Images.Entites.Sort1.val(), joueur.droite, 5, true));
				
			}
			else if(joueur.tempsAttaqueActuelle>=(dureeAttaqueTotale-GameWorld.TEMPS_RAFRAICHISSEMENT))
			{
				nombreDeBoulettesTireesAuCoursDeCetteAttaque=0;
			}
		}
		else
		{
			joueur.dureeAttaque=GameWorld.TEMPS_RAFRAICHISSEMENT*3;
			if(joueur.tempsAttaqueActuelle==0) // seulement au debut
			{
				GameWorld.addEntite(new Sort("nomdeProjectile", getX(), getY(), 999,(int)(joueur.getAttaqueTotale()/1.5),999,0.05,
						1.5,0.5,1, Images.Entites.Fleche.val(), joueur.droite, 1, false));
			}
		}
		
		int rotation=0;	//affichage de l'arme
		Arme temp = joueur.inventory.getArme();
		if(armeMagique)
		{
			//definit les bons angles pour la rotations de l'arme
			if(joueur.droite)
			{
				temp.setPosition(new Position(temp.getX()+joueur.getWidth()/2,temp.getY()));//-joueur..getHeight()/6));
				rotation=-45;
			}
			else
			{
				temp.setPosition(new Position(temp.getX()-joueur.getWidth()/2,temp.getY()));//-joueur..getHeight()/6));
				rotation=135;
			}
		}
		else
		{
			if(joueur.droite)
			{
				temp.setPosition(new Position(temp.getX()+joueur.getWidth()/2,temp.getY()));//-joueur..getHeight()/6));
				rotation=0;
			}
			else
			{
				temp.setPosition(new Position(temp.getX()-joueur.getWidth()/2,temp.getY()));//-joueur..getHeight()/6));
				rotation=180;
			}
		}
		GameWorld.addEntiteOneShot(temp, rotation);
		//fin affichage de l'arme

		
	}
	
	/**
	 * tire un gros projectile apres un instant de charge
	 */
	@Override
	public void attaque1(Joueur joueur)
	{
		//affichage de l'arme
		int rotation=-0;
		Arme temp = joueur.inventory.getArme();
		if(armeMagique)
		{
			//definit les angles pour l'attaque2
			if(joueur.droite)
			{
				temp.setPosition(new Position(temp.getX()+joueur.getWidth()/2,temp.getY()));//-joueur..getHeight()/6));
				rotation=-45;
			}
			else
			{
				temp.setPosition(new Position(temp.getX()-joueur.getWidth()/2,temp.getY()));//-joueur..getHeight()/6));
				rotation=135;
			}
		}
		else
		{
			if(joueur.droite)
			{
				temp.setPosition(new Position(temp.getX()+joueur.getWidth()/2,temp.getY()));//-joueur..getHeight()/6));
				rotation=0;
			}
			else
			{
				temp.setPosition(new Position(temp.getX()-joueur.getWidth()/2,temp.getY()));//-joueur..getHeight()/6));
				rotation=180;
			}
		}
		GameWorld.addEntiteOneShot(temp, rotation);
		//fin affichage de l'arme
		
		
		//tire le projectile
		joueur.dureeAttaque=GameWorld.TEMPS_RAFRAICHISSEMENT*15;
		double dureeAttaqueTotale=joueur.dureeAttaque/joueur.getVitesseAttaque();
		if(joueur.tempsAttaqueActuelle> dureeAttaqueTotale-(GameWorld.TEMPS_RAFRAICHISSEMENT))
		{
			if(armeMagique) //balance un sort
			//projectile gros et full puissance
			GameWorld.addEntite(new Sort("nomdeProjectile", getX(), getY(), 999,joueur.getAttaqueTotale(),999,0.05,
					2,2,1, Images.Entites.Sort1.val(), joueur.droite, 5, true));
			else //balance une fleche
				GameWorld.addEntite(new Sort("nomdeProjectile", getX(), getY(), 999,joueur.getAttaqueTotale(),999,0.05,
						3,1,1, Images.Entites.Fleche.val(), joueur.droite, 1, false));
		}
	}
	
	public boolean getArmeMagique()
	{
		return armeMagique;
	}
	
	/**
	 * si c'est une arme magique, dire combien coute un tir
	 */
	@Override
	public int coutTir()
	{
		if(armeMagique)
			return coutTir;
		else
			return 0;
	}
}
