import java.util.ArrayList;

public abstract class Arme extends Outil
{	

	
	//savoir si on peut l'equiper
	protected ArrayList<Integer> classesPouvantLaPorter;
	
	/**
	 * Constructeur de Arme
	 * @param nom
	 * @param x
	 * @param y
	 * @param degats
	 * @param width
	 * @param height
	 * @param quelleEntite pour l'affichage
	 * @param _typeArme les classes pouvant porter cette arme( magicien ? bourrin ? archer ? etc)
	 * @param val la valeur de l'arme
	 */
	public Arme(String nom, double x, double y, int degats, double width, double height, int quelleEntite, ArrayList<Integer> _typeArme, int val)
	{
		//Outil : nom, positionX, positionY, largeur, hauteur, quelle arme?
		super(nom, x, y, degats, width, height, quelleEntite, val);
		classesPouvantLaPorter = new ArrayList<Integer>();
		classesPouvantLaPorter.addAll(_typeArme);
		genererTexteDescriptif();
	}
	
	/**
	 * genere un texte pour l'affichage dans l'inventaire et chez le marchand
	 */
	public abstract void genererTexteDescriptif();
	
	
	public void setDegats(int deg)
	{
		bonus=deg;
	}
	public int getDegats()
	{
		return bonus;
	}

	
	//les differentes attaques que les armes peuvent faire
	public abstract void attaque1(Joueur joueur);
	
	public abstract void attaque2(Joueur joueur);

	//c'est une arme magique ?
	public abstract boolean getArmeMagique();
	
	//elle coute cmbien de mana a tirer?
	public abstract int coutTir();
	
	public ArrayList<Integer> getClassesPouvantPorter()
	{
		return classesPouvantLaPorter;
	}
}
