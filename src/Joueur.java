import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Joueur extends EtreVivant
{
	protected Inventory inventory;

	//experience pour chaque niveau
	protected int experience;
	protected int experienceRequise;
	protected int niveau;
	
	protected int pointsMana;
	protected int pointsManaMax;

	//les bonus dus aux equipements 
	protected double reculBonus;
	protected double vitessePBonus;
	protected int pointsDeDefenseBonus;
	protected int pointsDAttaqueBonus;
	protected int pointsDeVieMaxBonus;
	protected double gainXPBonus;
	protected double chanceBonus;
	protected double volVieBonus;
	protected double vitesseAttaque;
	protected int manaBonus;
	
	
	protected boolean  autoriseABouger;
	//regeneration auto
	protected int tempsAvantRegenMana;
	protected int tempsAvantRegenVie;
	
	protected int typePersonnage;

	//des bonus de classe
	protected int bonusDegatsClasse;
	protected int bonusArmureClasse;
	protected int bonusPVClasse;
	protected int bonusManaClasse;
	protected double bonusVitesseAttaqueClasse;
	protected double bonusVitesseClasse;
	protected double bonusEsquiveClasse;
	protected double bonusReculClasse;
	
	//0 : mage, 1 : archer, 2 : bourrin, 3 :persoBase
	
	
	public Joueur(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width, double height, double _recul)
	{
		//EtreVivant : nom, positionX, positionY, points de vie,  points d'attaque
		//points de defense, vitesse, largeur, hauteur
		super(nom, x, y, pDV, pAtk, pDef, vitesse, width, height, Images.Entites.Perso.val(), _recul);
		
		inventory = new Inventory(3);
		pointsMana=pointsManaMax=100;
		experience=0;
		niveau=0;

		initInventory();
		updateBonus();
		experienceRequise = (int)(100*1.7) * (this.getNiveau()+1);
		autoriseABouger=true;
		typePersonnage=0;
		tempsAvantRegenMana=500;
		tempsAvantRegenVie=1000;
		this.setTypePersonnage(3);
		
	}
	
	

	
	public void updateBonus()
	{
		reculBonus=0;
		vitessePBonus=0;
		pointsDeDefenseBonus=0;
		pointsDAttaqueBonus=0;
		pointsDeVieMaxBonus=0;
		vitesseAttaque=0;
		volVieBonus=0;
		chanceBonus=0;
		gainXPBonus=0;
		manaBonus=0;
		//ajout des bonus provenant des bijoux
		for(Bijoux b : inventory.getBijoux())
		{
			if(b!=null)
			{
				if(b.getTypeBonus() == Bijoux.TypesBonus.knockback.val())
					reculBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.attaque.val())
					pointsDAttaqueBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.defense.val())
					pointsDeDefenseBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.vitesse.val())
					vitessePBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.pv.val())
					pointsDeVieMaxBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.volVie.val())
					volVieBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.chance.val())
					chanceBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.gainXp.val())
					gainXPBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.mana.val())
					manaBonus+=b.getBonus();
				else if (b.getTypeBonus() == Bijoux.TypesBonus.vitesseAttaque.val())
					vitesseAttaque+=b.getBonus();
			}
		}
		
		//ajout des bonus provenant de l'arme
		if(inventory.getArme()!=null)
			pointsDAttaqueBonus+=inventory.getArme().getDegats();
		if(inventory.getArmure()!=null)
			pointsDeDefenseBonus+=inventory.getArmure().getDefense();
	}
	

	@Override
	public void jouer()
	{
		verifierEnVie();
		gueulard();
		updateInventory();
		updateImmunite();
		collisionMonstre();
		actualiserCoup();
		seDeplacer();
		changerMonde();
		regenererVie();
		ramasser();
	}
	
	public void regenererVie()
	{
		tempsAvantRegenVie-=GameWorld.TEMPS_RAFRAICHISSEMENT;
		tempsAvantRegenMana-=GameWorld.TEMPS_RAFRAICHISSEMENT;
		if(tempsAvantRegenVie<=0)
		{
			int multiplieur=1;
			if(GameWorld.getMondeActuel()==Maps.Map.Base.val())
				multiplieur=10;
			this.ajouterVie(1 * multiplieur);
			//met 0.5 seconde divisee par un ration fait par notre vie, pour regen plus vte quand on a plus de vie
			tempsAvantRegenVie=(int)(500 * (double)10/(Math.sqrt(this.getPVMax())));
		}
		if(tempsAvantRegenMana<=0)
		{
			int multiplieur=1;
			if(GameWorld.getMondeActuel()==Maps.Map.Base.val())
				multiplieur=10;
			this.ajouterMana(1 * multiplieur);
			tempsAvantRegenMana=(int)(150 * (double)10/Math.sqrt(this.getManaTotal()));
		}
	}
	
	/**
	 * initialise l'inventaire avec quelques babioles
	 */
	private void initInventory()
	{
		Potion b = new Potion("potion soin", 10,10,1,1, 5,30,0, 5);
		inventory.add(b);
		Bombe temp = new Bombe("bombe", 0,0,1,1,false,3, 5);
		inventory.add(temp);
		ArrayList<Integer> classesPortant = new ArrayList<Integer>();
		classesPortant.add(0);
		classesPortant.add(1);
		classesPortant.add(2);
		classesPortant.add(3);
		//nouvelle arme, 3 de degats, 3 de largeur, 3 de hauteur, image d'epee 1, toute le monde peut la porter, elle vaut 10
		ArmeCac cool = new ArmeCac("Arme cool", this.getX(), this.getY(), 5, 3,3, Images.Entites.Epee1.val(), classesPortant, 10);
		inventory.setArme(cool);
		inventory.add(cool);
		Armure cool2 = new Armure("Armure cool", this.getX(), this.getY(), 5, 1,1, Images.Entites.Armure1.val(), classesPortant, 10);
		inventory.setArmure(cool2);
		inventory.add(cool2);
		
		Bijoux[] bij = new Bijoux[2];
		bij[0]= new Bijoux("bijou cool8", this.getX(), this.getY(), 20, 1,1,
				Bijoux.TypesBonus.vitesse.val(), Images.Entites.bijouVitesse.val(), 50);
		bij[1]= new Bijoux("bijou cool8ss", this.getX(), this.getY(), 20, 1,1,
				Bijoux.TypesBonus.pv.val(), Images.Entites.bijouVie.val(), 500);

		inventory.add(bij[0]);
		inventory.add(bij[1]);
		inventory.setBijoux(bij);
		
		Item clef1 = new Cle("cle12",5,12,1,1, Maps.Map.Foret.val());
		inventory.add(clef1);
	
	}

	
	public void recevoir(Item i)
	{
		this.inventory.add(i);
	}

	public int getPointsDeVie()
	{
		return this.pointsDeVie;
	}

	//affiche l'inventaire et mode graphique et permet de le modifier
	public void afficherInventaire()
	{

		Keys.sync();
		int posCur[] = new int[1];
		posCur[0]=0;
		inventory.affiche(posCur);
		Fonctions.pause(GameWorld.TEMPS_RAFRAICHISSEMENT);
		
		// reinitialise les touches pour ne pas faire n'importe quoi en sortant d'inventaire
		Keys.remiseZero();
		updateBonus();

	}

	
	//Les fonctions de de mise a jour du vector.
	public void goUp()
	{
		if(autoriseABouger)
			vecteurAvancer.setY(vecteurAvancer.getY()+this.getVitesseTotale());
	}
	public void goDown()
	{
		if(autoriseABouger)
			vecteurAvancer.setY(vecteurAvancer.getY()-this.getVitesseTotale());
	}
	public void goLeft()
	{
		if(autoriseABouger)
		{
			vecteurAvancer.setX(vecteurAvancer.getX()-this.getVitesseTotale());
			droite=false;
		}
	}
	public void goRight()
	{
		if(autoriseABouger)
		{
			vecteurAvancer.setX(vecteurAvancer.getX()+this.getVitesseTotale());
			droite=true;
		}
	}
	public void dessine()
	{
		if((this.immunite/(GameWorld.TEMPS_RAFRAICHISSEMENT*2))%2==0 || immunite<0)
		{
		//Dessine le personnage avec la bonne orientation
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(Images.Entites.Perso.val(), numeroAnimation, vecteurAvancer, droite, frappeActuellement()),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		}
		
		updateVarAnim();
	}

	/**
	 * init l'attaque 1 de l'arme, ou attaque nulle si on est mage et qu'on a pas d'arme
	 */
	public void attaque1()
	{
		if(inventory.getArme()!=null)
		{
			 if(!this.retirerMana(this.inventory.getArme().coutTir()))
				 return;
			this.stopper();
			if(!frappeActuellement())
			{
				tempsAttaqueActuelle=0;
				quelleAttaque=1;
			}
		}
		else
		{
			if(typePersonnage==0)
			{
				//10 de mana, totalement arbitraire
				 if(!this.retirerMana(10))
					 return;
				GameWorld.addEntite(new Sort("nomdeProjectile", getX(), getY(), 999,this.getAttaqueTotale()/3,999,0.05,
						1,1,1, Images.Entites.Sort1.val(), droite, 5, true));
			}
		}
	}
	
	/**
	 * initialise l'attaque 2 de l'arme, 
	 */
	public void attaque2()
	{
		if(inventory.getArme()!=null)
		{
			//si c'est une arme magique, retirer le mana, sinon arreter l'attaque
			 if(!this.retirerMana(this.inventory.getArme().coutTir()))
				 return;
			 //si c'est une arme cac, arreter le personnage.
			if(inventory.getArme() instanceof ArmeCac)
				this.stopper();
			//lancer l'attaque
			if(!frappeActuellement())
			{
				tempsAttaqueActuelle=0;
				quelleAttaque=2;
			}
		}
	}

	public void verifierEnVie()
	{
		if(this.getPointsDeVie()<=0)
			this.mourir();
	}
	
	@Override
	public void mourir()
	{
		GameWorld.changerMonde(Maps.Map.Base.val());
	}
	
	/**
	 * effectue les dommzges sur l'entite trouvee, et ajoute les points de vie eventuellement voles
	 */
	@Override
	public void attaquer(EtreVivant e)
	{
		//les degats qu'on va faire, donc nos points d'attaque + les degats de
		//l'arme (+bijoux?) +  un nombre aleatoire entre 0 et les points d'ataque/5
		int degatsEnvoyes = this.getAttaqueTotale() + ThreadLocalRandom.current().nextInt(0, this.getAttaqueTotale()/5);
		//ajoute la vie, si on a reussi a en voler
		int degatsRecus = e.prendreDegats(degatsEnvoyes, this.getX(), this.getY(), this.getReculTotal());
		this.ajouterVieVolee(degatsRecus);
	}
	
	public int prendreDegats(int atk, double x, double y, double multiplicateurRecul)
	{
		if(ThreadLocalRandom.current().nextDouble()>this.getEsquiveTotale())
			return super.prendreDegats(atk, x, y, multiplicateurRecul);
		else
			return 0;
	}
	
	/**
 	* ramasse tous les items situes sous nos pieds. 
 	*/
	public void ramasser()
	{
		for(Item i : GameWorld.getItems().values())
		{
			//si il y a une collision, le ramasser
			if(Hitbox.collision(i.getHB(), this.getHB()))
			{
				if(i instanceof Money && ThreadLocalRandom.current().nextInt(0,100) < this.getChanceTotale())
				{
					inventory.addMoney(((Money)i).getQuantite());
					System.out.println("chance ! vous ramassez deux fois cet argent! ");
				}
					//revoie vrai si l'objet est bien ajoute a l'inventaire
				if(inventory.add(i))
				{
					//affiche le type d'objet quand on le ramasse
					afficherTexte(String.valueOf(i.getClass()).substring(5), this.getX(), this.getY(), 3, 500);
					GameWorld.removeEntite(i);
					System.out.println("Vous venez de ramasser : " + i);
				}
			}
		}
	}

	/**
	 * lance l'interraction avec le vendeur, pour acheter ou vendre des choses.
	 */
	public void interagirMarchands()
	{
		List<Marchand> marchands = GameWorld.getEntites().values().stream().filter(x -> x instanceof Marchand).map(Marchand.class::cast).collect(Collectors.toList());
		for(Marchand m : marchands)
		{
			m.getHB().setWidth(m.getHB().getWidth()*0.5);
			m.getHB().setHeight(m.getHB().getHeight()*1.2);
			if(Hitbox.collision(this.getHB(), m.getHB()))
			{
				m.interagir();
			}
			m.getHB().setWidth(m.getHB().getWidth()*2);
			m.getHB().setHeight(m.getHB().getHeight()*(1/1.2));
		}
	}
	
	// depose les cles que le joueur possede dans la porte
	public void deposerCle()
	{
		//stocke les portes
		List<PorteMagique> lesPortes = GameWorld.getEntites().values().stream()
				.filter(PorteMagique.class::isInstance).map(PorteMagique.class::cast)
				.collect(Collectors.toList());
	
		//pour chaque porte, essaie de l'ouvrir si on a la cle et si on est a proximite
		for(PorteMagique porte : lesPortes)
		{
			//Grossit un peu la hitbox, pour que la collision soit detectee sans que 
			//le joueur ne soit bloque par la porte
			porte.hitbox.setHeight(porte.hitbox.getHeight()*1.3);
			porte.hitbox.setWidth(porte.hitbox.getWidth()*1.3);
			
			if(Hitbox.collision(this.getHB(), porte.getHB()))
			{
				List<Cle> lesCles = inventory.getInventory().stream()
						.filter(Cle.class::isInstance).map(Cle.class::cast)
						.collect(Collectors.toList());
				for(Cle i : lesCles)
				{
					if(porte.placer(i))
					{
						System.out.println("vous avez ouvert " + porte + "avec la cle " + i );
						inventory.remove(i);
					}
				}
				//pour toujours ouvrir les portes des boss, menant a la base
				if(porte.getTypePorte()==Maps.Map.Base.val())
				{
					porte.placer(new Cle("cleee",5,12,1,1, Maps.Map.Base.val()));
				}
				//poser(i);
			}
			//puis la retrecit ! 
			porte.hitbox.setHeight(porte.hitbox.getHeight()/1.3);
			porte.hitbox.setWidth(porte.hitbox.getWidth()/1.3);
		}

	}

	/**
	 * pose un item
	 * @param i
	 */
	public void poser(Item i)
	{
		updateInventory();
		System.out.println("Vous posez " + i);
		if(inventory.getInventory().contains(i) && i!=null)
		{
			GameWorld.addEntite(inventory.remove(i));
		}
	}

	public void updateInventory()
	{
		inventory.update(this.getX(), this.getY());
	}
	
	public void jeter()
	{
			poser(inventory.getRandomItemInStock());
	}

	 /**
 	 * appele si on appuie sur P, qui est la touche raccourci de la 1ere fonction de potion. 
	 * consomme la potion mise a cet emplacement s'il y en a une.
	 */
	public void utiliserPotion1()
	{
		if(inventory.getRaccourci1()!=null && inventory.getRaccourci1() instanceof Potion)
		{
			//si potion de vie et que vie pas deja au max
			if(((Potion)inventory.getRaccourci1()).getEffet()==0 && pointsDeVie<this.getPVMax())
			{
				int bonus = ((Potion)inventory.getRaccourci1()).getBonus();
				this.ajouterVie(bonus);
				inventory.remove(inventory.getRaccourci1());
			}
			//si potion de mana et pas mana au max
			else if(((Potion)inventory.getRaccourci1()).getEffet()==1 && pointsMana<this.getManaTotal()) 
			{
				int bonus = ((Potion)inventory.getRaccourci1()).getBonus();
				this.ajouterMana(bonus);
				inventory.remove(inventory.getRaccourci1());	
			}
			return;
		}
	}
	
	/**
	 * appele si on appuie sur O, qui est la touche raccourci de la 2e fonction de potion. 
	 * consomme la potion mise a cet emplacement s'il y en a une.
	 */
	public void utiliserPotion2()
	{
		if(inventory.getRaccourci2()!=null && inventory.getRaccourci2() instanceof Potion)
		{
			//si potion de vie et que vie pas deja au max
			if(((Potion)inventory.getRaccourci2()).getEffet()==0 && pointsDeVie<this.getPVMax())
			{
				int bonus = ((Potion)inventory.getRaccourci2()).getBonus();
				this.ajouterVie(bonus);
				inventory.remove(inventory.getRaccourci2());
			}
			//si potion de mana et pas mana au max
			else if(((Potion)inventory.getRaccourci2()).getEffet()==1 && pointsMana<this.getManaTotal()) 
			{
				int bonus = ((Potion)inventory.getRaccourci2()).getBonus();
				this.ajouterMana(bonus);
				Anim.addTextDisp(new TexteTemporaire("text"+String.valueOf(this.getX()), this.getX(), this.getY(),
					this.getWidth(), this.getHeight(), false, 500, String.valueOf(bonus), 1));
				inventory.remove(inventory.getRaccourci2());	
			}
			return;
		}
	}
	
	public void equiper(Armure nouvelle)
	{
		inventory.setArmure(nouvelle);
	}
	
	public void equiper(Arme nouvelle)
	{
		inventory.setArme(nouvelle);
	}
	
	public void equiper(int n, Bijoux b)
	{
		inventory.setBijoux(n,b);
	}

	/**
	 * verifie quelques details, pratique pour le debug d'inventaire
	 */
	public void gueulard()
	{
		for(Item e : inventory.getInventory().stream().filter(x -> x!=null).collect(Collectors.toList()))
		{
			if(GameWorld.getEntites().containsKey(e.getNom()))
			{
				System.out.println("PROBLEME : DANS INVENTAIRE ET AU SOL : " + e);
			}
			if(!inventory.getInventory().contains(inventory.getArme()) && inventory.getArme()!=null)
			{
				System.out.println("ARME PAS DANS INVENTAIRE");
			}
			if(!inventory.getInventory().contains(inventory.getArmure()) && inventory.getArmure()!=null)
			{
				System.out.println("ARMURE PAS DANS INVENTAIRE");
			}
			for(int i =0; i<inventory.getBijoux().length; i++)
			{
				if(!inventory.getInventory().contains(inventory.getBijoux()[i]) && inventory.getBijoux()[i]!=null)
					System.out.println("BIJOU " + i + " EQUIPE MAIS PAS DANS L'INVENTAIRE");
			}
			
		}
	}
	
	/**
	 * met une bombe au sol si on a une dans l'iventaire, et l'allume.
	 */
	public void poserBombe()
	{
		for(Item i : inventory.getInventory())
		{
			if( i instanceof Bombe)
			{
				Item a = inventory.remove(i);
				GameWorld.addEntite(a);
				((Bombe)a).utiliser();
				return;
			}
		}
	}
	public Arme getArme()
	{
		return inventory.getArme();
	}
	public Armure getArmure()
	{
		return inventory.getArmure();
	}
	public Bijoux[] getBijoux()
	{
		return inventory.getBijoux();
	}
	public int getXP()
	{
		return experience;
	}
	public int getNiveau()
	{
		return niveau;
	}
	public void setDureeAttaque(int temps)
	{
		dureeAttaque=temps;
	}
	public int getDureeAttaque()
	{
		return dureeAttaque;
	}
	
	/**
	 * verifie si le joueur frappe actuellement, et si oui met a jour l'attaque en question.
	 */
	public void actualiserCoup()
	{
		if(this.frappeActuellement())
		{
			if(quelleAttaque==1)
			{
				if(inventory.getArme()!=null)
				{
					inventory.getArme().attaque1(this);
					this.setAutoriseABouger(false);
				}
			}
			else if(quelleAttaque==2)
			{
				if(inventory.getArme()!=null)
				{
					inventory.getArme().attaque2(this);
					if(inventory.getArme() instanceof ArmeCac)
						this.setAutoriseABouger(false);
				}
			}
		}
		else
			this.setAutoriseABouger(true);

		tempsAttaqueActuelle+=GameWorld.TEMPS_RAFRAICHISSEMENT;
	}

	/**
	 * 	collision du joueur avec les monstres
	 *  si il y a collision, l'entite attaque le joueur(sauf si c'est le dragon, sinon il ne serait pas vraiment battable)
	 */
	public void collisionMonstre()
	{
		List<Monstre> lesMonstres = GameWorld.getMonstres();
		for(int i=0; i<lesMonstres.size(); i++)
		{
			Monstre m = lesMonstres.get(i);
			//si c'est le dragon, pas de collision au corps a corps
			if(Hitbox.collision(m.getHB(), this.getHB()) && !(m instanceof Dragon))
			{
				m.attaquer(this);
			}
		}
	}
	
	/**
	 * se positionne dans le nouveau monde, pour commencer au debut du niveau ou justement en plein milieu pour le hub ou le chateau.
	 * @param quelMonde
	 */
	public void sePositionner(int quelMonde)
	{
		if(quelMonde==Maps.Map.Base.val())
		{
			this.setPosition(new Position(GameWorld.getWidthGrille(),GameWorld.getHeightGrille()/2));
		}
		else if(quelMonde==Maps.Map.Chateau.val())
		{
			this.setPosition(new Position(GameWorld.getWidthGrille()*3,GameWorld.getHeightGrille()/2));
		}
		else
		{
			this.setPosition(new Position(GameWorld.getWidthGrille()/10,GameWorld.getHeightGrille()/2));
		}
		
			
	}
	
	/**
	 * regarde si une porte ouverte est a proximite, sioui, la prend et change de monde.
	 */
	public void changerMonde()
	{
		{
			List<PorteMagique> lesPortes = GameWorld.getEntites().values().stream()
					.filter(PorteMagique.class::isInstance).map(PorteMagique.class::cast)
					.collect(Collectors.toList());
		
			for(PorteMagique porte : lesPortes)
			{

				if(Hitbox.collision(this.getHB(), porte.getHB()) && porte.isOpen())
				{
					GameWorld.changerMonde(porte.getTypePorte());
				}

			}
		}
	}
	
	/**
	 * des getters pour le total de nos caracteristiques, qui regroupe la base, les bonus equipement, et les bonus classe.
	 * @return le total
	 */
	
	
	public double getReculTotal()
	{
		return this.recul*(double)((this.reculBonus/100 +this.bonusReculClasse/100)+1);
	}
	public double getVitesseTotale()
	{
		return this.vitesseP*(double)((this.vitessePBonus/100 + this.bonusVitesseClasse/100)+1);
	}
	public int getDefenseTotale()
	{
		return this.pointsDeDefense+this.pointsDeDefenseBonus+this.bonusArmureClasse;
	}
	public int getAttaqueTotale()
	{
		return this.pointsDAttaque+this.pointsDAttaqueBonus+this.bonusDegatsClasse;
	}
	public int getPVMax()
	{
		return this.pointsDeVieMax+this.pointsDeVieMaxBonus+this.bonusPVClasse;
	}
	public double getVitesseAttaque()
	{
		return 1*(double)((this.vitesseAttaque/100 +this.bonusVitesseAttaqueClasse/100)+1);
	}
	public double getVolVieTotal()
	{
		return (double)(this.volVieBonus/100);
	}
	public double getGainXPTotal()
	{
		return 1*(double)((this.gainXPBonus/100)+1);
	}
	public double getChanceTotale()
	{
		return this.chanceBonus;
	}
	public int getManaTotal()
	{
		return this.manaBonus + this.pointsManaMax + this.bonusManaClasse;
	}
	public double getEsquiveTotale()
	{
		return this.bonusEsquiveClasse/100;
	}
	
	/**
	 * ajoute de l'xp, et nous fait passer le nivea si on en a obtenu assez
	 * @param xp
	 */
	public void addXp(int xp)
	{
		this.experience+=(xp*this.getGainXPTotal());
		if(this.experience>this.experienceRequise)
			up();
	}
	
	public void up()
	{
		if(this.getNiveau()<10)
		{
			experience-=experienceRequise;
			this.niveau++;
			experienceRequise = (int)(100*1.5) * (this.getNiveau()+1);
		}
		if(this.getNiveau()==1)
		{
			choisirClasse();
		}
		if(niveau==10) //on reste au niveau 10, ou bonne chance
			experienceRequise*=10000;
		updateBonusClasse();
		this.ajouterVie(this.getPVMax());
		this.ajouterMana(this.getManaTotal());
		}
	
	/**
	 * ajoute de la vie a notre stock, avec pour limite le max et indique combien a ete ajoute si c'est superieur a 1
	 * @param vie la vie a ajouter
	 */
	public void ajouterVie(int vie)
	{
		if(pointsDeVie+vie>this.getPVMax())
			vie=this.getPVMax()-pointsDeVie;
		this.pointsDeVie+=vie;
		if(vie>1)
		Anim.addTextDisp(new TexteTemporaire("text"+String.valueOf(this.getX()), this.getX(), this.getY(),
				this.getWidth(), this.getHeight(), false, 500, String.valueOf(vie), 1));


	}
	/**
	 * ajoute du mana a notre stock, avec pour limite le max et indique combien a ete ajoute si c'est stuperireur a 1
	 * @param mana le mana a ajouter
	 */
	public void ajouterMana(int mana)
	{
		if(pointsMana+mana>this.getManaTotal())
			mana=this.getManaTotal()-pointsMana;
		this.pointsMana+=mana;
		if(mana>1)
		Anim.addTextDisp(new TexteTemporaire("text"+String.valueOf(this.getX()), this.getX(), this.getY(),
			this.getWidth(), this.getHeight(), false, 500, String.valueOf(mana), 1));
	}
	
	public void setAutoriseABouger(boolean auth)
	{
		this.autoriseABouger=auth;
	}
	
	public boolean getAutoriseABouger()
	{
		return autoriseABouger;
	}
	
	/**
	 * affiche la bar de vie, mana et XP
	 */
 	public void afficherBarInfos()
	{
		double vieActuelle=this.getPointsDeVie();
		double manaActuel = this.pointsMana;
		
		if(vieActuelle<0)
			vieActuelle=0;
		if(manaActuel<0)
			manaActuel=0;
		//dessine la vie
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.filledRectangle(0.095, 0.97, 0.191*vieActuelle/this.getPVMax(), 0.01);
		
		//dessine le mana
		StdDraw.setPenColor(StdDraw.BLUE);
		StdDraw.filledRectangle(0.095, 0.947, 0.191*manaActuel/this.getManaTotal(), 0.01);
		
		//dessine l'exp
		StdDraw.setPenColor(StdDraw.GREEN);
		StdDraw.filledRectangle(0.095, 0.925, 0.191*this.experience/this.experienceRequise, 0.01);
		
		//dessine l'enveloppe
		StdDraw.picture(0.15,
				0.95,
				Anim.bonneImage(Images.Entites.Bar.val()),
				0.3,
				0.1,
				0);
		
		//dessine les niveaux
		StdDraw.setPenColor(StdDraw.GREEN);
		StdDraw.text(0.26,
				0.98,
				String.valueOf(this.getNiveau()));
	}
 	/**
 	 * une methode speciale est cree pour l'arme car elle est necessaire plus souvent que les autres equipements,
 	 * notamment quand on veut faire 1/3 des degats ou autre.
 	 */
	public void updateDegatsArme()
	{
		for(Bijoux b : inventory.getBijoux())
		{
			pointsDAttaqueBonus=0;
			if(b!=null)
			{
				if (b.getTypeBonus() == Bijoux.TypesBonus.attaque.val())
					pointsDAttaqueBonus+=b.getBonus();
			}
		}
		if(inventory.getArme()!=null)
			pointsDAttaqueBonus+=inventory.getArme().getDegats();
	}
	
	public void ajouterVieVolee(int degats)
	{
		this.ajouterVie((int)(degats*this.getVolVieTotal()));
	}
	
	public boolean retirerMana(int mana)
	{
		if(mana>this.pointsMana)
			return false;
		this.pointsMana-=mana;
		return true;
	}
	
	/**
	 * change la classe du perso, sera appele quand le niveau du joueur passera a 1
	 * @param typePerso la nouvelle classe
	 */
	public void setTypePersonnage(int typePerso)
	{
		if(typePerso>=0 && typePerso<=3)
		{
			typePersonnage=typePerso;
			inventory.setClasseJoueur(typePersonnage);
		}
	}
	public int getTypePerso()
	{
		return typePersonnage;
	}
	
	public void acheter(Item i, int prix)
	{
		if(i instanceof Consommable)
		{
			((Consommable)i).setNombre(5);
		}
		inventory.acheter(i, prix);
		
	}
	public void vendre(Item i, int prix)
	{
		inventory.vendre(i,  prix);
	}
	public LinkedList<Item> getInventaireNonEquipe()
	{
		return this.inventory.getItemsNonEquipes();
	}
	public int getRichesse()
	{
		return inventory.getMoney();
	}
	public void updateClasse(int classe)
	{
		inventory.setClasseJoueur(classe);
	}
	public int getClasseJoueur()
	{
		return typePersonnage;
	}
	
	/**
	 * cree les bonus relatifs au niveau et a la classe du joueur, pour les ajouter a notre total de caracteristiques
	 */
	public void updateBonusClasse()
	{
		if(getClasseJoueur()==3)
			return;
		bonusDegatsClasse=0;
		bonusArmureClasse=0;
		bonusPVClasse=0;
		bonusManaClasse=0;
		bonusVitesseAttaqueClasse=0;
		bonusVitesseClasse=0;
		bonusEsquiveClasse=0;
		bonusReculClasse=0;
		if(getClasseJoueur()==2)
		{
			//si premier gros bonus
			if(this.getNiveau()>=4)
			{
				bonusDegatsClasse+=20;
				bonusPVClasse+=50;
			}
			//deuxieme gros bonus
			if(this.getNiveau()>=7)
			{
				bonusVitesseAttaqueClasse+=15;
				bonusArmureClasse+=20;
			}
			//troisieme et dernier gros bonus
			if(this.getNiveau()>=10)
			{
				bonusDegatsClasse+=50;
				bonusArmureClasse+=30; //?
			}
			bonusPVClasse+=10*this.getNiveau();
			bonusDegatsClasse+=5*this.getNiveau();
		}
		else if(getClasseJoueur()==1)
		{
			//si premier gros bonus
			if(this.getNiveau()>=4)
			{
				bonusDegatsClasse+=10;
				bonusEsquiveClasse+=5;
			}
			//deuxieme gros bonus
			if(this.getNiveau()>=7)
			{
				bonusVitesseClasse+=15;
				bonusDegatsClasse+=10;  //?
			}
			//troisieme et dernier gros bonus
			if(this.getNiveau()>=10)
			{
				bonusVitesseAttaqueClasse+=50;
				bonusDegatsClasse+=50;
			}
			bonusEsquiveClasse+=5*this.getNiveau();
			bonusDegatsClasse+=10*this.getNiveau();
		}
		else if(getClasseJoueur()==0)
		{
			//si premier gros bonus
			if(this.getNiveau()>=4)
			{
				bonusArmureClasse+=20;
				bonusDegatsClasse+=60;
			}
			//deuxieme gros bonus
			if(this.getNiveau()>=7)
			{
				bonusVitesseAttaqueClasse+=25;
				bonusDegatsClasse+=20;
			}
			//troisieme et dernier gros bonus
			if(this.getNiveau()>=10)
			{
				bonusPVClasse+=100;
				bonusReculClasse+=50;
				bonusManaClasse+=1000; //?
			}
			bonusManaClasse+=10*this.getNiveau();
			bonusDegatsClasse+=10*this.getNiveau();
		}
	}
	
	public void choisirClasse()
	{
		SelectionClasse.interagir();
	}
}
