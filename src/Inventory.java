import java.awt.Font;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class Inventory
{
	//Les equipements sont references autrement que par la LinkedList de l'inventaire, mais sont aussi dedans.
	protected Arme arme;
	protected Armure armure;
	protected Bijoux[] bijoux;
	protected Potion potionRaccourci1;
	protected Potion potionRaccourci2;
	
	//l'inventaire du joueur
	private LinkedList<Item> inventory;
	private LinkedList<Item> inventaireTrie;
	private int money;
	
	//pour l'affichage des items et de la suibrillance dans l'inventaire
	//premiere dimention : 0-positionX   1-positionY   2-largeur   3-hauteur
	//deuxieme dimention : les differents endroits/items
	private double[][] positionsDessinItems;
	private double[][] positionsDessinSurbrillance;
	private int objetSelectionne;
	
	private int joueurDeQuelleClasse;
	
	/**
	 * Constructeur de l'inventaire, initialise tout a null et la classe du joueur, qui sert a savoitr quelle arme il peut porter
	 * @param taille
	 */
	public Inventory(int classeJoueur)
	{
		inventory = new LinkedList<Item>();
		inventaireTrie = new LinkedList<Item>();
		money=0;
		setArme(null);
		setArmure(null);
		bijoux = new Bijoux[5];
		for(int i=0; i<bijoux.length; i++)
			setBijoux(i, null);
		potionRaccourci1 = null;
		potionRaccourci2 = null;
		positionsDessinItems = new double[4][19];
		positionsDessinSurbrillance = new double[4][19];
		initPositionDessins();
		objetSelectionne=-1;
		joueurDeQuelleClasse=classeJoueur;
	}
	 /*
	  * l'inventaire se presente sous cette forme la : (en affichage graphique)
	  * chiffres : la position dans InventaireTrie des items
	  * 		
	  * 		12			
	  * 	17	13			9	0	1	2
	  * 		14			10	3	4	5	
	  * 	18	15			11	6	7	8
	  * 		16
	  */
	
	
	public void addMoney(int howMuch)
	{
		money+=howMuch;
	}
	/**
	 * fonction pour acheter des items
	 * @param howMuch
	 * @return renvoie true si le joueur a assez, et le debite
	 */
	public boolean pay(int howMuch)
	{
		if(money<howMuch)
			return false;
		money-=howMuch;
		return true;
	}
	
	/**
	 * ajoute un item dans l'iventaire et en paie le prix
	 * @param item
	 * @param prix
	 */
	public void acheter(Item item, int prix)
	{
		if(prix<=this.money && this.placePour(item))
		{
			this.add(item);
			this.pay(prix);
		}
	}
	/**
	 * supprime l'item de l'inventaire et ajoute le prix a notre bourse, suppose que l'inventaire contient bien l'item (assure dans Marchand)
	 * @param item
	 * @param prix
	 */
	public void vendre(Item item, int prix)
	{
		this.remove(item);
		this.addMoney(prix);
	}
	
	public LinkedList<Item> getInventory()
	{
		return inventory;
	}
	
	/**
	 * recopie l'inventaire dans l'inventaire trie pour faciliter l'affichage et la selection dans l'inventaire
	 * comme represente precedemment
	 */
	private void recopierInventaire()
	{
		inventaireTrie=new LinkedList<Item>();
		for(Item i : inventory)
		{
			if(i != arme && i!=null && i!= armure && i!=bijoux[0] && i!=bijoux[1] && i!=bijoux[2]
					&& i!=bijoux[3]  && i!=bijoux[4] && i!=potionRaccourci1 && i!= potionRaccourci2)
			{
				inventaireTrie.add(i);
			}
		}
		int nombreNull = 9-inventaireTrie.size();
		for(int i=0; i<nombreNull; i++)
		{
			inventaireTrie.add(null); // ajoutre en null les places qu'il reste dans l'espace de droite qui n'a pas ete occupe par les items
		}
		inventaireTrie.add(null);//la piece, endroit null car la piece n'est pas un item
		inventaireTrie.add(potionRaccourci1);
		inventaireTrie.add(potionRaccourci2);
		for(int i=0; i<bijoux.length; i++)
			inventaireTrie.add(bijoux[i]);
		inventaireTrie.add(arme);
		inventaireTrie.add(armure);

	}
	/**
	 * ajoute un item a l'inventaire, tout en verifiant la place disponible et si on l'a deja etc
	 * @param i l'item a ajouter
	 * @return true si on l'a bien ajoute, false sinon
	 */
	public boolean add(Item i)
	{
		if(i instanceof Money)
		{
			//ici j'ajoute l'argent au stock d'argent, puis supprime l'Item Money
			//du GameWorld car l'objet n'existe plus
			money+=((Money)i).getQuantite();
			return true;
		}
		else if(i instanceof Potion)
		{
			for(Item a : inventory) //regarde wi il existe deja une potion dans l'inventaire
			{
				if(a instanceof Potion)
				{
					if(((Potion)a).getEffet() == ((Potion)i).getEffet() &&  ((Potion)a).getBonus() == ((Potion)i).getBonus())
					{
						((Potion)a).ajouterStock(((Potion)i).getNombre());
						return true;
					}
				}
			}
		}
		else if(i instanceof Bombe)
		{
			if( ((Bombe)i).getOnFire())
			{
				return false;
			}
			for(Item a : inventory)
			{
				if(a instanceof Bombe)
				{
					((Bombe)a).ajouterStock(((Bombe)i).getNombre());
					return true;
				}
			}
		}
		if(placePour(i))
		{
			inventory.add(i);
			return true;
		}
		else
		{
			System.out.println("inventaire plein ! ");
			return false;
		}
	}
	/**
	 * supprime un item de l'inventaire, ou si on l'a en plusieurs exemplaires, en supprime 1
	 * @param i l'item a supprimer
	 * @return l'item supprime, null si on ne l'avait pas
	 */
	public Item remove(Item i)
	{
		if(i instanceof Potion)
		{
			((Potion)i).utiliserUnite();
			if(((Potion)i).getNombre()<=0)
			{
				inventory.remove(i);
				((Potion) i).setNombre(1);
				syncVar();
				return i;
			}
			else
			{
				Potion nouvellePotion =new Potion((Potion)i);
				nouvellePotion.setNombre(1);
				syncVar();
				return nouvellePotion;
			}
		}
		else if(i instanceof Bombe)
		{
			((Bombe)i).utiliserUnite();
			if(((Bombe)i).getNombre()<=0)
			{
				inventory.remove(i);
				syncVar();
				return i;
			}
			else
			{
				Bombe nouvelleBombe = new Bombe((Bombe)i);
				nouvelleBombe.setNombre(1);
				syncVar();
				return nouvelleBombe;
			}
		}
		if(inventory.remove(i))
		{
			syncVar();
			return i;
		}
		else
			return null;
	}

	/**
	 * Met a jour les variables de l'inventaire pour ne pas avoir d'objets en double.
	 */
	private void syncVar()
	{
		if(!inventory.contains(arme))
			setArme(null);
		if(!inventory.contains(armure))
			setArmure(null);
		for(int i=0; i<bijoux.length; i++)
		{
			if(!inventory.contains(bijoux[i]))
				bijoux[i]=null;
		}
		if(!inventory.contains(potionRaccourci1))
			potionRaccourci1=null;
		if(!inventory.contains(potionRaccourci2))
			potionRaccourci2=null;
	}

	
	//retourne la taille utilisee de l'inventaire, moins ce qui est equipe
	public int getSize()
	{
		int retour = 0;
		for(int i=0; i<inventory.size(); i++)
		{
			if(inventory.get(i)!=null)
				retour++;
		}
		if(arme!=null)
			retour--;
		if(armure!=null)
			retour--;
		for(int i=0; i<bijoux.length; i++)
		{
			if(bijoux[i]!=null)
				retour--;
		}
		if(potionRaccourci1!=null)
			retour--;
		if(potionRaccourci2!=null)
			retour--;
		return retour;
	}
	
	//retourne la taille de l'inventaire - la taille des equipements (arme + armure + 5 bijoux + 2 emplacements potion )
	public int getMaxSize()
	{
		return 9;
	}
	
	// affiche l'inventaire
	public void affiche(int[] positionCurseur)
	{
		while(!processKeysInventory(positionCurseur))
		{
			StdDraw.clear();
			
			dessine(positionCurseur[0]);
	
			// Montre la fenetre graphique mise a jour et attends 20 millisecondes
			StdDraw.show();
		}

	}
	
	/**
	 * reagit aux touches pressees par l'utilisateur, permet de se deplacer dans l'inventaire
	 * @param positionCurseur la postition du curseur actuellement
	 * @return true si on ferme l'inventaire
	 */
	public boolean processKeysInventory(int[] positionCurseur)
	{
		//System.out.println(Keys.getKeys().entrySet());

		if(Keys.getKey(Keys.Touches.i.val()) && !Keys.getPreviousKey(Keys.Touches.i.val()))
		{
			return true;
		}
		if(Keys.getKey(Keys.Touches.escape.val()) && !Keys.getPreviousKey(Keys.Touches.escape.val()))
		{
			return true;
		}		
		if(Keys.getKey(Keys.Touches.returnKey.val()) && !Keys.getPreviousKey(Keys.Touches.returnKey.val()))
		{
			if(objetSelectionne<0)
				objetSelectionne=positionCurseur[0];
			else
				action(objetSelectionne, positionCurseur[0]);
		}
		if(Keys.appuiBas())
		{
			bougerCurseur(0, positionCurseur);
		}
		if(Keys.appuiHaut())
		{
			bougerCurseur(2, positionCurseur);
		}
		if(Keys.appuiDroite())
		{
			bougerCurseur(1, positionCurseur);
		}
		if(Keys.appuiGauche())
		{
			bougerCurseur(3, positionCurseur);
		}
		
			Keys.sync();
			return false;
	}
		
	/**
	 * dessine l'inventaire, donc l'image de base, les items, et les cadres de surbrillance
	 * @param positionCurseur
	 */
	public void dessine(int positionCurseur)
	{
		StdDraw.picture(0.5,
				0.5,
				Anim.bonneImage(Images.Entites.Inventaire.val()),
				1,
				1,
				0); 

		if(objetSelectionne<positionsDessinSurbrillance[0].length)
		{
			StdDraw.picture(positionsDessinSurbrillance[0][positionCurseur],
					positionsDessinSurbrillance[1][positionCurseur],
					Anim.bonneImage(Images.Entites.Surbrillance.val()),
					positionsDessinSurbrillance[2][positionCurseur],
					positionsDessinSurbrillance[3][positionCurseur],
					0);
		}
		
		if(objetSelectionne>=0 && objetSelectionne<positionsDessinSurbrillance[0].length)
		{
			StdDraw.picture(positionsDessinSurbrillance[0][objetSelectionne],
					positionsDessinSurbrillance[1][objetSelectionne],
					Anim.bonneImage(Images.Entites.Surbrillance.val()),
					positionsDessinSurbrillance[2][objetSelectionne],
					positionsDessinSurbrillance[3][objetSelectionne],
					0);
		}
		
		//permet d'afficher les items a l'endroit desire
		recopierInventaire();
		for(int i=0; i<inventaireTrie.size(); i++)
		{

			if(inventaireTrie.get(i)!=null)
			{
				double changement0=7*positionsDessinItems[2][i]/10;
				double changement1=5*positionsDessinItems[3][i]/10;
				//si c'est une arme, change sa taille pour l'afficher correctement.
				if(inventaireTrie.get(i) instanceof ArmeCac)
				{					
					positionsDessinItems[0][i]-=changement0;
					positionsDessinItems[1][i]-=changement1;
					positionsDessinItems[2][i]=positionsDessinItems[2][i]*27/10;
					positionsDessinItems[3][i]=positionsDessinItems[3][i]*27/10 *2/3;
				}
				StdDraw.picture(positionsDessinItems[0][i],
							positionsDessinItems[1][i],
							Anim.bonneImage(inventaireTrie.get(i).getNumEntite()),
							positionsDessinItems[2][i],
							positionsDessinItems[3][i],
							0);
				afficherQuantite(i);
				//puis remet sa taille initiale.
				if(inventaireTrie.get(i) instanceof ArmeCac)
				{
					positionsDessinItems[0][i]+=changement0;
					positionsDessinItems[1][i]+=changement1;
					positionsDessinItems[2][i]=positionsDessinItems[2][i]*10/27;
					positionsDessinItems[3][i]=positionsDessinItems[3][i]*10/27 *3/2;
				}
			}
		}
		afficheCaracteristiquesItem(positionCurseur);
		dessineLeP();
		dessineLeO();
		dessineNombreThunes();
		dessineLaBar();
		afficheDescriptionPerso();
		afficheEmbleme();
	}
	
	public void dessineNombreThunes()
	{
		StdDraw.text(0.685,
				0.43,
				String.valueOf(this.money));
	}
	
	public void afficheEmbleme()
	{
		if(this.joueurDeQuelleClasse>=0 && this.joueurDeQuelleClasse<=2)
		{
			
		int quelEmbleme=-1;
		if(joueurDeQuelleClasse==0)
			quelEmbleme = Images.Entites.EmblemeMago.val();
		else if(joueurDeQuelleClasse==1)
			quelEmbleme = Images.Entites.EmblemeDist.val();
		else if(joueurDeQuelleClasse==2)
			quelEmbleme = Images.Entites.EmblemeBast.val();
		
		StdDraw.picture(0.29,
				0.415,
				Anim.bonneImage(quelEmbleme),
				0.04,
				0.04*3/2,
				0);
		}
	}
	
	public void afficheDescriptionPerso()
	{
		Joueur leJoueur = GameWorld.getJoueur();
		StdDraw.setPenColor(StdDraw.BLACK);
		
		StdDraw.textLeft(0.03,
				0.9,
				"Notre fier Jacob possede des caracteristiques hors du commun :");
		
		StdDraw.textLeft(0.1, 0.83, "Attaque : " + leJoueur.getAttaqueTotale());
		StdDraw.textLeft(0.1, 0.8, "Defense : " + leJoueur.getDefenseTotale());
		StdDraw.textLeft(0.1, 0.77, "VieMax : " + leJoueur.getPVMax());
		StdDraw.textLeft(0.1, 0.74, "ManaMax : " + leJoueur.getManaTotal());
		StdDraw.textLeft(0.1, 0.71, "Vitesse : " + ((int)((leJoueur.getVitesseTotale()*10000))) /(double)100 );
		StdDraw.textLeft(0.35, 0.83, "VitesseAttaque : " +((int)((leJoueur.getVitesseAttaque()*10000))) /(double)100    );
		StdDraw.textLeft(0.35, 0.8, "Recul : " + leJoueur.getReculTotal());
		StdDraw.textLeft(0.35, 0.77, "GainXP : " + leJoueur.getGainXPTotal()*100 +"%");
		StdDraw.textLeft(0.35, 0.74, "Chance : " + leJoueur.getChanceTotale());
		StdDraw.textLeft(0.35, 0.71, "VolVie : " + leJoueur.getVolVieTotal()*100 + "%");
		StdDraw.textLeft(0.1, 0.68, "Esquive : " + ((int)((leJoueur.getEsquiveTotale()*10000))) /(double)100 + "%");

}
	
	public void afficherQuantite(int i)
	{
		Item lObjet = inventaireTrie.get(i);
		if(lObjet!=null && lObjet instanceof Consommable)
		{
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.text(positionsDessinItems[0][i]+positionsDessinItems[2][i]/2,
				positionsDessinItems[1][i]-6*positionsDessinItems[3][i]/10,
				String.valueOf(((Consommable)(inventaireTrie.get(i))).getNombre()));
		}
		if(lObjet!=null && lObjet instanceof Bombe)
			StdDraw.text(positionsDessinItems[0][i]+positionsDessinItems[2][i]/2,
					positionsDessinItems[1][i]+4*positionsDessinItems[3][i]/10,
					"L");
		
	}
	private static void dessineLeP()
	{
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.text(0.712,
				0.292,
				"P");
	}
	
	private static void dessineLeO()
	{
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.text(0.712,
				0.174,
				"O");
	}
	/**
	 * dessine les infos sur la vie, le mana, et lexperience.
	 */
	private static void dessineLaBar()
	{
		double vieActuelle=GameWorld.getJoueur().getPointsDeVie();
		double manaActuel = GameWorld.getJoueur().pointsMana;
		
		//securite
		Joueur joueur = GameWorld.getJoueur();
		if(vieActuelle<0)
			vieActuelle=0;
		if(manaActuel<0)
			manaActuel=0;
		//dessine la vie
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.textLeft(0.07,
				0.45,
				"HP : " + String.valueOf(joueur.getPointsDeVie()+"/"+joueur.getPVMax()));
		
		//dessine le mana
		StdDraw.setPenColor(StdDraw.BLUE);
		StdDraw.textLeft(0.07,
				0.42,
				"MANA : " + String.valueOf(joueur.pointsMana+"/"+joueur.getManaTotal()));
		
		//dessine l'exp
		StdDraw.setPenColor(StdDraw.GREEN);
		StdDraw.textLeft(0.07,
				0.39,
				"LVL : " + String.valueOf(joueur.getNiveau()));
		//dessine l'exp
		StdDraw.setPenColor(StdDraw.GREEN);
		StdDraw.textLeft(0.04,
				0.1,
				String.valueOf(joueur.getXP())+"/"+String.valueOf(joueur.experienceRequise));
		
	}
	
	/**
	 * Affiche les caracteristiques de l'item sur lequel le curseur est pose.
	 * @param positionCurseur
	 */
	private void afficheCaracteristiquesItem(int positionCurseur)
	{
		StdDraw.setPenColor(StdDraw.BLACK);
		Font oldFont = StdDraw.getFont();
		StdDraw.setFont(new Font("Arial", Font.PLAIN, 14));
		//si pas d'item, n'affiche rien
		if(inventaireTrie.get(positionCurseur)==null)
		{
			if(positionCurseur==10)
			{
				StdDraw.textLeft(0.75,
						0.7,
						"Mettre Potion ici pour");
				StdDraw.textLeft(0.75,
						0.67,
						"utiliser avec \"P\"");
			}
			if(positionCurseur==11)
			{
				StdDraw.textLeft(0.75,
						0.7,
						"Mettre Potion ici pour");
				StdDraw.textLeft(0.75,
						0.67,
						"utiliser avec \"O\"");
			}
			if(positionCurseur==9)
			{
				StdDraw.text(0.85,
						0.92,
						"Categorie : Thunes");
				
				StdDraw.textLeft(0.75,
						0.87,
						"Je possede " + this.getMoney() + " Or");
				
				
				StdDraw.textLeft(0.75,
						0.81,
						" Est-ce beaucoup ? ");
				
				StdDraw.textLeft(0.75,
						0.78,
						" Quel est le sens de ");
				StdDraw.textLeft(0.75,
						0.75,
						" la vie ?");
				
				
			}
		}
		else
		{
			//sinon sauvegarde la font, et decoupe le texte en plusieures parties de 20 caracteres
			//pour afficher dans l'espace reserve a cet effet
			String description1 = inventaireTrie.get(positionCurseur).getTexteDescriptif()[1];
			StdDraw.text(0.85,
					0.92,
					inventaireTrie.get(positionCurseur).getTexteDescriptif()[0]);

			LinkedList<String> description= new LinkedList<String>();
			int i;
			for(i=20; i<description1.length(); i+=20)
			{
				description.add(description1.substring(i-20, i));

			}
			description.add(description1.substring(i-20));

			for(int nombreChaines=0; nombreChaines<description.size(); nombreChaines++)
			{
				StdDraw.textLeft(0.75,
						0.87-0.03*nombreChaines,
						description.get(nombreChaines));
			}
		}
		
		StdDraw.setFont(oldFont);
	}
	
	/**
	 * permet de gerer l'echange de deux objets dans l'inventaire,
	 * comme le changement d'arme ou la selection de potions en raccourci
	 * @param objet1
	 * @param objet2
	 */
	private void action(int objet1, int objet2)
	{
		if(objet1==objet2)
		{
			objetSelectionne=-1;
			return;
		}
			//je repere les deux objets a echanger
		Item object1 = inventaireTrie.get(objet1);
		Item object2 = inventaireTrie.get(objet2);
		objetSelectionne=-1;
		//vu le nombre de manipulations, c'est plus prudent
		try
		{	
			//boucle pour tout tester en echangeant les objets, sans avoir a tout retaper.
			for(int sensDesObjets=0; sensDesObjets<2; sensDesObjets++)
			{
				if(sensDesObjets==1)
				{
					Item temp=object2;
					object2=object1;
					object1=temp;
					int temp2=objet1;
					objet1=objet2;
					objet2=temp2;
				}
				if((objet1<9 && objet2<9)) //les deux objets sont dans l'inventaire de droite (non equipes)
				{
					echangerPlaces(object1, object2);
					return;
				}
				//Si le joueur a voulu changer d'armure
				else if(objet1==18 && (object2==null || object2 instanceof Armure))
				{
					if(setArmure((Armure)object2))
						echangerPlaces(object1, object2);
					return;
				}
				//Si le joueur veut changer d'arme
				else if(objet1==17 && (object2==null || object2 instanceof Arme))
				{
					if(setArme((Arme)object2))
						echangerPlaces(object1, object2);
					return;
				}
				//si le joueur veut changer de bijoux
				else if(objet1>=12 && objet1<=16 && (object2==null || object2 instanceof Bijoux))
				{
					//bijoux[0,1,2]etc
					if(setBijoux(-12+objet1, (Bijoux)object2))
			//		bijoux[-12+objet1]=(Bijoux)object2;
						echangerPlaces(object1, object2);
					return;
				}
				//le joueur veut changer de potions raccourci
				else if(objet1>=10 && objet1<=11 && (object2==null || object2 instanceof Potion))
				{
					if(objet1==10)
						potionRaccourci1=(Potion)object2;
					else if(objet1==11)
						potionRaccourci2=(Potion)object2;
					echangerPlaces(object1, object2);
					return;
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("rate, ce genre de choses arrive (quand?)");
			return;
		}
		
	}
	 /*
	  * l'inventaire se presente sous cette forme la :
	  * 		
	  * 		12			
	  * 	17	13			9	0	1	2
	  * 		14			10	3	4	5	
	  * 	18	15			11	6	7	8
	  * 		16
	  */
	
	
	/**
	 * echange la place de deux items en inventaire, meme avec un null.
	 * @param a premier item
	 * @param b deuxieme item
	 */
	private void echangerPlaces(Item a, Item b)
	{
		LinkedList<Item> nouvelle= new LinkedList<Item>();
		//utile pour ne pas recopier plein de fois un meme item dans le cas ou l'un des items est null
		boolean changementAFait=false;
		boolean changementBFait=false;
		for(int i=0; i<inventaireTrie.size(); i++)
		{
			Item itemActuel=inventaireTrie.get(i);
			if((itemActuel!=b && itemActuel!=a) || (changementAFait && changementBFait))
			{
				nouvelle.add(inventaireTrie.get(i));
			}
			else if(itemActuel==a && !changementAFait)
			{
				changementAFait=true;
				nouvelle.add(b);
			}
			else if(itemActuel==b && !changementBFait)
			{
				changementBFait=true;
				nouvelle.add(a);
			}
		}

		inventory = new LinkedList<Item>();
		inventory.addAll(nouvelle);
		GameWorld.getJoueur().updateBonus();
	}
	
	/**
	 * Bouge le curseur de l'inventaire en fonction de sa position actuelle
	 * 
	 * @param action (gauche droite haut bas)
	 * @param positionCurseur (la position actuelle)
	 */
	private void bougerCurseur(int action, int[] positionCurseur)
	{
		//Cette fonction repond a chacun des cas en particulier, pour avoir des 
		//deplacements dans l'inventaire plus propres
		switch(action)
		{
		case 0: //que faire si le curseur doit se deplacer vers le bas
			if(positionCurseur[0]>=0 && positionCurseur[0]<6)
				positionCurseur[0]+=3;
			else if((positionCurseur[0]>8 && positionCurseur[0]<11) || 
					(positionCurseur[0]>=12 && positionCurseur[0]<16) ||
					positionCurseur[0]==17)
				positionCurseur[0]++;
			return;
		case 1: //que faire si le curseur veut se deplacer vers la droite
			if(	(positionCurseur[0]>=0 && positionCurseur[0]<2) ||
				(positionCurseur[0]>=3 && positionCurseur[0]<5) ||
				(positionCurseur[0]>=6 && positionCurseur[0]<8) )
				positionCurseur[0]++;
			else if(positionCurseur[0]==9)
				positionCurseur[0]=0;
			else if(positionCurseur[0]==10)
				positionCurseur[0]=3;
			else if(positionCurseur[0]==11)
				positionCurseur[0]=6;
			else if(positionCurseur[0]==12 || positionCurseur[0]==13)
				positionCurseur[0]=9;
			else if(positionCurseur[0]==14)
				positionCurseur[0]=10;
			else if (positionCurseur[0]==15 || positionCurseur[0]==16)
				positionCurseur[0]=11;
			else if(positionCurseur[0]==17)
				positionCurseur[0]=13;
			else if(positionCurseur[0]==18)
				positionCurseur[0]=15;
			return;
		case 2: //que faire si le curseur doit se deplacer vers le haut
			if(positionCurseur[0]>=3 && positionCurseur[0]<9)
				positionCurseur[0]-=3;
			else if((positionCurseur[0]>9 && positionCurseur[0]<12) || 
					(positionCurseur[0]>12 && positionCurseur[0]<=16) ||
					positionCurseur[0]==18)
				positionCurseur[0]--;
			return;
		case 3: //que faire si le curseur veut se deplacer vers la gauche
			if(	(positionCurseur[0]>0 && positionCurseur[0]<=2) ||
					(positionCurseur[0]>3 && positionCurseur[0]<=5) ||
					(positionCurseur[0]>6 && positionCurseur[0]<=8) )
					positionCurseur[0]--;
			else if(positionCurseur[0]==0)
				positionCurseur[0]=9;
			else if(positionCurseur[0]==3)
				positionCurseur[0]=10;
			else if(positionCurseur[0]==6)
				positionCurseur[0]=11;
			else if(positionCurseur[0]==9)
				positionCurseur[0]=13;
			else if(positionCurseur[0]==10)
				positionCurseur[0]=14;
			else if(positionCurseur[0]==11)
				positionCurseur[0]=15;
			else if(positionCurseur[0]==12 || positionCurseur[0]==13 || positionCurseur[0]==14)
				positionCurseur[0]=17;
			else if (positionCurseur[0]==15 || positionCurseur[0]==16)
				positionCurseur[0]=18;
			return;
			default:
				return;
		}
	}
	
	/**
	 * met a jour la position des items dans l'inventaire, utile si on a prevu de les jeter ou de frapper avec(arme)
	 * @param x le X du joueur
	 * @param y le Y du joueur
	 */
	public void update(double x, double y)
	{
		for (Item entry : inventory.stream().filter(item -> item!=null).collect(Collectors.toList()))
		{
		   entry.setPosition(new Position(x,y));
		   entry.updateHB();
		}
		if(arme!=null)
		{
			arme.setPosition(new Position(x,y));
			arme.updateHB();
		}
		if(armure!=null)
		{
			armure.setPosition(new Position(x,y));
			armure.updateHB();	
		}
		for(Bijoux b : bijoux)
		{
			if(b!=null)
			{
					b.setPosition(new Position(x,y));
					b.updateHB();	
			}
		}
	}
	/**
	 * est-ce qu'il reste de la place dans l'inventaire ?
	 * @param i l'item qu'on veut ajouter (si on l'a deja, on augmentera juste le nombre)
	 * @return
	 */
	public boolean placePour(Item i)
	{
		return this.getSize()<this.getMaxSize();
	}

	public int getMoney()
	{
		return money;
	}
	
	public void setMoney(int mon)
	{
		money=mon;
	}
	
	public boolean setArme(Arme a)
	{
		if(a==null)
		{
			arme=null;
			return true;
		}
		if(a.getClassesPouvantPorter().contains(this.joueurDeQuelleClasse))
		{
			arme=a;
			return true;
		}
		
		return false;
	}
	
	public boolean setBijoux(int positionBijou, Bijoux leNouveauBijou)
	{
		
		bijoux[positionBijou]=leNouveauBijou;

		return true;
	}
	
	public Arme getArme()
	{
		return arme;
	}
	
	public boolean setArmure(Armure a)
	{
		if(a==null)
		{
			armure=null;
			return true;
		}
		System.out.println("ma classe : " + joueurDeQuelleClasse + "  l'armure  : " + a.getClassesPouvantPorter());

		
		if(a.getClassesPouvantPorter().contains(this.joueurDeQuelleClasse))
		{
			armure=a;
			return true;
		}
		
		return false;
	}
	
	public Armure getArmure()
	
	{
		return armure;
	}
	public Bijoux[] getBijoux()
	{
		return bijoux;
	}
	
	public void setBijoux(Bijoux[] b)
	{
		for(int i=0; i<b.length; i++)
		{
			bijoux[i]=b[i];
		}
	}

	/**
	 * 	calcule les emplacements de l'inventaire pour l'affichage, 
	 *  pour afficher les items au bons endroits et la surbrillance aussi
	 *  les valeurs sont codees dans le dur car l'inventaire est une image
	 *  et donc ces valeurs, entre 0 et 1, ne changeront jamais.
	 */
	private void initPositionDessins()
	{
		//LES POSITIONS DE SURBRILLANCE :
		//les 9 items de droite : 
		//l'espacement entre chaque carre
		double espacementX = 0.081;
		double espacementY = espacementX*3/2;
		//la taille des carres
		double largeurCarre = 0.07867;
		double hauteurCarre = largeurCarre* 3/2;
		for(int l=0; l<3; l++)
		{
			for(int h=0; h<3; h++)
			{
				positionsDessinSurbrillance[0][l+3*h] = 0.765 + l*espacementX;
				positionsDessinSurbrillance[1][l+3*h] = 0.382 - h*espacementY;
				positionsDessinSurbrillance[2][l+3*h] = largeurCarre;
				positionsDessinSurbrillance[3][l+3*h] = hauteurCarre;
			}
		}
		//Les 3 items a raccourcis
		//espacement entre les blocs
		double espacementX2 = 0.081;
		double espacementY2 = espacementX2*3/2;
		//taille des blocs
		double largeurCarre2 = 0.07867;
		double hauteurCarre2 = largeurCarre2* 3/2;
		for(int h=0; h<3; h++)
		{
			positionsDessinSurbrillance[0][9+h] = 0.687;
			positionsDessinSurbrillance[1][9+h] = 0.382 - h*espacementY2;
			positionsDessinSurbrillance[2][9+h] = largeurCarre2;
			positionsDessinSurbrillance[3][9+h] = hauteurCarre2;
		}
		//les 5 bijoux
		//l'espacement entre chaque carre
		double espacementX3 = 0.0367;
		double espacementY3 = espacementX3*3/2;
		//taille des blocs
		double largeurCarre3 = 0.0345;
		double hauteurCarre3 = largeurCarre3* 3/2;
		for(int h=0; h<5; h++)
		{
			positionsDessinSurbrillance[0][12+h] = 0.2995;
			positionsDessinSurbrillance[1][12+h] = 0.3315 - h*espacementY3;
			positionsDessinSurbrillance[2][12+h] = largeurCarre3;
			positionsDessinSurbrillance[3][12+h] = hauteurCarre3;
		}
		//contour epee
		positionsDessinSurbrillance[0][17] = 0.2352;
		positionsDessinSurbrillance[1][17] = 0.276;
		positionsDessinSurbrillance[2][17] = 0.0675;
		positionsDessinSurbrillance[3][17] = 0.165;
		//contour armure
		positionsDessinSurbrillance[0][18] = 0.2355;
		positionsDessinSurbrillance[1][18] = 0.135;
		positionsDessinSurbrillance[2][18] = 0.070;
		positionsDessinSurbrillance[3][18] = 0.1059;
		
		//LES POSITIONS D'ITEM : 
		for(int i=0; i<positionsDessinSurbrillance[0].length; i++)
		{
			positionsDessinItems[0][i]=positionsDessinSurbrillance[0][i];
			positionsDessinItems[1][i]=positionsDessinSurbrillance[1][i];
			positionsDessinItems[2][i]=positionsDessinSurbrillance[2][i]*6/10;
			positionsDessinItems[3][i]=positionsDessinSurbrillance[3][i]*6/10;
		}
		
	}

	public Item getRaccourci1()
	{
		return potionRaccourci1;
	}
	
	public Item getRaccourci2()
	{
		return potionRaccourci2;
	}
	
	public void setClasseJoueur(int classeJoueur)
	{
		joueurDeQuelleClasse=classeJoueur;
	}

	/**
	 * renvoie le dernier item de l'inventaire (non equipe), null s'il est vide
	 * @return l'Item en question
	 */
	public Item getRandomItemInStock()
	{
		for(int i=inventory.size()-1; i>=0; i--)
		{
			Item lItem = inventory.get(i);
			boolean equipe = false;
			if(lItem==null)
				equipe=true;
			if(lItem==arme)
				equipe=true;
			else if(lItem==armure)
				equipe=true;
			else if(lItem==potionRaccourci1)
				equipe=true;
			else if(lItem==potionRaccourci2)
				equipe=true;
			for(int b=0; b<bijoux.length; b++)
			{
				if(bijoux[b] == lItem)
					equipe=true;
			}
			if(!equipe)
			{
				return inventory.get(i);
			}
		}
		return null;
	}

	/**
	 * renvoie tous les items non equipes, pratique pour voir son inventaire pendant l'achat/vente
	 * @return
	 */
	public LinkedList<Item> getItemsNonEquipes()
	{
		LinkedList<Item> items = new LinkedList<Item>();
		items.addAll(this.inventory);
		for(int i=0; i<inventory.size(); i++)
		{
			Item lItem = inventory.get(i);
			boolean equipe = false;
			if(lItem==null)
				equipe=true;
			if(lItem==arme)
				equipe=true;
			else if(lItem==armure)
				equipe=true;
			else if(lItem==potionRaccourci1)
				equipe=true;
			else if(lItem==potionRaccourci2)
				equipe=true;
			for(int b=0; b<bijoux.length; b++)
			{
				if(bijoux[b] == lItem)
					equipe=true;
			}
			if(equipe)
				items.remove(lItem);
		}
		return items;
	}
}

/**
 * Boutons : 
 * 				EQUIPER ?
 * 				Montrer stats ?
 * 				Utiliser ?
 * 				
 * 				JETER
 * 				
 * 				
 */
