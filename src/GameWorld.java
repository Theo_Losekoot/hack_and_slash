import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Map.Entry;

public class GameWorld
{
	//permet de gerer le temps de rafraichissement, le laisser a 20 est bien car ca reste fluide
	public static final int TEMPS_RAFRAICHISSEMENT=20;

	// la taille du monde
	private static int widthGrille;
	private static int heightGrille;

	// on cree notre personnage
	private static Joueur personnage;

	// les monstres sur la carte
	private static ArrayList<Monstre> monstres;

	// l'ensemble des entites, pour gerer (notamment) l'affichage
	private static HashMap<String, Entite> entites;
	
	//l'ensemble des entites du hub, pour retenir quelles portes sont ouvertes etc.
	private static HashMap<String, Entite> entitesMapBase;
	
	//quelques elements a dessiner, sans etre dans les entites (arme, trucs)
	private static HashMap<Entite, Double> entitesOneShot;

	// pour savoir si le heros est toujours en vie
	private static boolean alive;

	// Pour savoir si la partie est gagnee ou pas
	private static boolean gameWon;

	//pour savoir sur quelle map on se situe
	private static int mapActuelle;
	
	//pour faire reaparaitre les mobs jusqu'a un certain nombre
	private static int mobsTuesMap;
	
	//pour faire apparaitre le boss une seule fois
	private static boolean bossApparu;
	
	// constructeur, il faut initialiser notre monde virtuel
	public GameWorld(int tailleGrille)
	{
		// la largeur de la grille, et sa hauteur
		GameWorld.widthGrille = tailleGrille*3/2;
		GameWorld.heightGrille = tailleGrille;

		gameWon = false;
		
		//on initialise les images
		Images.init();

		// on cree collections
		entites = new HashMap<String, Entite>();
		monstres = new ArrayList<Monstre>();
		
		mobsTuesMap=0;
		bossApparu=false;

		//nom, posX, posY, PdV, PAtk, PDef, speed, width, height 
		// on cree notre personnage
		personnage = new Joueur("Jacob", 1, 15, 110, 10, 0, 0.017, 1.5, 1.5, 1);
		entites.put(personnage.getNom(), personnage);
		alive = true;
		
		//cree des tableaux d'entites
		entitesMapBase=new HashMap<String,Entite>();
		entitesOneShot= new HashMap<Entite,Double>();
	
		//definit la map actuelle
		mapActuelle=Maps.Map.Base.val();

		Maps.construitMap(mapActuelle);
		
		for(Entite e : Maps.getEntitesMap(mapActuelle))
		{
			entites.put(e.getNom(), e);
		}
		personnage.sePositionner(mapActuelle);
	
	//	Images.chargerMonstre(Images.Entites.Dragon.val());
	//	Monstre dragon = new Dragon("dragon", 15, 15, Dragon.getUsualLifePoints(), Dragon.getUsualAttack(), Dragon.getUsualDefense(),
//				Dragon.getUsualSpeed(), Dragon.getUsualWidth(), Dragon.getUsualHeight(), Dragon.getUsualKnockback(), Dragon.getUsualDurationAttack());
	//	entites.put(dragon.getNom(), dragon);
	//	monstres.add(dragon);

		
	}

	/**
	 * En fonction de toutes les touches pressees par l'utilisateur, fait les mises a jour
	 * necessaires dans le monde du jeu.
	 * 
	 */
	public void processUserInput()
	{
		if(Keys.getKey(Keys.Touches.d.val()) || Keys.getKey(Keys.Touches.rightKey.val()))
		{
			personnage.goRight();
		}
		if(Keys.getKey(Keys.Touches.z.val()) || Keys.getKey(Keys.Touches.upKey.val()) || Keys.getKey(Keys.Touches.w.val()))
		{
			personnage.goUp();
		}
		if(Keys.getKey(Keys.Touches.s.val()) || Keys.getKey(Keys.Touches.downKey.val()))
		{
			personnage.goDown();
		}
		if(Keys.getKey(Keys.Touches.q.val()) || Keys.getKey(Keys.Touches.leftKey.val()) || Keys.getKey(Keys.Touches.a.val()))
		{
			personnage.goLeft();
		}
		if(Keys.getKey(Keys.Touches.l.val()) && !Keys.getPreviousKey(Keys.Touches.l.val()))
		{
			personnage.poserBombe();
		}
		if(Keys.getKey(Keys.Touches.p.val()) && !Keys.getPreviousKey(Keys.Touches.p.val()))
		{
			personnage.utiliserPotion1();
		}
		if(Keys.getKey(Keys.Touches.t.val()) && !Keys.getPreviousKey(Keys.Touches.t.val()))
		{
			personnage.jeter();
		}
		if(Keys.getKey(Keys.Touches.i.val()) && !Keys.getPreviousKey(Keys.Touches.i.val()))
		{
			personnage.afficherInventaire();
		}
		if(Keys.getKey(Keys.Touches.o.val()) && !Keys.getPreviousKey(Keys.Touches.o.val()))
		{
			personnage.utiliserPotion2();
		}
		if(Keys.getKey(Keys.Touches.m.val()))
		{
			personnage.deposerCle();
		}
		if(Keys.getKey(Keys.Touches.j.val()) && !Keys.getPreviousKey(Keys.Touches.j.val()))
		{
			personnage.attaque1();
		}
		if(Keys.getKey(Keys.Touches.n.val()) && !Keys.getPreviousKey(Keys.Touches.n.val()))
		{
			changerMonde(Maps.Map.Base.val());
		}
		if(Keys.getKey(Keys.Touches.e.val()) && !Keys.getPreviousKey(Keys.Touches.e.val()))
		{
			personnage.interagirMarchands();
		}
		if(Keys.getKey(Keys.Touches.k.val()) && !Keys.getPreviousKey(Keys.Touches.k.val()))
		{
			personnage.attaque2();
			
		}
		if(Keys.getKey(Keys.Touches.c.val()) && !Keys.getPreviousKey(Keys.Touches.c.val()))
		{
			personnage.addXp(150);
			
		}
		if(Keys.getKey(Keys.Touches.v.val()) && !Keys.getPreviousKey(Keys.Touches.v.val()))
		{
			for(int i=0; i<monstres.size(); i++)
			{
				if(monstres.get(i) instanceof Monstre)
				{
					Monstre leMonstre = monstres.get(i--);
					leMonstre.deceder();
				}
			}
		}
		Keys.sync();
		GameWorld.syncMonstresEntites();

	}

	// on fait bouger tous les monstres et le personnage
	public void step()
	{
		entitesOneShot= new HashMap<Entite,Double>();

		//for (Monstre monstre : monstres)
		//	monstre.jouerr();
		for(int i=0; i<monstres.size(); i++)
		{
			Monstre a=monstres.get(i);
			a.jouer();
			if(!a.isAlive())
				i--;
		}
		//diminue le TTL des bombes, et les fait exploser au besoin 
		updateBombes();

		//update le personnage
		personnage.jouer();
		
		//actualise les missiles 
		faireJouerMissiles();

		//ajoute des monstres supplementaires si on en a pas tue assez
		Maps.ajouterMonstres(mapActuelle, monstres, personnage.getX(), mobsTuesMap);

	}
	
	/**
	 * ajoute aux entites les monstres qui lui manquent
	 */
	public static void syncMonstresEntites()
	{
		for(Monstre m : monstres)
		{
			if(!entites.containsKey(m.getNom()))
				entites.put(m.getNom(), m);
		}
	}
	
	/**
	 * ajoute aux monstres les entites qui lui manquent
	 */
	public static void syncEntitesMonstres()
	{
		for(Monstre m : entites.values().stream().filter(Monstre.class::isInstance).map(x -> (Monstre)x).collect(Collectors.toList()))
		{
			if(!monstres.contains(m))
				monstres.add(m);
		}
	}
	
	// dessine la grille entiere
	public void dessine()
	{
		dessinerMap();
		
		//Pour afficher les entites, je les trie d'abord par la position
		//de leur hitbox, de maniere a afficher les entites au premier plan en premier.
		LinkedList<Entite> entitesTriees = new LinkedList<Entite>();
		entitesTriees.addAll(Anim.getEntitesDisparaissant());
		entitesTriees.addAll(entites.values());
		Collections.sort(entitesTriees);


		//affiche toutes les entites
		for(Entite e : entitesTriees)
		{
			if(surEcran(e))
				e.dessine();
		}
		//affiche les entites qui ne sont pas forcement la pour durer, qu'une autre classe a juste demande d'afficher.
		//comme l'arme par exemple
		for(Entry<Entite, Double> e : entitesOneShot.entrySet())
		{
			if(surEcran(e.getKey()))
				e.getKey().dessine(e.getValue());
		}
		//affiche les textes (genre soin etc)
		for(TexteTemporaire t : Anim.getTextesDisparaissant())
		{
			t.dessine();
		}
		//affiche la bar de vie
		personnage.afficherBarInfos();
	}
	
	/**
	 * permet de determiner si une entite est affiche sur l'ecran ou s'il est en dehors, 
	 * de maniere a ne pas dessiner toutes les entites s'il n'y en a pas besoin. economie de ressources.
	 * @param e l'entite en question
	 * @return true si on veut l'afficher, false sinon
	 */
	public static boolean surEcran(Entite e)
	{
		if(Math.abs(e.getX()-personnage.getX())>GameWorld.getWidthGrille())
		{
			return false;
		}
		if(Math.abs(e.getY()-personnage.getY())>GameWorld.getHeightGrille())
		{
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @return toutes les entites presentes sur la carte
	 */
	public static HashMap<String,Entite> getEntites()
	{
		return entites;
	}
	
	/**
	 * 
	 * @return une list des monstres presents sur la map
	 */
	public static List<Monstre> getMonstres()
	{
		return monstres;
	}
	
	/**
	 * renvoie tous les items contenus dans les entites
	 * @return
	 */
	public static HashMap<String,Item> getItems()
	{
		 HashMap<String, Item> retour= new HashMap<String,Item> ();
		 for(Entite e : entites.values())
		 {
			 if (e instanceof Item)
			 {
				 retour.put(e.getNom(), (Item)e);
			 }
		 }
		 return retour;
	}

	/**
	 * 
	 * @return true si le personnage est encore en vie
	 */
	public static boolean isCharacterAlive()
	{
		return alive;
	}
	
	/**
	 * 
	 * @return la largeur de la map affichable a l'ecran
	 */
	public static int getWidthGrille()
	{
		return widthGrille;
	}
	/**
	 * 
	 * @return la hauteur de la map affichabl a l'ecran
	 */
	public static int getHeightGrille()
	{
		return heightGrille;
	}

	// Pose un Item sur la carte
	public static void dropItem(Item i)
	{
		entites.put(i.getNom(),i);
	}
	/**
	 * indique si on a gagne
	 * @return true si on a gagne
	 */
	public static boolean gameWon()
	{
		return gameWon;
	}
	
	/**
	 * ajoute une entite a la liste d'entites du gameworld, la remplace si le nom existe deja.
	 * @param e l'entite
	 */
	public static void addEntite(Entite e)
	{
		if(entites.containsKey(e.getNom()))
		{
			System.out.println("ATTENTION, TU REMPLACES UNE UNITE, TU N'EN CREE PAS DE NOUVELLE");
		}
		entites.put(e.getNom(), e);
	}
	
	/**
	 * supprime une entite des entites du gameworld
	 * @param e l'entite a supprimer
	 */
	public static void removeEntite(Entite e)
	{
		try
		{
			entites.remove(e.getNom());
		}
		catch(Exception a)
		{
			System.out.println("erreur lors de la suppression d'Entite du gameworld : " + a);
		}
	}
	/**
	 * pour chaque bombe, appelle la methode updateBombe, de maniere a faire exploser les bombes au bout d'un moment
	 */
	public static void updateBombes()
	{
		for(Bombe i : entites.values().stream().filter(Bombe.class::isInstance).map(x -> (Bombe)x).collect(Collectors.toList()))
		{
			i.updateBombe();
		}
	}
	/**
	 * dessine la map en essayant de limiter l'impact en ressources(plus d'explications dans la fonction dessinerMap)
	 */
	public void dessinerMap()
	{
		Maps.dessinerMap(personnage.getX(), personnage.getY(), mapActuelle);

	}
	
	/**
	 * appelle ma methode de Maps pour connaitre le decalage en X dans l'affichage de toutes les entites
	 * @return le decalage
	 */
	public static double getDecalageXAffichage()
	{
		return Maps.decalageX(personnage.getX(), mapActuelle);
	}
	
	/**
	 * appelle ma methode de Maps pour connaitre le decalage en Y dans l'affichage de toutes les entites
	 * @return le decalage
	 */
	public static double getDecalageYAffichage()
	{
		return Maps.decalageY(personnage.getY(), mapActuelle);
	}
	
	/**
	 * permet de charger une nouvelle map
	 * @param nouveauMonde le numero de la nouvelle map
	 */
	public static void changerMonde(int nouveauMonde)
	{
		//sauvegarde les choses qu'on a fait sur le hub, ouverture des portes etc
		if(mapActuelle==Maps.Map.Base.val())
		{
			entitesMapBase = new HashMap<String, Entite>();
			entitesMapBase.putAll(entites);
			entitesMapBase.remove(personnage.getNom());
		}
		//pour soulager un peu la memoire
		Images.dechargerMap(mapActuelle);
		//change de map et cree la nouvelle map et recupere toutes les entites presentes dessus
		//pour avoir la map quand meme
		mapActuelle=nouveauMonde;
		Images.chargerMap(mapActuelle);
		monstres = new ArrayList<Monstre>();
		entites = new HashMap<String, Entite>();
		entites.put(personnage.getNom(), personnage);
		if(mapActuelle!=Maps.Map.Base.val())
		{
			Maps.construitMap(nouveauMonde);
			for(Entite e : Maps.getEntitesMap(mapActuelle))
				entites.put(e.getNom(), e);
		}
		else
			entites.putAll(entitesMapBase);
		
		//mets les monstres qui sont dans entites dans monstres aussi.
		syncEntitesMonstres();
		System.out.println("teleportation a la map : " + nouveauMonde);
		personnage.sePositionner(nouveauMonde);
		mobsTuesMap=0;
		bossApparu=false;
		Anim.flushEntitesDisparaissant();
	}
	
	/**
	 * ajoute les entites temporaires a afficher
	 * @param e l'entite
	 * @param rotation de combien elle tourne
	 */
	public static void addEntiteOneShot(Entite e, double rotation)
	{
		entitesOneShot.put(e, rotation);
	}
	
	public static Joueur getJoueur()
	{
		return personnage;
	}
	public static int getNbMobsTuesMap()
	{
		return mobsTuesMap;
	}
	public static void incNbMobsTuesMap()
	{
		mobsTuesMap++;
	}
	public static boolean bossApparu()
	{
		return bossApparu;
	}
	public static void bossApparait()
	{
		bossApparu=true;
	}
	/**
	 * retourne la distance enre le joueur et le x et y donnes en parametre
	 * @param x
	 * @param y
	 * @return
	 */
	public static double getDistanceJoueur(double x, double y)
	{
		double distX = Math.abs(x-personnage.getX());
		double distY = Math.abs(y-personnage.getY());
		
		return Math.sqrt(distX*distX+distY*distY);
	}
	/**
	 * fais le tour des missiles, qui ne sont pas dans la liste des monstres
	 */
	public void faireJouerMissiles()
	{
		List<Projectile> lesMissiles = entites.values().stream().filter(Projectile.class::isInstance).map(x -> (Projectile)x).collect(Collectors.toList());
		lesMissiles.forEach(x -> x.jouer());
	}
	
	public static int getMondeActuel()
	{
		return mapActuelle;
	}
}
