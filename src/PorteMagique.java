
public class PorteMagique extends Entite
{
	//pour savoir si on peut passer a travers la porte
	private boolean porteOuverte;
	//pour savoir quelle cle doit pouvoir nous ouvrir
	private int typePorte;

	public PorteMagique(String nom, double x, double y, double width, double height, boolean isObstacle, int _typePorte, boolean ouvert)
	{
		//Entite : nom, positionX, positionY, largeur, hauteur, obstacle?, 
		//etreVivant ? quelle entite?
		super(nom, x, y, width, height, isObstacle, false, Images.Entites.PorteMagique.val());
		porteOuverte = ouvert;
		typePorte=_typePorte;
		if(!ouvert)
		{
			if(typePorte==Maps.Map.Chateau.val())
				this.quelleEntite=Images.Entites.PorteBossFermee.val();
			else 
				this.quelleEntite=Images.Entites.PorteFermee.val();
		}
		else
		{
			if(typePorte==Maps.Map.Chateau.val())
				this.quelleEntite=Images.Entites.PorteBoss.val();
			else 
				this.quelleEntite=Images.Entites.Porte.val();
		}
	}

	public void dessine()
	{
		//Dessine la porte, fermee ou ouverte
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(this.quelleEntite),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
	}	
	
	@Override
	public void dessine(double rotation)
	{
		//Dessine la porte, fermee ou ouverte
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(this.quelleEntite),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				rotation); 
	}

	/**
	 * fontion pour placer la cle dans la porte, et ouvrir si c'est la bonne cle
	 * @param c le type de cle que le joueur possede
	 * @return un boolean, pour savoir si la cle a ete utilisee pour ouvrir la porte
	 */
	public boolean placer(Cle c)
	{
		if(c.getTypeCle()==this.getTypePorte() && !porteOuverte)
		{
			porteOuverte=true;
			if(this.quelleEntite == Images.Entites.PorteFermee.val())
				this.quelleEntite = Images.Entites.Porte.val();
			else if(this.quelleEntite == Images.Entites.PorteBossFermee.val())
				this.quelleEntite = Images.Entites.PorteBoss.val();
			else
				System.out.println("oups");
			return true;
		}
		return false;
	}

	public boolean isOpen()
	{
		return porteOuverte;
	}
	
	public int getTypePorte()
	{
		return typePorte;
	}
}
