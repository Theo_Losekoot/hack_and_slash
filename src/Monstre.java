import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public abstract class Monstre extends EtreVivant
{
	//pour donner un nom different a chaque monstre cree
	private static int numeroInstanciation=0;
	
	protected ArrayList<Item> tresor;
	protected boolean droitePrecedent=true;

	// constructeur
	public Monstre(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width, double height, int entite, double _recul)
	{
		//etreVivant : nom, positionX, positionY, points de vie, points d'attaque, 
		//points de defense, vitesse, largeur, hauteur
		super(nom + String.valueOf(numeroInstanciation), x, y, pDV, pAtk, pDef, vitesse, width, height, entite, _recul);
		tresor = genererTresor();
		numeroInstanciation++;
	}

	@Override
	public abstract void jouer();

	
	@Override
	public void mourir()
	{
		dropContent();
		GameWorld.getMonstres().remove(this);
		GameWorld.removeEntite(this);
		GameWorld.incNbMobsTuesMap();
	}
	
	public int getPointsDeVie()
	{
		return this.pointsDeVie;
	}
	
	public void dropContent()
	{
		for( Item i : tresor)
		{
			lacher(i);
		}
	}
	
	public void ajouterItemTresor(Item i)
	{
		tresor.add(i);
	}
	
	public void lacher(Item i)
	{
		updateItem(i);
		GameWorld.addEntite(i);
	}

	public void updateItem(Item i)
	{
		   i.setPosition(new Position(this.getX(),this.getY()));
		   i.updateHB();
	}
	
	public ArrayList<Item> genererTresor()
	{
		ArrayList<Item> futurTresor = new ArrayList<Item>();
		if(ThreadLocalRandom.current().nextInt(0,5)==0)
		{
			Money money = new Money("paquet",0,0,1,1,1);
			futurTresor.add(money);
		}
		return futurTresor;
	}

	
	public abstract void actualiserCoup();

	
	// methode permettant l'application du vecteur aux monstres
	public abstract void seDeplacer();

	/**
	 * detecte si un monstre a rencontré un autre monstre ou un obstacle, qutre que lui meme et le joueur
	 * @return true si il y a collision
	 */
	public boolean collisionPourMonstres()
	{
		boolean collision=false;
		for (Entite a : GameWorld.getEntites().values())
		{
			if (a.isObstacle() && a.getNom() != this.getNom() && a.getNom()!=GameWorld.getJoueur().getNom())
			{
				if(Hitbox.collision(a.getHB(), this.getHB()))
				{
					collision = true;
				}
			}
		}
		return collision;
	}
	
	public void dessine()
	{
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(this.quelleEntite, this.numeroAnimation, this.vecteurAvancer, this.droite, this.frappeActuellement()),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();
	}
	
	@Override
	public void updateVarAnim()
	{
		droitePrecedent=droite;
		this.numeroAnimation[0]++;
		if(vecteurAvancer.getX()>this.vecteurAvancer.getCritereArret() || (droitePrecedent && vecteurAvancer.getX()==0))
		{
			droite=true;
		}
		else
		{
			droite=false;
		}
	}
	
	public void deceder()
	{
		this.pointsDeVie=0;
		this.mourir();
	}
}
