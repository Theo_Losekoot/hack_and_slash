import java.util.List;
import java.util.stream.Collectors;

public abstract class Projectile extends EtreVivant
{
	private static int numeroInstanciation=0;
	protected double rayonAction;
	protected boolean magique;
	//si le missile est encore en jeu apres 10sec, le supprimer
	protected int TTL = 10000;
	public Projectile(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width,
			double height, double _recul, int entite, boolean _droite, double _rayonAction, boolean _magique)
	{
		super(nom + String.valueOf(numeroInstanciation), x, y, pDV, pAtk, pDef, vitesse, width, height, entite, _recul);
		droite=_droite;
		rayonAction=_rayonAction;
		numeroInstanciation++;
		magique=_magique;
	}
	
	/**
	 * ne prend aucun degats.
	 */
	@Override
	public int prendreDegats(int attaque, double x, double y, double multip)
	{
		return 0;
	}
	
	/**
	 * determine le deplacement des lapins au fil du temps, donc l'IA des lapins.
	 */
	public abstract void augmenterVecteur();

	/**
	 * lance l'attaque (si il y en a une ? on sait pas encore)
	 */
	public void attaquer()
	{
		// vecteurAvancer.setX(0);
		// vecteurAvancer.setY(0);
		if (!frappeActuellement())
		{
			tempsAttaqueActuelle = 0;
		}
	}

	/**
	 * bha du coup pour l'instant c'est vide
	 */
	public void actualiserCoup()
	{
		List<Entite> lesObstacles = GameWorld.getEntites().values().stream()
				.filter(x -> x.isObstacle() && !(x instanceof Joueur) || x instanceof Monstre && !(x instanceof Projectile))
				.collect(Collectors.toList());
		for(Entite e : lesObstacles)
		{
			this.getHB().setWidth(this.getHB().getWidth()*1.2);
			this.getHB().setHeight(this.getHB().getHeight()*1.2);
			if(Hitbox.collision(this.getHB(), e.getHB()))
			{
				if(e instanceof Monstre )
					attaquer((EtreVivant)e);
				else
					mourir();
			}
			this.getHB().setWidth(this.getHB().getWidth()*(1/1.2));
			this.getHB().setHeight(this.getHB().getHeight()*(1/1.2));
		}

	}
	

	
	@Override
	public void mourir()
	{
		GameWorld.removeEntite(this);
	}
	
	
	public void attaquer(EtreVivant a)
	{
			a.prendreDegats(this.pointsDAttaque, position.getX(), position.getY(), this.getRecul());
			if(magique)
				Anim.addEntiteDisp(new EntiteTimer("explosion" + String.valueOf(this.getNom()), this.getX(), this.getY(),
						this.getWidth()*3, this.getHeight()*3,false, 350, Images.Entites.Explosion.val()));
			mourir();
	}
	
	/**
	 * Implemente la methode jouer
	 * appele les differentes fonctions pour faire vivre le missile,comme les coups le deplacement
	 * et gerer la mort du missile
	 */
	@Override
	public void jouer()
	{
		actualiserCoup();
		seDeplacer();
		return;
	}
	
	
	// methode permettant l'application du vecteur au missile et la modification
	//de son orientation, ainsi que la mise a jour du vecteur.
	public void seDeplacer()
	{
		augmenterVecteur();
		avancer();
		vecteurAvancer.ralentir();
	}
	
	@Override
	public void dessine()
	{
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(this.quelleEntite, this.numeroAnimation),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();
	}
	
	protected void initVecteur()
	{
		if(droite)
			vecteurAvancer.setX(this.getVitesse()*10);
		else
			vecteurAvancer.setX(-this.getVitesse()*10);


	}
	
}
