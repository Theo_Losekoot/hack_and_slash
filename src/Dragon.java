import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Dragon extends Chasseur
{
	
	private static double usualWidth=20;
	private static double usualHeight=15;
	private static int usualLifePoints=3000;
	private static int usualAttack=150;
	private static int usualDefense=150;
	private static double usualSpeed=0.012;
	private static double usualKnockback=2;
	private static int usualDurationAttack=1000;
	
	
	//pour faire des deplacements un peu plus hasardeux et ne pas le laisser bloque a un endroit trop facilement
	private int tempsAvantChangementVecteurHasard;
	private Vector vectRandom;
	private int tempsAttaqueActuelle;
	private int tempsAvantProchaineAttaque;
	private int dureeAttaque;
	
	public Dragon(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width, double height, double _recul, int _dureeAttaque)
	{
		super(nom,x,y,pDV,pAtk,pDef,vitesse,width,height, Images.Entites.Paysan.val(), _recul, false);
		ArrayList<Integer> classesPortant = new ArrayList<Integer>();
		dureeAttaque=_dureeAttaque;
		tempsAttaqueActuelle = dureeAttaque*2;
		tempsAvantProchaineAttaque=0;
		classesPortant.add(0);
		classesPortant.add(1);
		classesPortant.add(2);
		classesPortant.add(3);
		//tout le monde peut la porter
		this.tresor.add(new ArmeCac("Arme crakey", this.getX(), this.getY(), 9001, 30,30, Images.Entites.EpeeCrakey.val(), classesPortant, 1000000));
		tempsAvantChangementVecteurHasard=2000;
		vectRandom = new Vector(this.getVitesse());
		this.getHB().setHeight(this.getHB().getHeight()/2);
		this.getHB().setWidth(7*this.getHB().getWidth()/10);
	}
	
	/**
	 * Implemente la methode jouer, qui est appelee a chaque tour de monstre.
	 * appele les differentes fonctions pour faire vivre le monsre, comme l'immunite, les coups le deplacement
	 * et gerer la mort du monstre
	 */
	@Override
	public void jouer()
	{
		if(this.getPointsDeVie()<=0)
		{
			mourir();
			return;
		}
		updateImmunite();
		detecterCoup();
		actualiserCoup();
		seDeplacer();
		return;
	}
	
	
	public boolean frappeActuellement()
	{
		return  this.tempsAttaqueActuelle<=dureeAttaque;
	}
	
	public void attaquer(EtreVivant a)
	{
		a.prendreDegats(this.pointsDAttaque, position.getX(), position.getY(), this.getRecul());
	}
	

	/**
	 * lance l'attaque (si il y en a une ? on sait pas encore)
	 */
	public void attaquer()
	{
		// vecteurAvancer.setX(0);
		// vecteurAvancer.setY(0);
		if (!frappeActuellement())
		{
			tempsAttaqueActuelle = 0;
		}
	}
	
	/**
	 * bha du coup pour l'instant c'est vide
	 */
	@Override
	public void actualiserCoup()
	{
		tempsAvantProchaineAttaque-=GameWorld.TEMPS_RAFRAICHISSEMENT;
		tempsAttaqueActuelle+=GameWorld.TEMPS_RAFRAICHISSEMENT;
		Joueur perso = GameWorld.getJoueur();
		if(frappeActuellement())
		{
			
			//si la moitie de l'animation est passee, donc si le dragon crache sa flamme
			if(tempsAttaqueActuelle>dureeAttaque/2)
			{
				if(droite)
					this.getHB().setPosXM(this.getHB().getPosXM()+this.getHB().getWidth()/4);
				else
					this.getHB().setPosXM(this.getHB().getPosXM()-this.getHB().getWidth()/4);
				double differenceHauteur=6*this.getHB().getHeight()/10;
				//reduit la hauteur, pour ne pas cracher des flammes au dessus de sa tete
				this.getHB().setHeight(this.getHB().getHeight()-differenceHauteur);
				if(Hitbox.collision(this.getHB(),perso.getHB()))
				{
					attaquer(perso);
				}
				//et la reaugmente, quand meme
				this.getHB().setHeight(this.getHB().getHeight()+differenceHauteur);

				if(droite)
					this.getHB().setPosXM(this.getHB().getPosXM()-this.getHB().getWidth()/4);
				else
					this.getHB().setPosXM(this.getHB().getPosXM()+this.getHB().getWidth()/4);
			}
		}
		
	}
	
	public void detecterCoup()
	{
		Joueur perso = GameWorld.getJoueur();
		if(tempsAvantProchaineAttaque<=0)
		{
			if(Hitbox.collision(this.getHB(),perso.getHB()))
			{
				tempsAttaqueActuelle=0;
				tempsAvantProchaineAttaque = (int)(dureeAttaque*1.2);
			}
		}
	}
	
		/**
		 * le dragon suit le joueur, avec un peu de hasard
		 */
	@Override
	public void augmenterVecteur()
	{
		if(!frappeActuellement())
		{
			//met a jour le vecteur hasard
			tempsAvantChangementVecteurHasard-=GameWorld.TEMPS_RAFRAICHISSEMENT;
			if(tempsAvantChangementVecteurHasard<=0)
			{
				tempsAvantChangementVecteurHasard=2000;
				vectRandom.setX(ThreadLocalRandom.current().nextDouble());
				vectRandom.setY(ThreadLocalRandom.current().nextDouble());
				vectRandom = vectRandom.normalised();
			}

			Vector newNorm = new Vector(this.getVitesse());			
			Joueur perso = GameWorld.getJoueur();

			//se rapproche du joueur, sauf si on est deja au meme niveau, alors s'en ecarte
			if(Math.abs(perso.getX() - this.getHB().getPosXM())<8*this.getHB().getWidth()/20)
			{
				newNorm.setX(20*(-perso.getX()+this.getX()));
			}
			else
			{
				newNorm.setX(perso.getX()-this.getX());

			}
			//normalise le vecteur, en comptant 3 fois plus le vecteur Y, pour que le dragon puisse se rapprocher en face du joueur
			newNorm.setY(perso.getY()-this.getHB().getPosYM());
			newNorm.setX(newNorm.getX()/3);
			newNorm = newNorm.normalised();
			//met un peu de hasard, pas trop non plus
			newNorm.setX(newNorm.getX()+vectRandom.getX()/3);
			newNorm.setY(newNorm.getY()+vectRandom.getY()/3);
			newNorm = newNorm.normalised();
			vecteurAvancer.setX(vecteurAvancer.getX()+newNorm.getX()*this.getVitesse());
			vecteurAvancer.setY(vecteurAvancer.getY()+newNorm.getY()*this.getVitesse());
		}
			else	
			{
				vecteurAvancer.setY(0);
				vecteurAvancer.setX(0);
			}
			
	}
	
	public static double getUsualWidth()
	{
		return usualWidth;
	}
	public static double getUsualHeight()
	{
		return usualHeight;
	}

	public static int getUsualLifePoints()
	{
		return usualLifePoints;
	}

	public static int getUsualAttack()
	{
		return usualAttack;
	}

	public static int getUsualDefense()
	{
		return usualDefense;
	}

	public static double getUsualSpeed()
	{
		return usualSpeed;
	}
	public static double getUsualKnockback()
	{
		return usualKnockback;
	}
	public static int getUsualDurationAttack()
	{
		return usualDurationAttack;
	}
	
	@Override
	public void dessine()
	{

		//Dessine le fermier avec la bonne orientation
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(Images.Entites.Dragon.val(), numeroAnimation, vecteurAvancer, droite, frappeActuellement(), tempsAttaqueActuelle),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();

		//dessine une barre de vie
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.filledRectangle((double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()-this.getHeight()/2) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				(double)(this.getPointsDeVie()*3/(double)Dragon.getUsualLifePoints()) /(double)GameWorld.getWidthGrille(), (double)(this.getHeight()/60) /GameWorld.getHeightGrille());

	}
	
	@Override
	public void mourir()
	{
		dropContent();
		GameWorld.getMonstres().remove(this);
		GameWorld.removeEntite(this);
		GameWorld.incNbMobsTuesMap();
		GameWorld.getJoueur().addXp(1000);
	}
	
	@Override
	public void updateHB()
	{
		//this.hitbox.update(this.getHB().getPosXM(), this.getHB().getPosYM());
		this.hitbox.update(this.getX(), this.getY()-this.getHB().getHeight());

	}
	
	/**
	 * re-implemente la variable updateVarAnim, car se sert de la variable droite pour savoir quel cote il faut frapper
	 */
	@Override
	public void updateVarAnim()
	{
		this.numeroAnimation[0]++;
			if(vecteurAvancer.getX()>this.vecteurAvancer.getCritereArret()*5 || (vecteurAvancer.getX()<Math.abs(vecteurAvancer.getCritereArret()) && GameWorld.getJoueur().getX()-this.getX()>0 && vecteurAvancer.getX()>-vecteurAvancer.getCritereArret()))		{
				droite=true;
			}
			else
			{
				droite=false;
			}
	}

}
