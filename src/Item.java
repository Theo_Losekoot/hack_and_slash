
public abstract class Item extends Entite
{

	String texteDescriptif[];
	int valeur;
	
	/**
	 * Constructeur de la classe Item
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param isObstacle
	 * @param entite ; pour l'affichage
	 * @param _valeur : valeur (argent) de l'item
	 */
	public Item(String nom, double x, double y, double width, double height, boolean isObstacle, int entite, int _valeur)
	{
		//Entite : nom, positionX, positionY, largeur, hauteur, obstacle ? 
		//etre vivant ? quelleEntite?
		super(nom, x, y, width, height, isObstacle, false, entite);
		texteDescriptif = new String[2];
		valeur=_valeur;
	}

	public abstract void genererTexteDescriptif();
	
	public void dessine()
	{
				//Dessine l'item avec la bonne orientation
				StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
						(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
						Anim.bonneImage(this.quelleEntite),
						(double)this.getWidth()/ GameWorld.getWidthGrille(),
						(double)this.getHeight()/GameWorld.getHeightGrille(),
						0); 			

	}
	public void dessine(double rotation)
	{
				//Dessine l'item avec la bonne orientation
				StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
						(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
						Anim.bonneImage(this.quelleEntite),
						(double)this.getWidth()/ GameWorld.getWidthGrille(),
						(double)this.getHeight()/GameWorld.getHeightGrille(),
						rotation); 			
	}

	public String[] getTexteDescriptif()
	{
		return texteDescriptif;
	}
	
	public void setValeurItem(int val)
	{
		valeur=val;
	}
	public int getValeurItem()
	{
		return valeur;
	}
}
