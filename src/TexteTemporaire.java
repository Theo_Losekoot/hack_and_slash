
public class TexteTemporaire extends EntiteTimer
{

	private String texte;
	//est-ce un degat ou du soin ?
	private int couleur;
	/**
	 * Constructeur de TexteTemporaire
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param isObstacle
	 * @param tempsRestant
	 * @param _texte
	 */
	public TexteTemporaire(String nom, double x, double y, double width, double height, boolean isObstacle, int tempsRestant, String _texte, int _couleur)
	{
		//EntiteAnimee : nom, positionX, positionY, largeur, hauteur, obstacle?
		//c'est un etre vivant ?, quelle entite ?, routation
		super(nom, x, y, width, height, isObstacle, tempsRestant, Images.Entites.Error.val());
		couleur=_couleur;
		texte=_texte;

	}
	

	//Dessine l'entite avec la bonne image et la bonne animation
	public void dessine()
	{
		//degats
		if(couleur==0)
			StdDraw.setPenColor(StdDraw.RED);
		//soin
		else if(couleur==1)
			StdDraw.setPenColor(StdDraw.GREEN);
		//mana
		else if (couleur==2)
			StdDraw.setPenColor(StdDraw.BLUE);
		else if(couleur==3)
			StdDraw.setPenColor(StdDraw.YELLOW);
		StdDraw.text((this.getX()/GameWorld.getWidthGrille()) - GameWorld.getDecalageXAffichage(),
				((this.getY()+(double)this.getNumeroAnimation()/10) /GameWorld.getHeightGrille()) - GameWorld.getDecalageYAffichage(),
				texte);
		
		updateVarAnim();
	}

}
