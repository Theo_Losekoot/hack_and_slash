
public class Mur extends Entite
{

	public Mur(String nom, int x, int y, double width, double height)
	{
		//Entite : nom, positionX, positionY, largeur, hauteur, obstacle?,
		//etrevivant ? quelle entite ?
		super(nom, x, y, width, height, true, false, Images.Entites.Mur.val());
		// TODO Auto-generated constructor stub
	}

	@Override
	public void dessine()
	{
		// TODO Auto-generated method stub
		StdDraw.setPenColor(StdDraw.GREEN);
		StdDraw.filledRectangle((double)(this.getHB().getPosXM() / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				this.getY() / GameWorld.getHeightGrille() -GameWorld.getDecalageYAffichage(),
				this.getWidth()/2  / GameWorld.getWidthGrille(),
				this.getHeight()/2 / GameWorld.getHeightGrille());
		StdDraw.setPenColor(StdDraw.BLACK);
	}
	@Override
	public void dessine(double rotation)
	{
		// TODO Auto-generated method stub
		StdDraw.setPenColor(StdDraw.GREEN);
		StdDraw.filledRectangle((double)(this.getHB().getPosXM() / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				this.getY() / GameWorld.getHeightGrille() -GameWorld.getDecalageYAffichage(),
				this.getWidth()/2  / GameWorld.getWidthGrille(),
				this.getHeight()/2 / GameWorld.getHeightGrille());
		StdDraw.setPenColor(StdDraw.BLACK);
	}

}
