import java.util.ArrayList;

public class Armure extends Outil
{	
	protected ArrayList<Integer> classesPouvantLaPorter;

	

	/**
	 * Constructeur de armure
	 * @param nom
	 * @param x
	 * @param y
	 * @param defense
	 * @param width
	 * @param height
	 * @param quelleEntite
	 * @param classesPortant quelle classe peut porter cette armrue
	 */
	public Armure(String nom, double x, double y, int defense, double width, double height, int quelleEntite, ArrayList<Integer> classesPortant, int val)
	{
		//Outil : nom, positionX, positionY, largeur, hauteur, quelle arme?
		super(nom, x, y, defense, width, height, quelleEntite, val);
		classesPouvantLaPorter = new ArrayList<Integer>();
		classesPouvantLaPorter.addAll(classesPortant);
		genererTexteDescriptif();
	}
	
	/**
	 * genere texte descriptif pour l'inventaire
	 */
	public void genererTexteDescriptif()
	{
		texteDescriptif[0] = "Categorie : Armure";
		texteDescriptif[1] = "+" + this.getDefense() + " defense                              ";
		if(classesPouvantLaPorter!=null)
		{
			texteDescriptif[1] += "Classe possible :   " ;
			if (classesPouvantLaPorter.contains(Integer.valueOf(0)))
				texteDescriptif[1] += "     Mage           ";
			if (classesPouvantLaPorter.contains(1))
				texteDescriptif[1] += "     archer         ";
			if (classesPouvantLaPorter.contains(2))
				texteDescriptif[1] += "     Bourrin        ";
			if (classesPouvantLaPorter.contains(3))
				texteDescriptif[1] += "     Peon(tous)     ";
		}
	}
	
	public void setDefense(int def)
	{
		bonus=def;
	}
	public int getDefense()
	{
		return bonus;
	}
	
	public ArrayList<Integer> getClassesPouvantPorter()
	{
		return classesPouvantLaPorter;
	}

}
