public class Potion extends Consommable
{

	//pour pouvoir afficher plusieurs potions comme un seul item.
	private int leBonus;
	private int quelEffet;


/**
 * Constructeur de potion, avec tout ce dont la potion pourrait avoir besoin, et un nom different pour le hashmap
 * @param nom 
 * @param x
 * @param y
 * @param width
 * @param height
 * @param sol 
 * @param nb
 */
	public Potion(String nom, double x, double y, double width, double height, int nb, int bonus,  int effet, int val)
	{
		//Consommable : nom, positionX, positionY, largeur, hauteur
		//obstacle? si on se cogne dessus, nombre d'objets, quel objet
		super(nom , x, y, width, height, false, nb, Images.Entites.Error.val(), val);
		if(effet==0) //vie
			setEntite(Images.Entites.LifePotion.val());
		else if(effet==1) //mana
			setEntite(Images.Entites.ManaPotion.val());
		leBonus = bonus;
		quelEffet=effet;
		genererTexteDescriptif();
	}

	
	public void genererTexteDescriptif()
	{
		texteDescriptif[0] = "Categorie : Potion";
		texteDescriptif[1] = "Redonne " + this.leBonus + " points de";
		if(quelEffet==0)
			texteDescriptif[1]+="vie";
		else if(quelEffet==1)
			texteDescriptif[1]+="mana";
	}
	
	public Potion(Potion p)
	{
		//Consommable : nom, positionX, positionY, largeur, hauteur
		//obstacle? si on se cogne dessus, nombre d'objets, quel objet
		super(p.getNom(), p.getX(), p.getY(), p.getWidth(), p.getHeight(), false, p.getNombre(), p.getNumEntite(), p.getValeurItem());
		this.leBonus = p.getBonus();
		this.quelEffet = p.getEffet();
		genererTexteDescriptif();
	}

	/**
	 * ajoute le nombre nb de potions au stock
	 * @param nb
	 */

	public String toString()
	{
		return (super.toString()+ "  il y en a " + this.getNombre() );
	}
	
	public int getBonus()
	{
		return leBonus;
	}
	
	public int getEffet()
	{
		return quelEffet;
	}
	public void utiliser()
	{
		
	}
}
