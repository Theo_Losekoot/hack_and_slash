
public class Hitbox
{
	//attributs utiles pour les collisions
	private double posXM,posYM; // position X middle and position Y middle
	private double width, height;
	private boolean isObstacle;
	private boolean vivant;
	
	//les getters et setters.
	public double getPosXM() {	return posXM; }
	public void setPosXM(double posXM) { this.posXM = posXM; }
	public double getPosYM() { return posYM; }
	public void setPosYM(double posYM) { this.posYM = posYM; }
	public double getWidth() { return width; }
	public void setWidth(double width) { this.width = width; }
	public double getHeight() { return height; }
	public void setHeight(double height) { this.height = height; } 
	public boolean isObstacle() { return isObstacle; }
	public void setIsObstacle(boolean obstacle) { this.isObstacle = obstacle; } 
	
	/**
	 * Constructeur de la hitbox
	 * @param obstacle est-ce que c'est un obstacle ?
	 * @param etreVivant un etre vivant ?
	 */
	public Hitbox(boolean obstacle, boolean etreVivant)
	{
		posXM=posYM=width=height=0;
		isObstacle=obstacle;
		vivant=etreVivant;
	}
	/**
	 * Constructeur de la hitbox
	 * @param wid 
	 * @param hei
	 * @param obstacle
	 * @param etreVivant
	 */
	public Hitbox(double wid, double hei, boolean obstacle, boolean etreVivant)
	{
		posXM=posYM=0;
		width=wid;
		height=hei;
		isObstacle=obstacle;
		//si c'est un etre vivant, reduit la hitbox pour pas que la tete cogne
		if(etreVivant)
		{
			height/=1.5;
			posYM-=(hei/4);
		}
		vivant=etreVivant;
	}
	/**
	 * dernier constructeur de la hitbox
	 * @param wid
	 * @param hei
	 * @param x
	 * @param y
	 * @param obstacle
	 * @param etreVivant
	 */
	public Hitbox(double wid, double hei, double x, double y, boolean obstacle, boolean etreVivant)
	{
		posXM=x;
		posYM=y;
		width=wid;
		height=hei;
		isObstacle=obstacle;
		//si c'est un etreVivant, redit la hauteur de la hitbox pour ne pas qu'il se cogne la tete
		if(etreVivant)
		{
			height/=1.5;
			posYM-=(hei/3.5);
		}
		vivant=etreVivant;
	}
	/**
	 * detecte la collision entre deux Hitbox
	 * @param un premiere hitbox
	 * @param deux deuxieme hitbox
	 * @return true si il y a collision, false sinon
	 */
	public static boolean collision(Hitbox un, Hitbox deux)
	{
		   if((deux.getPosXM() - deux.getWidth()/2 >= un.getPosXM() + un.getWidth()/2)
		    || (deux.getPosXM() + deux.getWidth()/2 <= un.getPosXM() - un.getWidth()/2)
		    || (deux.getPosYM() - deux.getHeight()/2 >= un.getPosYM() + un.getHeight()/2)
		    || (deux.getPosYM() + deux.getHeight()/2 <= un.getPosYM() - un.getHeight()/2))
		          return false; 
			return true;
	}
	/**
	 * copie les attributs d'une hitbox a une autre
	 * @param un premiere
	 * @param deux deuxieme
	 */
	public static void copy(Hitbox un, Hitbox deux)
	{
		deux.setHeight(un.getHeight());
		deux.setPosXM(un.getPosXM());
		deux.setPosYM(un.getPosYM());
		deux.setWidth(un.getWidth());
	}
	/**
	 * met a jour les positions des hitbox, pratique pour les collisions
	 * @param x positionX
	 * @param y positionY
	 */
	public void update(double x, double y)
	{
		this.posXM=x;
		this.posYM=y;
		if(vivant)
		{
			this.posYM-=(this.height/4);
		}
	}

}
