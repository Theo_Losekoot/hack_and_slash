import java.awt.Font;

public class Main
{

	public static void main(String[] args)
	{

		// la carte sera une grille de taille tailleGrille * tailleGrille
		int tailleGrille = 25;
		Font font2 = new Font("family.ttf", Font.PLAIN, 16);
		StdDraw.setFont(font2);
		GameWorld world = new GameWorld(tailleGrille);

		
		// permet le double buffering, pour permettre l'animation
		StdDraw.enableDoubleBuffering();

		// la boucle principale de notre jeu
		while (GameWorld.isCharacterAlive() && !GameWorld.gameWon())
		{
			long debutTour = System.nanoTime();

			// Efface la fenetre graphique
			StdDraw.clear();
			
			//tests

			//System.out.println(GameWorld.getEntites().values());//.stream().filter(PorteMagique.class::isInstance) .collect(Collectors.toList()));

			world.processUserInput();

			world.step();

			// dessine la carte
			world.dessine();

			// Montre la fenetre graphique mise a jour et attends 20 millisecondes
			StdDraw.show();
			//fait une pause de 20ms, moins le temps du tour actuel 
			//(pour garder un rythme régulier malgres les differents temps que le tour a pu durer).
			Fonctions.pause(debutTour, GameWorld.TEMPS_RAFRAICHISSEMENT);


		}

		if (GameWorld.gameWon())
			System.out.println("Game won !");

	}

}
