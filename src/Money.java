public class Money extends Item
{
	public Money(String nom, double x, double y, double width, double height, int argent)
	{
		//Item : nom, positionX, positionY, largeur, hauteur, obstacle? quelle entite?
		super(nom+String.valueOf(numeroPaquet), x, y, width, height, false, Images.Entites.Money.val(), argent);
		quantite=argent;
		numeroPaquet++;
		genererTexteDescriptif();
	}
	
	private int quantite;
	private static int numeroPaquet=0;

	
	public void genererTexteDescriptif()
	{
		texteDescriptif[0] = "Categorie : Thunes";
		texteDescriptif[1] = "Je possede " + quantite + " Or                         Est-ce beaucoup ?      Quel est le sens de la vie ?";
	}
	
	public void setQuantite(int thunes)
	{
		quantite=thunes;
	}
	public int getQuantite()
	{
		return quantite;
	}
	
}
