
public abstract class Outil extends Item
{

	protected int bonus;

	public Outil(String nom, double x, double y, int bonus, double width, double height, int entite, int val)
	{
		//item : nom, positionX, positionY, largeur, hauteur, obstacle? quelleEntite?
		super(nom, x, y, width, height, false, entite, val);
		this.bonus = bonus;
		genererTexteDescriptif();

	}

	public Outil(String nom, double x, double y, int bonus, double width, double height, boolean estPose, int entite, int val)
	{
		super(nom, x, y, width, height, false, entite, val);
		this.bonus = bonus;

	}

	public int getBonus()
	{
		return this.bonus;
	}

}
