import java.awt.Font;
import java.util.ArrayList;
import java.util.LinkedList;

public class Marchand extends Entite
{

	private LinkedList<Item> mesItems;
	protected int objetSelectionne;
	protected int[] positionCurseur;
	protected LinkedList<Item> inventaireDuJoueur;
	private double[][] positionsDessinItems;
	private double[][] positionsDessinSurbrillance;
	
	
	public Marchand(String nom, double x, double y, double width, double height, boolean isObstacle, int entite)
	{
		//Entite : nom, positionX, positionY, largeur, hauteur, obstacle ? 
		//etre vivant ? quelleEntite?
		super(nom, x, y, width, height, isObstacle, true, entite);
		mesItems = new LinkedList<Item>();
		//met tous les items achetables en magasin
		initialiserItems();
		//pour l'affichage
		objetSelectionne=-1;
		positionCurseur = new int[1];
		positionCurseur[0]=0;
		inventaireDuJoueur = new LinkedList<Item>();
		positionsDessinItems = new double[4][39];
		positionsDessinSurbrillance = new double[4][39];
		initPositionDessins();
		
	}

	private void initialiserItems()
	{
		ArrayList<Integer> classesPortant = new ArrayList<Integer>();
		
		classesPortant.add(0);
		classesPortant.add(1);
		classesPortant.add(2);
		classesPortant.add(3);
		//tout le monde peut les porter
	//	mesItems.add(new ArmeCac("Arme cool1", this.getX(), this.getY(), 5, 3,3, Images.Entites.Epee1.val(),classesPortant , 10));
		mesItems.add(new ArmeCac("Arme cool2", this.getX(), this.getY(), 20, 3,3, Images.Entites.Epee2.val(),classesPortant, 10));
		mesItems.add(new ArmeCac("Arme cool3", this.getX(), this.getY(), 40, 3,3, Images.Entites.Epee3.val(), classesPortant, 15));
		classesPortant.clear();
		classesPortant.add(2);
		
		//seuls les bourrins peuvent les porter
		mesItems.add(new ArmeCac("Arme cool4", this.getX(), this.getY(), 50, 3,3, Images.Entites.Epee4.val(), classesPortant, 25));
		mesItems.add(new ArmeCac("Arme cool5", this.getX(), this.getY(), 120, 4,4, Images.Entites.Epee5.val(), classesPortant, 35));
		mesItems.add(new ArmeCac("Arme cool6", this.getX(), this.getY(), 200, 5,5, Images.Entites.Epee6.val(), classesPortant, 50));

		classesPortant.add(0);
		classesPortant.add(1);
		classesPortant.add(3);
		
		//tout le monde peut la porter, un peu de fun !

		
		classesPortant.clear();
		classesPortant.add(0);

		//seuls les mages peuvent porter
		mesItems.add(new ArmeTir("Arme Baton1" , this.getX(), this.getY(), 30, 2,2 , Images.Entites.Baton1.val(), classesPortant, 25, true, 10));
		mesItems.add(new ArmeTir("Arme Baton2" , this.getX(), this.getY(), 60, 2,2 , Images.Entites.Baton1.val(), classesPortant, 35, true, 10));
		mesItems.add(new ArmeTir("Arme Baton3" , this.getX(), this.getY(), 100, 2,2 , Images.Entites.Baton1.val(), classesPortant, 50, true, 10));
		
		classesPortant.clear();
		classesPortant.add(1);
		
		//seuls les archers peuvent porter
		mesItems.add(new ArmeTir("Arc1" , this.getX(), this.getY(), 10, 2,2 , Images.Entites.Arc1.val(), classesPortant, 25, false, 0));
		mesItems.add(new ArmeTir("Arc2" , this.getX(), this.getY(), 15, 2,2 , Images.Entites.Arc2.val(), classesPortant, 35, false, 0));
		mesItems.add(new ArmeTir("Arc3" , this.getX(), this.getY(), 20, 2,2 , Images.Entites.Arc3.val(), classesPortant, 40, false, 0));
		
		classesPortant.add(0);
		classesPortant.add(2);
		classesPortant.add(3);
		
		//tout le monde peut les porter
	//	mesItems.add(new Armure("armure1", this.getX(), this.getY(), 5, 1,1, Images.Entites.Armure1.val(), classesPortant, 50));
		
		classesPortant.clear();
		classesPortant.add(2);
		
		//seuls les bourrins peuvent porter
		mesItems.add(new Armure("armure2", this.getX(), this.getY(), 30, 1,1, Images.Entites.Armure2.val(), classesPortant, 12));
		mesItems.add(new Armure("armure3", this.getX(), this.getY(), 50, 1,1, Images.Entites.Armure3.val(), classesPortant, 25));
		mesItems.add(new Armure("armure4", this.getX(), this.getY(), 80, 1,1, Images.Entites.Armure4.val(), classesPortant, 50));
		
		classesPortant.clear();
		classesPortant.add(1);
		
		//seuls les archers peuvent porter
		mesItems.add(new Armure("armure5", this.getX(), this.getY(), 15, 1,1, Images.Entites.Armure5.val(), classesPortant, 12));
		mesItems.add(new Armure("armure6", this.getX(), this.getY(), 25, 1,1, Images.Entites.Armure6.val(), classesPortant, 25));
		mesItems.add(new Armure("armure7", this.getX(), this.getY(), 40, 1,1, Images.Entites.Armure7.val(), classesPortant, 50));
		
		classesPortant.clear();
		classesPortant.add(0);
		
		//seuls les mages peuvent equiper
		mesItems.add(new Armure("armure8", this.getX(), this.getY(), 10, 1,1, Images.Entites.Armure8.val(), classesPortant, 12));
		mesItems.add(new Armure("armure9", this.getX(), this.getY(), 15, 1,1, Images.Entites.Armure9.val(), classesPortant, 25));
		mesItems.add(new Armure("armure10", this.getX(), this.getY(), 20, 1,1, Images.Entites.Armure10.val(), classesPortant, 50));
		
		classesPortant.clear();
		
		//quelques bijoux
		mesItems.add(new Bijoux("bijou cool0", this.getX(), this.getY(), 20, 1,1,
				Bijoux.TypesBonus.vitesseAttaque.val(), Images.Entites.bijouVitesseAttaque.val(), 75));
	//	mesItems.add(new Bijoux("bijou cool1", this.getX(), this.getY(), 20, 1,1,
		//		Bijoux.TypesBonus.chance.val(), Images.Entites.bijouChance.val(), 50));
//		mesItems.add(new Bijoux("bijou cool2", this.getX(), this.getY(), 20, 1,1,
	//			Bijoux.TypesBonus.attaque.val(), Images.Entites.bijouDegats.val(), 20));
	//	mesItems.add(new Bijoux("bijou cool3", this.getX(), this.getY(), 20, 1,1,
//				Bijoux.TypesBonus.defense.val(), Images.Entites.bijouDefense.val(), 20));
	//	mesItems.add(new Bijoux("bijou cool4", this.getX(), this.getY(), 20, 1,1,
//				Bijoux.TypesBonus.gainXp.val(), Images.Entites.bijouGainXP.val(), 50));
//		mesItems.add(new Bijoux("bijou cool5", this.getX(), this.getY(), 20, 1,1,
//				Bijoux.TypesBonus.knockback.val(), Images.Entites.bijouKnockback.val(), 50));
		mesItems.add(new Bijoux("bijou cool6", this.getX(), this.getY(), 30, 1,1,
				Bijoux.TypesBonus.mana.val(), Images.Entites.bijouMana.val(), 75));
		mesItems.add(new Bijoux("bijou cool7", this.getX(), this.getY(), 50, 1,1,
				Bijoux.TypesBonus.pv.val(), Images.Entites.bijouVie.val(), 75));
		mesItems.add(new Bijoux("bijou cool8", this.getX(), this.getY(), 30, 1,1,
				Bijoux.TypesBonus.vitesse.val(), Images.Entites.bijouVitesse.val(), 75));
//		mesItems.add(new Bijoux("bijou cool9", this.getX(), this.getY(), 20, 1,1,
	//			Bijoux.TypesBonus.volVie.val(), Images.Entites.bijouVolVie.val(), 2));
		//999 potions de vie +40
		mesItems.add(new Potion("potion soin40", 10,10,1,1, 5,50,0, 20));
		//999 potions de vie +70
		mesItems.add(new Potion("potion soin70", 10,10,1,1, 5,75,0, 40));
		//999 potions de mana +50
		mesItems.add(new Potion("potion mana40", 10,10,1,1, 5,100,1, 20));
		
		mesItems.add(new Bombe("bombe", 0,0,1,1,false,5, 10));
		
		//mesItems.add(new ArmeCac("Arme crakey", this.getX(), this.getY(), 9000, 30,30, Images.Entites.EpeeCrakey.val(), classesPortant, 9001));


		
	}
	
	public void interagir()
	{
		//\"annule\" le enter qu'on a fait pour entrer en conversation avec le marchant, pour ne pas selectionner le premier item
		Keys.sync();
		inventaireDuJoueur = GameWorld.getJoueur().getInventaireNonEquipe();
		while(!processKeysInventory(positionCurseur))
		{
			StdDraw.clear();
			
			dessine(positionCurseur[0]);
	
			// Montre la fenetre graphique mise a jour et attends 20 millisecondes
			StdDraw.show();
		}
		Keys.sync();
	}
	
	
	
	/**
	 * reagit aux touches pressees par l'utilisateur, permet de se deplacer dans l'inventaire
	 * @param positionCurseur la postition du curseur actuellement
	 * @return true si on ferme l'inventaire
	 */
	public boolean processKeysInventory(int[] positionCurseur)
	{

		if(Keys.getKey(Keys.Touches.i.val()) && !Keys.getPreviousKey(Keys.Touches.i.val()))
		{
			return true;
		}
		if(Keys.getKey(Keys.Touches.escape.val()) && !Keys.getPreviousKey(Keys.Touches.escape.val()))
		{
			return true;
		}
		if(Keys.getKey(Keys.Touches.returnKey.val()) && !Keys.getPreviousKey(Keys.Touches.returnKey.val()))
		{
			if(objetSelectionne<0)
				objetSelectionne=positionCurseur[0];
			else
				action(objetSelectionne, positionCurseur[0]);
		}
		if(Keys.appuiBas())
		{
			bougerCurseur(0, positionCurseur);
		}
		if(Keys.appuiHaut())
		{
			bougerCurseur(2, positionCurseur);
		}
		if(Keys.appuiDroite())
		{
			bougerCurseur(1, positionCurseur);
		}
		if(Keys.appuiGauche())
		{
			bougerCurseur(3, positionCurseur);
		}
		
			Keys.sync();
			return false;
	}
		
	
	/*
	 * l'interface se presente comme : 
	 * 	
	 * 0	1	2	3 			28	29	30
	 * 4	5	6	7 			31	32	33
	 * 8	9	10	11			34	35	36
	 * 12	13	14	15
	 * 16	17	18	19
	 * 20	21	22	23			37
	 * 24	25	26	27			38
	 * 
	 * 
	 */
	
	private void bougerCurseur(int quelSens, int[] positionCurseur)
	{
	//Cette fonction repond a chacun des cas en particulier, pour avoir des 
			//deplacements dans l'inventaire plus propres
			switch(quelSens)
			{
			case 0: //que faire si le curseur doit se deplacer vers le bas
				//si curseur a gauche
				if(positionCurseur[0]>=0 && positionCurseur[0]<=23)
					positionCurseur[0]+=4;
				//si curseur a droite mais pas en bas
				else if((positionCurseur[0]>=28 && positionCurseur[0]<=33))
					positionCurseur[0]+=3;
				//si curseur en bas a droite
				else if	(positionCurseur[0]>=34 && positionCurseur[0]<=36)
					positionCurseur[0]=37;
					//si curseur sur acheter
				else if(positionCurseur[0]==37)
					positionCurseur[0]=38;
				return;

			case 1: //que faire si le curseur veut se deplacer vers la droite
				//si le curseur est sur une des cases qui ne necessite que de faire +1
				if(	(positionCurseur[0]>=0 && positionCurseur[0]<=2) ||
						(positionCurseur[0]>=4 && positionCurseur[0]<=6) ||
						(positionCurseur[0]>=8 && positionCurseur[0]<=10) ||
						(positionCurseur[0]>=12 && positionCurseur[0]<=14) ||
						(positionCurseur[0]>=16 && positionCurseur[0]<=18) ||
						(positionCurseur[0]>=20 && positionCurseur[0]<=22) ||
						(positionCurseur[0]>=24 && positionCurseur[0]<=26) ||
						(positionCurseur[0]>=28 && positionCurseur[0]<=29) ||
						(positionCurseur[0]>=31 && positionCurseur[0]<=32) ||
					(positionCurseur[0]>=34 && positionCurseur[0]<=35) )
					positionCurseur[0]++;
				//les position a droite du carre de gauche
				else if(positionCurseur[0]==3)
					positionCurseur[0]=28;
				else if(positionCurseur[0]==7)
					positionCurseur[0]=31;
				else if(positionCurseur[0]==11)
					positionCurseur[0]=34;
				else if(positionCurseur[0]==15)
					positionCurseur[0]=34;
				else if(positionCurseur[0]==19)
					positionCurseur[0]=37;
				else if(positionCurseur[0]==23)
					positionCurseur[0]=37;
				else if (positionCurseur[0]==27)
					positionCurseur[0]=38;
				return;

			case 2: //que faire si le curseur doit se deplacer vers le haut
				//si curseur a gauche
				if(positionCurseur[0]>=4 && positionCurseur[0]<=27)
					positionCurseur[0]-=4;
				//si curseur a droite mais pas en bas
				else if((positionCurseur[0]>=31 && positionCurseur[0]<=36))
					positionCurseur[0]-=3;
				//si curseur en bas a droite
				else if	(positionCurseur[0]==37)
					positionCurseur[0]=35;
					//si curseur sur acheter
				else if(positionCurseur[0]==38)
					positionCurseur[0]=37;
				return;
				/*
				 * l'interface se presente comme : 
				 * 	
				 * 0	1	2	3 			28	29	30
				 * 4	5	6	7 			31	32	33
				 * 8	9	10	11			34	35	36
				 * 12	13	14	15
				 * 16	17	18	19
				 * 20	21	22	23			37
				 * 24	25	26	27			38
				 * 
				 * 
				 */
			case 3: //que faire si le curseur veut se deplacer vers la gauche
				//si le curseur est sur une des cases qui ne necessite que de faire -1
				if(	(positionCurseur[0]>=1 && positionCurseur[0]<=3) ||
						(positionCurseur[0]>=5 && positionCurseur[0]<=7) ||
						(positionCurseur[0]>=9 && positionCurseur[0]<=11) ||
						(positionCurseur[0]>=13 && positionCurseur[0]<=15) ||
						(positionCurseur[0]>=17 && positionCurseur[0]<=19) ||
						(positionCurseur[0]>=21 && positionCurseur[0]<=23) ||
						(positionCurseur[0]>=25 && positionCurseur[0]<=27) ||
						(positionCurseur[0]>=29 && positionCurseur[0]<=30) ||
						(positionCurseur[0]>=32 && positionCurseur[0]<=33) ||
						(positionCurseur[0]>=35 && positionCurseur[0]<=36))
					positionCurseur[0]--;
				
				//les position a gauche du carre de droite
				else if(positionCurseur[0]==28)
					positionCurseur[0]=3;
				else if(positionCurseur[0]==31)
					positionCurseur[0]=7;
				else if(positionCurseur[0]==34)
					positionCurseur[0]=11;
				else if(positionCurseur[0]==37)
					positionCurseur[0]=23;
				else if (positionCurseur[0]==38)
					positionCurseur[0]=27;
				return;
			}
	}

	public void action(int objet1, int objet2)
	{

		for(int i=0; i<=1; i++)
		{
			if(i==1)
			{
				int temp = objet1;
				objet1=objet2;
				objet2=temp;
			}
			if(objet1==objet2)
			{
				objetSelectionne=-1;
				return;
			}
			objetSelectionne=-1;
			//si on selectionne un item a vendre et qu'on ne clique pas sur acheter
			if(objet1<=27 && objet2!=37)
				return;
			
			//si onselectionne un item dans notre inventaire et qu'on clique sur autre chose que vendre
			if(objet1>=28 && objet1<=36 && objet2!=38)
				return;
			
			//objet1 selectionne un item du shop et objet2 le bouton acheter
			if(objet1<=27 && objet2==37)
			{
				if(objet1>=mesItems.size())
					return;
				Item object = mesItems.get(objet1);
				if(object==null)
					return;
				//achete l'ite a 1.8 fois son prix
				GameWorld.getJoueur().acheter(object, (int)(object.getValeurItem()*2));
				inventaireDuJoueur = GameWorld.getJoueur().getInventaireNonEquipe();
	
				return;
	
			}
			//objet1 selectionne un objet de l'inventaire et objet2 le bouton vendre
			else if(objet1>=28 && objet1<=36 && objet2==38)
			{
				System.out.println("j'essaie de vendre");
				if(objet1>=inventaireDuJoueur.size()+28)
					return;
				Item object = inventaireDuJoueur.get(objet1-28);
				if(object==null)
					return;
				//vend l'item a sa valeur
				System.out.println(object);
				GameWorld.getJoueur().vendre(object, (int)(object.getValeurItem()));
				inventaireDuJoueur = GameWorld.getJoueur().getInventaireNonEquipe();
				return;
			}	
		}
	}
	
	
	/**
	 * dessine l'inventaire, donc l'image de base, les items, et les cadres de surbrillance
	 * @param positionCurseur
	 */
	public void dessine(int positionCurseur)
	{
		StdDraw.picture(0.5,
				0.5,
				Anim.bonneImage(Images.Entites.guiMarchand.val()),
				1,
				1,
				0); 

		//affiche le \"curseur\"
		if(objetSelectionne<positionsDessinSurbrillance[0].length)
		{
			//pour le type de surbrillance
			int entite;
			if(positionCurseur==37)
				entite=Images.Entites.acheterSel.val();
			else if(positionCurseur==38)
				entite=Images.Entites.vendreSel.val();
			else
				entite=Images.Entites.Surbrillance.val();
			
			StdDraw.picture(positionsDessinSurbrillance[0][positionCurseur],
					positionsDessinSurbrillance[1][positionCurseur],
					Anim.bonneImage(entite),
					positionsDessinSurbrillance[2][positionCurseur],
					positionsDessinSurbrillance[3][positionCurseur],
					0);
		}
		
		//affiche la surbrillance sur l'objet selectionne
		if(objetSelectionne>=0 && objetSelectionne<positionsDessinSurbrillance[0].length)
		{
			//pour le type de surbrillance
			int entite;
			if(objetSelectionne==37)
				entite=Images.Entites.acheterSel.val();
			else if(objetSelectionne==38)
				entite=Images.Entites.vendreSel.val();
			else
				entite=Images.Entites.Surbrillance.val();
			StdDraw.picture(positionsDessinSurbrillance[0][objetSelectionne],
					positionsDessinSurbrillance[1][objetSelectionne],
					Anim.bonneImage(entite),
					positionsDessinSurbrillance[2][objetSelectionne],
					positionsDessinSurbrillance[3][objetSelectionne],
					0);
		}

		for(int i=0; i<positionsDessinItems[0].length; i++)
		{
			Item lItem = null;
			if(i<=27 && mesItems.size()>i)
				lItem = mesItems.get(i);
			else if(i>=28 && i<=36 && inventaireDuJoueur.size()+28 > i)
			{
				lItem = inventaireDuJoueur.get(i-28);
			}
			if(lItem!=null)
			{

				double changement0=7*positionsDessinItems[2][i]/10;
				double changement1=5*positionsDessinItems[3][i]/10;
				//si c'est une arme, change sa taille pour l'afficher correctement.
				if(lItem instanceof ArmeCac)
				{					
					positionsDessinItems[0][i]-=changement0;
					positionsDessinItems[1][i]-=changement1;
					positionsDessinItems[2][i]=positionsDessinItems[2][i]*27/10;
					positionsDessinItems[3][i]=positionsDessinItems[3][i]*27/10 *2/3;
				}
				StdDraw.picture(positionsDessinItems[0][i],
							positionsDessinItems[1][i],
							Anim.bonneImage(lItem.getNumEntite()),
							positionsDessinItems[2][i],
							positionsDessinItems[3][i],
							0);
				//puis remet sa taille initiale.
				if(lItem instanceof ArmeCac)
				{
					positionsDessinItems[0][i]+=changement0;
					positionsDessinItems[1][i]+=changement1;
					positionsDessinItems[2][i]=positionsDessinItems[2][i]*10/27;
					positionsDessinItems[3][i]=positionsDessinItems[3][i]*10/27 *3/2;
				}
				afficherQuantite(i);
			}
		}
		afficheCaracteristiquesItem(positionCurseur);
		afficheMesThunes();
	}
	
	/**
	 * affiche notre nombre d'argent
	 */
	public void afficheMesThunes()
	{
		StdDraw.picture(0.54,
				0.85,
				Anim.bonneImage(Images.Entites.Money.val()),
				0.08,
				0.08,
				0);
		StdDraw.setFont(new Font("Arial", Font.PLAIN, 20));

		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.text(0.54, 0.90, String.valueOf(GameWorld.getJoueur().getRichesse()));
	}
	
	/**
	 * affiche la quantite des items empiles dans l'inventaire
	 * @param i
	 */
	public void afficherQuantite(int i)
	{
		Item object=null;
		
		if((i>=28 && i<=36))
			 object = inventaireDuJoueur.get(i-28);
		if(i>=0 && i<=27)
			object = mesItems.get(i);
		
		if(object!=null && object instanceof Consommable)
		{
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.text(positionsDessinItems[0][i]+positionsDessinItems[2][i]/2,
				positionsDessinItems[1][i]-6*positionsDessinItems[3][i]/10,
				String.valueOf((((Consommable)object).getNombre())));
		}
	}
	
	/**
	 * affiche les caracteristiques des items ainsi que leur prix
	 * @param positionCurseur
	 */
	public void afficheCaracteristiquesItem(int positionCurseur)
	{
		Item lItem = null;
		if(positionCurseur<=27 && mesItems.size()>positionCurseur)
		{
			lItem = mesItems.get(positionCurseur);
		}
		else if(positionCurseur>=28 && positionCurseur<=36 && inventaireDuJoueur.size()+28 > positionCurseur)
		{
			lItem = inventaireDuJoueur.get(positionCurseur-28);
		}
		if(lItem!=null)
		{	
			if(positionCurseur<=27)
			{
				lItem.setValeurItem(lItem.getValeurItem()*2);
			}
			StdDraw.setPenColor(StdDraw.BLACK);
			Font oldFont = StdDraw.getFont();
			StdDraw.setFont(new Font("Arial", Font.PLAIN, 13));
			String description1 = lItem.getTexteDescriptif()[1];
			StdDraw.text(0.71,
					0.52,
					lItem.getTexteDescriptif()[0]);
					LinkedList<String> description= new LinkedList<String>();
			int a;
			for(a=20; a<description1.length(); a+=20)
			{
				description.add(description1.substring(a-20, a));
			}
			description.add(description1.substring(a-20));
			for(int nombreChaines=0; nombreChaines<description.size(); nombreChaines++)
			{
				StdDraw.textLeft(0.69,
						0.47-0.025*nombreChaines,
						description.get(nombreChaines));
			}
			StdDraw.textLeft(0.69,
					0.47-0.025*description.size(),
					"Valeur : " + lItem.getValeurItem());
			StdDraw.setFont(oldFont);
			if(positionCurseur<=27)
			{
				lItem.setValeurItem(lItem.getValeurItem()/2);
			}
		}
	}
	public void dessine()
	{
		
				//Dessine l'item avec la bonne orientation
				StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
						(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
						Anim.bonneImage(this.quelleEntite),
						(double)this.getWidth()/ GameWorld.getWidthGrille(),
						(double)this.getHeight()/GameWorld.getHeightGrille(),
						0); 			
	}
	public void dessine(double rotation)
	{
				//Dessine l'item avec la bonne orientation
				StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
						(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
						Anim.bonneImage(this.quelleEntite),
						(double)this.getWidth()/ GameWorld.getWidthGrille(),
						(double)this.getHeight()/GameWorld.getHeightGrille(),
						rotation); 			
	}

	/**
	 * 	calcule les emplacements de l'inventaire pour l'affichage, 
	 *  pour afficher les items au bons endroits et la surbrillance aussi
	 *  les valeurs sont codees dans le dur car l'inventaire est une image
	 *  et donc ces valeurs, entre 0 et 1, ne changeront jamais.
	 */
	private void initPositionDessins()
	{
		//LES POSITIONS DE SURBRILLANCE :
		//les 28 items de gauche : 
		//l'espacement entre chaque carre
		double espacementX = 0.0773;
		double espacementY = 0.1152;
		//la taille des carres
		double largeurCarre = 0.072;
		double hauteurCarre = largeurCarre* 3/2;
		for(int l=0; l<4; l++)
		{
			for(int h=0; h<7; h++)
			{
				positionsDessinSurbrillance[0][l+4*h] = 0.1722 + l*espacementX;
				positionsDessinSurbrillance[1][l+4*h] = 0.837 - h*espacementY;
				positionsDessinSurbrillance[2][l+4*h] = largeurCarre;
				positionsDessinSurbrillance[3][l+4*h] = hauteurCarre;
			}
		}
		
		//LES POSITIONS DE SURBRILLANCE :
		//les 9 items de droite : 
		//l'espacement entre chaque carre
		double espacementX2 = 0.0771;
		double espacementY2 = 0.1152;
		//la taille des carres
		double largeurCarre2 = 0.072;
		double hauteurCarre2 = largeurCarre2* 3/2;
		for(int l=0; l<3; l++)
		{
			for(int h=0; h<3; h++)
			{
				positionsDessinSurbrillance[0][28+l+3*h] = 0.675 + l*espacementX2;
				positionsDessinSurbrillance[1][28+l+3*h] = 0.82 - h*espacementY2;
				positionsDessinSurbrillance[2][28+l+3*h] = largeurCarre2;
				positionsDessinSurbrillance[3][28+l+3*h] = hauteurCarre2;
			}
		}
		//Les 2 boutons
		positionsDessinSurbrillance[0][37] = 0.754;
		positionsDessinSurbrillance[1][37] = 0.235;
		positionsDessinSurbrillance[2][37] = 0.164;
		positionsDessinSurbrillance[3][37] = 0.065;
		
		positionsDessinSurbrillance[0][38] = 0.754;
		positionsDessinSurbrillance[1][38] = 0.151;
		positionsDessinSurbrillance[2][38] = 0.164;
		positionsDessinSurbrillance[3][38] = 0.061;
		
		
		
		//LES POSITIONS D'ITEM : 
		//-2 : les 2 boutons
		for(int i=0; i<positionsDessinSurbrillance[0].length-2; i++)
		{
			positionsDessinItems[0][i]=positionsDessinSurbrillance[0][i];
			positionsDessinItems[1][i]=positionsDessinSurbrillance[1][i];
			positionsDessinItems[2][i]=positionsDessinSurbrillance[2][i]*6/10;
			positionsDessinItems[3][i]=positionsDessinSurbrillance[3][i]*6/10;
		}
		
		
	}

	
}
