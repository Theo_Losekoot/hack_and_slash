import java.awt.Image;
import java.util.LinkedList;

public class Anim
{
	private static LinkedList<EntiteTimer> entitesDisparaissant = new LinkedList<EntiteTimer>();
	private static LinkedList<TexteTemporaire> texteDisparaissant = new LinkedList<TexteTemporaire>();

	/**
	 * ajoute des entites qui ne jouent pas, juste pour etre affichees.
	 * @param e
	 */
	public static void addEntiteDisp(EntiteTimer e)
	{
		entitesDisparaissant.add(e);

	}
	
	public static void addTextDisp(TexteTemporaire e)
	{
		texteDisparaissant.add(e);
	}
	
	/**
	 * supprime toutes les entites, pour le changement de map par exemple
	 */
	public static void flushEntitesDisparaissant()
	{
		entitesDisparaissant.clear();
		texteDisparaissant.clear();
	}
	
	/**
	 * met a jour les entites devant disparaitre, et les supprime si leur temps est venu
	 */
	public static void updateEntitesDisp()
	{
		for(int i=0; i<entitesDisparaissant.size(); i++)
		{
			EntiteTimer e = entitesDisparaissant.get(i);
			if(e.diminuerTTL())
			{
				entitesDisparaissant.remove(i);
				i--;
			}
		}
	}
	/**
	 * update les entites et les renvoie.
	 * @return les entites disparaissant
	 */
	public static LinkedList<EntiteTimer> getEntitesDisparaissant()
	{
		updateEntitesDisp();
		return entitesDisparaissant;
	}
	
	
	/**
	 * met a jour les Textes devant disparaitre, et les supprime si leur temps est venu
	 */
	public static void updateTextesDisp()
	{
		for(int i=0; i<texteDisparaissant.size(); i++)
		{
			EntiteTimer e = texteDisparaissant.get(i);
			if(e.diminuerTTL())
			{
				texteDisparaissant.remove(i);
				i--;
			}
		}
	}
	/**
	 * update les entites et les renvoie.
	 * @return les entites disparaissant
	 */
	public static LinkedList<TexteTemporaire> getTextesDisparaissant()
	{
		updateTextesDisp();
		return texteDisparaissant;
	}
	
	
	/**
	 * 
	 * @param type Le type de personne (joueur / monstre / piege, etc)
	 * @param precedenteposition[0] pour enchainer sur l'image suivante
	 * @param vecteurAvancer pour orienter le personnage correctement
	 * @return la bonne image à afficher
	 */
	public static Image bonneImage(int type, int[] position, Vector vecteurAvancer, boolean droite, boolean frappe)
	{
		if(type==Images.Entites.Perso.val())
			return getBonneImageJoueur(position, vecteurAvancer, droite, frappe);
		else if (type==Images.Entites.Lapin.val())
			return getBonneImageLapin(position, vecteurAvancer, droite);
		else if(type==Images.Entites.Poule.val())
			return getBonneImagePoule(position, vecteurAvancer, droite);
		else if(type==Images.Entites.Paysan.val() || type==Images.Entites.PaysanInvocateur.val())
			return getBonneImagePaysan(position, vecteurAvancer, droite, frappe);
		else if(type==Images.Entites.Sort1.val())
			return getBonneImageSort1(position, droite);
		else if(type==Images.Entites.Fleche.val())
			return getBonneImageFleche(droite);
		else if(type==Images.Entites.Trolls.val())
			return getBonneImageTroll(position, vecteurAvancer, droite);
		return Images.getError();
	}
	public static Image bonneImage(int type, int[] position, Vector vecteurAvancer, boolean droite, boolean frappe, int tempsFrappe)
	{
		if(type==Images.Entites.Dragon.val())
			return getBonneImageDragon(position, vecteurAvancer, droite, frappe, tempsFrappe);
		else return Images.getError();

	}

	/**
	 * renvoie la bonne image dans le cas d'une image sans animation
	 * @param type le type de l'entite
	 * @return l'image correspondante a l'entite
	 */
	public static Image bonneImage(int type)
	{
		if(type==Images.Entites.ArbreForet.val())
			return Images.getArbre1();
		else if(type==Images.Entites.Bombe.val())
			return Images.getBombe();
		else if(type==Images.Entites.Bijoutier.val())
			return Images.getBijoutier();
		else if(type==Images.Entites.Forgeron.val())
			return Images.getForgeron();
		else if(type==Images.Entites.Moulin.val())
			return Images.getMoulin();
		else if(type==Images.Entites.Vieux.val())
			return Images.getVieux();
		else if(type==Images.Entites.Arbre2.val())
			return Images.getArbre2();
		else if(type==Images.Entites.Helice.val())
			return Images.getHelice();
		else if(type==Images.Entites.LifePotion.val())
			return Images.getLifePotion();
		else if(type==Images.Entites.Cle.val())
			return Images.getClef();
		else if(type==Images.Entites.ManaPotion.val())
			return Images.getManaPotion();
		else if(type==Images.Entites.Inventaire.val())
			return Images.getInventaire();
		else if(type==Images.Entites.Mage.val())
			return Images.getMage();
		else if(type==Images.Entites.Distance.val())
			return Images.getDistance();
		else if(type==Images.Entites.Baston.val())
			return Images.getBaston();
		else if(type==Images.Entites.Surbrillance.val())
			return Images.getSurbrillance();
		else if (type==Images.Entites.Sang.val())
			return Images.getSang();
		else if(type==Images.Entites.Pilier.val())
			return Images.getPilier();
		else if(type==Images.Entites.Money.val())
			return Images.getMoney();
		else if(type == Images.Entites.Bar.val())
			return Images.getBar();
		else if(type== Images.Entites.Epee1.val())
			return Images.getEpees()[0];
		else if(type== Images.Entites.Epee2.val())
			return Images.getEpees()[1];
		else if(type== Images.Entites.Epee3.val())
			return Images.getEpees()[2];
		else if(type== Images.Entites.Epee4.val())
			return Images.getEpees()[3];
		else if(type== Images.Entites.Epee5.val())
			return Images.getEpees()[4];
		else if(type== Images.Entites.Epee6.val())
			return Images.getEpees()[5];
		else if(type== Images.Entites.EpeeCrakey.val())
			return Images.getEpees()[6];
		else if(type==Images.Entites.Arc1.val())
			return Images.getArcs()[0];
		else if(type==Images.Entites.Arc2.val())
			return Images.getArcs()[1];
		else if(type==Images.Entites.Arc3.val())
			return Images.getArcs()[2];
		else if(type==Images.Entites.Baton1.val())
			return Images.getBatons()[0];
		else if(type==Images.Entites.Baton2.val())
			return Images.getBatons()[1];
		else if(type==Images.Entites.Baton3.val())
			return Images.getBatons()[2];
		else if(type==Images.Entites.Armure1.val())
			return Images.getArmures()[0];
		else if(type==Images.Entites.Armure2.val())
			return Images.getArmures()[1];
		else if(type==Images.Entites.Armure3.val())
			return Images.getArmures()[2];
		else if(type==Images.Entites.Armure4.val())
			return Images.getArmures()[3];
		else if(type==Images.Entites.Armure5.val())
			return Images.getArmures()[4];
		else if(type==Images.Entites.Armure6.val())
			return Images.getArmures()[5];
		else if(type==Images.Entites.Armure7.val())
			return Images.getArmures()[6];
		else if(type==Images.Entites.Armure8.val())
			return Images.getArmures()[7];
		else if(type==Images.Entites.Armure9.val())
			return Images.getArmures()[8];
		else if(type==Images.Entites.Armure10.val())
			return Images.getArmures()[9];
		else if(type==Images.Entites.bijouChance.val())
			return Images.getBijoux()[0];
		else if(type==Images.Entites.bijouDefense.val())
			return Images.getBijoux()[1];
		else if(type==Images.Entites.bijouDegats.val())
			return Images.getBijoux()[2];
		else if(type==Images.Entites.bijouMana.val())
			return Images.getBijoux()[3];
		else if(type==Images.Entites.bijouKnockback.val())
			return Images.getBijoux()[4];
		else if(type==Images.Entites.bijouVie.val())
			return Images.getBijoux()[5];
		else if(type==Images.Entites.bijouVitesse.val())
			return Images.getBijoux()[6];
		else if(type==Images.Entites.bijouVitesseAttaque.val())
			return Images.getBijoux()[7];
		else if(type==Images.Entites.bijouVolVie.val())
			return Images.getBijoux()[8];
		else if(type==Images.Entites.bijouGainXP.val())
			return Images.getBijoux()[9];
		else if(type==Images.Entites.Porte.val())
			return Images.getPorte();
		else if(type==Images.Entites.PorteFermee.val())
			return Images.getPorteFermee();
		else if(type==Images.Entites.PorteBoss.val())
			return Images.getPorteBoss();
		else if(type==Images.Entites.PorteBossFermee.val())
			return Images.getPorteBossFermee();
		else if (type==Images.Entites.guiMarchand.val())
			return Images.getGuiMarchand();
		else if (type==Images.Entites.acheterSel.val())
			return Images.getAcheterSel();
		else if (type==Images.Entites.vendreSel.val())
			return Images.getVendreSel();
		else if(type==Images.Entites.EmblemeMago.val())
			return Images.getEmblemes()[0];
		else if(type==Images.Entites.EmblemeDist.val())
			return Images.getEmblemes()[1];
		else if(type==Images.Entites.EmblemeBast.val())
			return Images.getEmblemes()[2];
		else if(type==Images.Entites.SelectArcher.val())
			return Images.getSelectArcher();
		else if(type==Images.Entites.SelectMage.val())
			return Images.getSelectMage();
		else if(type==Images.Entites.SelectBourrin.val())
			return Images.getSelectBourrin();
		else if(type==Images.Entites.ChoixArbre.val())
			return Images.getChoixClasse();

		return Images.getError();
	}
	/**
	 * renvoie la bonne image dans le cas d'une image avec animation mais entite non vivante
	 * @param type le type de l'entite
	 * @return l'image correspondante a l'entite
	 */
	public static Image bonneImage(int type, int[] position)
	{
		if(type==Images.Entites.Fontaine.val())
		{
			int tempsSurImage=5;
			if(position[0]<tempsSurImage)
				return Images.getFontaine()[0];
			if(position[0]<2*tempsSurImage)
				return Images.getFontaine()[1];
			if(position[0]<3*tempsSurImage)
				return Images.getFontaine()[2];
			else
			{
				position[0]=1;
				return Images.getFontaine()[0];
			}
		}
		else if(type==Images.Entites.Explosion.val())
		{
			double tempsSurImage=(double)(501/GameWorld.TEMPS_RAFRAICHISSEMENT)/10;
			for(int i=0; i<Images.getExplosion().length; i++)
			{
				if(position[0]<(i+1)*tempsSurImage)
					return Images.getExplosion()[i];
			}
		}
		else if (type==Images.Entites.Pique.val())
		{
			if(position[0]==0)
				return Images.getPique()[0];
			else if(position[0]==1)
				return Images.getPique()[1];
		}
		else if(type==Images.Entites.Sort1.val())
		{
			int tempsSurImage=8;
			if(position[0]>=Images.getSort1().length*tempsSurImage)
				position[0]=0;
			for(int i=0; i<Images.getSort1().length; i++)
			{
				if(position[0]<(i+1)*tempsSurImage)
					return Images.getSort1()[i];
			}
		}
		return bonneImage(type);
	}

	
	/**
	 * renvoie la bonne image pour le joueur, methode appelee par celle plus haut.
	 * @param position - niveau de l'aniùation du joueur
	 * @param vecteurAvancer le vecteur deplacement du personnage
	 * @param droite si le personage est oriente a droite
	 * @param frappe le personnage frappe actuellement?
	 * @return la bonne image
	 */
	private static Image getBonneImageJoueur(int[] position, Vector vecteurAvancer, boolean droite, boolean frappe)
	{
		//definit 2-3 variables servant a selectionner la bonne image
		int tempsSurImage=12;
		int orientation;
		if(frappe)
		{
			if(droite)
				return Images.getPersoFrappeDroite();
			else
				return Images.getPersoFrappeGauche();
		}
		if(vecteurAvancer.getX()<vecteurAvancer.getCritereArret()*2.5
				&& vecteurAvancer.getX() > -vecteurAvancer.getCritereArret()*2.5
				&& vecteurAvancer.getY() < vecteurAvancer.getCritereArret()*2.5
				&& vecteurAvancer.getY() > -vecteurAvancer.getCritereArret()*2.5)
		{
			orientation=0;
		}
		else if (vecteurAvancer.getX()<0 || (vecteurAvancer.getX()>-vecteurAvancer.getCritereArret()*2.5 && !droite))
		{
			orientation=-1;
		}
		else
		{
			orientation=1;
		}
		//renvoie la bonne image en fonction du moent de l'animation et des caracteristiques (droite, etc) du personnage
		switch(orientation)
		{
		case -1 :
			if(position[0]>=tempsSurImage*4)
				position[0]=0;
			if(position[0]<tempsSurImage)
				return Images.getPerso12();
			else if(position[0]<2*tempsSurImage)
				return Images.getPerso13();
			else if(position[0]<3*tempsSurImage)
				return Images.getPerso14();
			else
				return Images.getPerso15();
		case 1 :
			if(position[0]>=tempsSurImage*4)
				position[0]=0;
			if(position[0]<tempsSurImage)
				return Images.getPerso2();
			else if(position[0]<2*tempsSurImage)
				return Images.getPerso3();
			else if(position[0]<3*tempsSurImage)
				return Images.getPerso4();
			else
				return Images.getPerso5();
		case 0 :
			if(position[0]>=tempsSurImage*5)
				position[0]=0;
			if(droite)
			{
				if(position[0]<tempsSurImage*2.5)
					return Images.getPerso0();
				else
					return Images.getPerso1();
			}
			else
			{
				if(position[0]<tempsSurImage*2.5)
					return Images.getPerso10();
				else
					return Images.getPerso11();
			}
		}		
		return Images.getError();
	}
	
	/**
	 * renvoie la bonne image pour le lapin en fonction de certaines de ses caracteristiques
	 * @param position
	 * @param vecteurAvancer
	 * @param droite
	 * @return la bonne image
	 */
	public static Image getBonneImageLapin(int[] position, Vector vecteurAvancer, boolean droite)
	{
		//definit 2-3 variables servant a selectionner la bonne image
		int tempsSurImage=12;
		int orientation;

		if (!droite && (vecteurAvancer.getX()<-vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
		{
			orientation=-1;
		}
		else if (droite && (vecteurAvancer.getX()>vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
		{
			orientation=1;
		}
		else
			orientation=0;
		switch(orientation)
		{
		case 0 :
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(droite)
				return Images.getLapin()[0];
			else
				return Images.getLapin()[3];
		case 1:
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(position[0]<tempsSurImage*1)
				return Images.getLapin()[0];
			else if (position[0]<tempsSurImage*2)
				return Images.getLapin()[1];
			else
				return Images.getLapin()[2];
		case -1 :
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(position[0]<tempsSurImage*1)
				return Images.getLapin()[3];
			else if (position[0]<tempsSurImage*2)
				return Images.getLapin()[4];
			else
				return Images.getLapin()[5];
		}
		return Images.getError();
	}
	
	/**
	 * retourne la bonne image pour la poule
	 * @param position
	 * @param vecteurAvancer
	 * @param droite
	 * @return
	 */
	public static Image getBonneImagePoule(int[] position, Vector vecteurAvancer, boolean droite)
	{
		//definit 2-3 variables servant a selectionner la bonne image
				int tempsSurImage=4;
				int orientation;

				if (!droite && (vecteurAvancer.getX()<-vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
				{
					orientation=-1;
				}
				else if (droite && (vecteurAvancer.getX()>vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
				{
					orientation=1;
				}
				else
					orientation=0;
				switch(orientation)
				{
				case 0 :
					if(position[0]>=tempsSurImage*3)
						position[0]=0;
					if(droite)
						return Images.getPoule()[0];
					else
						return Images.getPoule()[3];
				case 1:
					if(position[0]>=tempsSurImage*3)
						position[0]=0;
					if(position[0]<tempsSurImage*1)
						return Images.getPoule()[0];
					else if (position[0]<tempsSurImage*2)
						return Images.getPoule()[1];
					else
						return Images.getPoule()[2];
				case -1 :
					if(position[0]>=tempsSurImage*3)
						position[0]=0;
					if(position[0]<tempsSurImage*1)
						return Images.getPoule()[3];
					else if (position[0]<tempsSurImage*2)
						return Images.getPoule()[4];
					else
						return Images.getPoule()[5];
				}
				return Images.getError();
	}

	/**
	 * affiche la bone image pour le paysan et le paysan invocateur (c'est les memes, pas le temps)
	 * @param position
	 * @param vecteurAvancer
	 * @param droite
	 * @param frappe
	 * @return
	 */
	public static Image getBonneImagePaysan(int[] position, Vector vecteurAvancer, boolean droite, boolean frappe)
	{
		//definit 2-3 variables servant a selectionner la bonne image
		int tempsSurImage=10;
		int orientation;

		if (!droite && (vecteurAvancer.getX()<-vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
		{
			orientation=-1;
		}
		else if (droite && (vecteurAvancer.getX()>vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
		{
			orientation=1;
		}
		else
			orientation=0;
		switch(orientation)
		{
		case 0 :
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(droite)
				return Images.getPaysan()[0];
			else
				return Images.getPaysan()[3];
		case 1:
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(position[0]<tempsSurImage*1)
				return Images.getPaysan()[0];
			else if (position[0]<tempsSurImage*2)
				return Images.getPaysan()[1];
			else
				return Images.getPaysan()[2];
		case -1 :
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(position[0]<tempsSurImage*1)
				return Images.getPaysan()[3];
			else if (position[0]<tempsSurImage*2)
				return Images.getPaysan()[4];
			else
				return Images.getPaysan()[5];
		}
		return Images.getError();
	}
	
	/**
	 * renvoie la bonne image pour le sort 1
	 * @param position
	 * @param droite
	 * @return
	 */
	public static Image getBonneImageSort1(int[] position, boolean droite)
	{
			int tempsSurImage=8;
			if(position[0]>=Images.getSort1().length/2*tempsSurImage)
				position[0]=0;
			for(int i=0; i<Images.getSort1().length/2; i++)
			{
				int a=i;
				if(droite)
					a+=Images.getSort1().length/2;
				if(position[0]<(i+1)*tempsSurImage)
					return Images.getSort1()[a];
			}
			return Images.getError();
		}
		
	
	public static Image getBonneImageTroll(int[] position, Vector vecteurAvancer, boolean droite)
	{
		//definit 2-3 variables servant a selectionner la bonne image
		int tempsSurImage=10;
		int orientation;

		if (!droite && (vecteurAvancer.getX()<-vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
		{
			orientation=-1;
		}
		else if (droite && (vecteurAvancer.getX()>vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
		{
			orientation=1;
		}
		else
			orientation=0;
		switch(orientation)
		{
		case 0 :
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(droite)
				return Images.getTroll()[0];
			else
				return Images.getTroll()[3];
		case 1:
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(position[0]<tempsSurImage*1)
				return Images.getTroll()[0];
			else if (position[0]<tempsSurImage*2)
				return Images.getTroll()[1];
			else
				return Images.getTroll()[2];
		case -1 :
			if(position[0]>=tempsSurImage*3)
				position[0]=0;
			if(position[0]<tempsSurImage*1)
				return Images.getTroll()[3];
			else if (position[0]<tempsSurImage*2)
				return Images.getTroll()[4];
			else
				return Images.getTroll()[5];
		}
		return Images.getError();	}
	
	public static Image getBonneImageDragon(int[] position, Vector vecteurAvancer, boolean droite, boolean frappe, int tempsFrappe)
	{
		//definit 2-3 variables servant a selectionner la bonne image
		int tempsSurImage=12;
		int orientation;

		if (!droite && (vecteurAvancer.getX()<-vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
		{
			orientation=-1;
		}
		else if (droite && (vecteurAvancer.getX()>vecteurAvancer.getCritereArret() || Math.abs(vecteurAvancer.getY())>vecteurAvancer.getCritereArret()))
		{
			orientation=1;
		}
		else
			orientation=0;
		if(frappe)
		{
			if(droite)
			{
				//trouve l'entier de l'image, entre 0 et 6
				int quelleImage=(tempsFrappe*7)/Dragon.getUsualDurationAttack();
				//retourne les 7 images de frappe de droite
				return Images.getDragon()[10+quelleImage];
			}
			else
			{
				//trouve l'entier de l'image, entre 0 et 6
				int quelleImage=(tempsFrappe*7)/Dragon.getUsualDurationAttack();
				//retourne les 7 images de frappe de droite
				if(quelleImage>=7)
					quelleImage=6;
				return Images.getDragon()[17+quelleImage];		
			}
		}
		
		//renvoie la bonne image en fonction du moment de l'animation et des caracteristiques (droite, etc) du personnage
		switch(orientation)
		{
		case -1 : // les images de gauche
			if(position[0]>=tempsSurImage*5)
				position[0]=0;
			if(position[0]<tempsSurImage)
			for(int i=0; i<5; i++)
			{
				if(position[0]<tempsSurImage*(i+1))
				{
					return Images.getDragon()[5+i];
				}

			}
		case 1 : //les images de droite
			if(position[0]>=tempsSurImage*5)
				position[0]=0;
			if(position[0]<tempsSurImage)
			for(int i=0; i<5; i++)
			{
				if(position[0]<tempsSurImage*(i+1))
				{
					return Images.getDragon()[i];
				}

			}
		case 0 :
			if(position[0]>=tempsSurImage*5)
				position[0]=0;
			if(droite)
			{
				if(position[0]<tempsSurImage*2.5)
					return Images.getDragon()[0];
				else
					return Images.getDragon()[1];
			}
			else
			{
				if(position[0]<tempsSurImage*2.5)
					return Images.getDragon()[5];
				else
					return Images.getDragon()[6];
			}
		}		
		return Images.getError();	
	}

	
	public static Image getBonneImageFleche(boolean droite)
	{
		if(droite)
			return Images.getFlecheDroite();
		else
			return Images.getFlecheGauche();
	}

}
