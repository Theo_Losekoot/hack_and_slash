import java.util.List;
import java.util.stream.Collectors;

public class Sort extends Projectile
{
		
	public Sort(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width,
			double height, double _recul, int entite, boolean _droite, double _rayonAction, boolean magique)
	{
		super(nom, x, y, pDV, pAtk, pDef, vitesse, width, height, _recul, entite, _droite, 5, magique);
		droite=_droite;
		rayonAction=_rayonAction;
		initVecteur();
	}
	
	
	
	@Override
	public void actualiserCoup()
	{
		if((TTL-=GameWorld.TEMPS_RAFRAICHISSEMENT)<0)
			mourir();
		List<Entite> lesObstacles = GameWorld.getEntites().values().stream()
				.filter(x -> x.isObstacle() && !(x instanceof Joueur) || x instanceof Monstre && !(x instanceof Projectile)).
				collect(Collectors.toList());
		for(Entite e : lesObstacles)
		{
			this.getHB().setWidth(this.getHB().getWidth()*1.5);
			this.getHB().setHeight(this.getHB().getHeight()*1.3);
			if(Hitbox.collision(this.getHB(), e.getHB()))
			{
				if(e instanceof Monstre )
					attaquer((EtreVivant)e);
				else
					mourir();
			}
			this.getHB().setWidth(this.getHB().getWidth()*(1/1.5));
			this.getHB().setHeight(this.getHB().getHeight()*(1/1.3));
		}

	}
	
	/**
	 * ne prend aucun degats.
	 */
	@Override
	public int prendreDegats(int attaque, double x, double y, double multip)
	{
		return 0;
	}
	
	/**
	 * determine le deplacement des lapins au fil du temps, donc l'IA des lapins.
	 */
	public void augmenterVecteur()
	{
		Vector newNorm = new Vector(this.getVitesse());
		Monstre ennemiLePlusProche=null;
		double distanceMin=Integer.MAX_VALUE;
		for(Monstre m :GameWorld.getMonstres().stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()))
		{
				//calcule la distance(sans la racine, c'est plus court, on calculera la racine apres
				double distance = (m.getX()-this.getX())*(m.getX()-this.getX()) + (m.getY()-this.getY())*(m.getY()-this.getY());
				if(distance<distanceMin)
				{
					distanceMin=distance;
					ennemiLePlusProche = m;
				}
		}
		distanceMin = Math.sqrt(distanceMin);
		if(distanceMin>rayonAction || ennemiLePlusProche==null)
		{
			if(droite)
				newNorm.setX(1);
			else
				newNorm.setX(-1);
		}
		else
		{
			newNorm.setX(ennemiLePlusProche.getX()-this.getX());
			newNorm.setY(ennemiLePlusProche.getY()-this.getY());
		}
		//System.out.println(newNorm.getX() + newNorm.getY());

		newNorm = newNorm.normalised();
		vecteurAvancer.setX(vecteurAvancer.getX() + this.getVitesse() * newNorm.getX());
		vecteurAvancer.setY(vecteurAvancer.getY() + this.getVitesse() * newNorm.getY());
	}


	
	public void attaquer(EtreVivant a)
	{
		if(!(a instanceof Joueur))
		{
			int degatsPris = a.prendreDegats(this.pointsDAttaque, position.getX(), position.getY(), this.getRecul());
			GameWorld.getJoueur().ajouterVieVolee(degatsPris);
			if(magique)
				Anim.addEntiteDisp(new EntiteTimer("explosion" + String.valueOf(this.getNom()), this.getX(), this.getY(),
					this.getWidth()*3, this.getHeight()*3,false, 350, Images.Entites.Explosion.val()));
			mourir();
		}
	}
	
	
	@Override
	public void dessine()
	{
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				//l'entite, le numero d'animation, un vecteur de remplissage, vers la droite ? et un boolean de remplissage(frappe)
				Anim.bonneImage(this.quelleEntite, this.numeroAnimation, new Vector(1),  droite, false),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();
	}
	
}
