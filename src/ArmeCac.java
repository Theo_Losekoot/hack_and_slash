import java.util.ArrayList;
import java.util.List;

public class ArmeCac extends Arme
{

	/**
	 * Constructeur de ArmeCac, une arme au corps a corps, ici que des epees
	 * @param nom
	 * @param x
	 * @param y
	 * @param degats
	 * @param width
	 * @param height
	 * @param quelleEntite pour l'affichage
	 * @param _typeArme les classes pouvant porter cette arme( magicien ? bourrin ? archer ? etc)
	 * @param val la valeur de l'arme
	 */
	public ArmeCac(String nom, double x, double y, int degats, double width, double height, int quelleEntite,
			ArrayList<Integer> _classesPouvantPorter, int val)
	{
		//arme : nom, positionX, positionY, largeur, hauteur, quelle arme?, quel type d'arme ?
		super(nom, x, y, degats, width, height, quelleEntite, _classesPouvantPorter, val);

	}
	
	/**
	 * genere un texte pour l'affichage dans l'inventaire
	 */
	@Override
	public void genererTexteDescriptif()
	{
		texteDescriptif[0] = "Categorie : ArmeCac";
		texteDescriptif[1] = "+" + this.getDegats() + " degats                               ";
		
		if(classesPouvantLaPorter!=null)
		{
			texteDescriptif[1] += "Classe possible :    " ;
			if (classesPouvantLaPorter.contains(Integer.valueOf(0)))
				texteDescriptif[1] += "     Mage           ";
			if (classesPouvantLaPorter.contains(1))
				texteDescriptif[1] += "     archer         ";
			if (classesPouvantLaPorter.contains(2))
				texteDescriptif[1] += "     Bourrin        ";
			if (classesPouvantLaPorter.contains(3))
				texteDescriptif[1] += "     Peon(tous)     ";
		}
	}
	
	public void setDegats(int deg)
	{
		bonus=deg;
	}
	public int getDegats()
	{
		return bonus;
	}
	
	/**
	 * fait une attaque rapide devant soi
	 */
	@Override
	public void attaque1(Joueur joueur)
	{
		//definit la duree de l'attaque
		joueur.dureeAttaque=GameWorld.TEMPS_RAFRAICHISSEMENT*17;
		double dureeAttaqueTotale=joueur.dureeAttaque/joueur.getVitesseAttaque();
		
		//prepare la rotation pour les armes, ainsi que leurs position.
		double rotation = 0;
		if(joueur.droite)
		{
			rotation =180+ 180  -90*((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale);
		}
		else
			rotation =90+ 90*((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale);
		double largeurEpee = 6*joueur.inventory.getArme().getHB().getWidth()/10;
		double hauteurEpee = joueur.inventory.getArme().getHB().getHeight()/6;
		//prepare une hitbox pour l'epee
		double xEpee = joueur.getX();
		if(joueur.droite)
		{ //deplace l'epee a droite, et la met plus loin quand elle est au centre que sur les bords
			xEpee+=(largeurEpee/2 -Math.abs( (0.5-((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale))* (largeurEpee/3) )); 
		}
		else
		{ //deplace l'epee a gauche, et la met plus loin quand elle est au centre que sur les bords
			xEpee-=(55*largeurEpee/100  -Math.abs( (0.5-((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale))* (largeurEpee/3) )); 
		}
		//notre position + 
		double yEpee = joueur.getY()+(2*joueur.getHeight()/10)*joueur.inventory.getArme().getHeight() - ((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale)*(4*joueur.getHeight()/10)*joueur.inventory.getArme().getHeight();
		
		//detecte la collision et attaque dans le cas ou il y a collosion
		ObstacleInvisible epee = new ObstacleInvisible("coup", xEpee, yEpee, largeurEpee, hauteurEpee);
		for(Entite e : GameWorld.getMonstres())
		{
			if(Hitbox.collision(e.getHB(), epee.getHB()))
			{
				joueur.attaquer((EtreVivant)e);
			}
		}
		//affiche l'arme
		Arme temp = joueur.inventory.getArme();
		if(joueur.droite)
			temp.setPosition(new Position(temp.getX()-joueur.getWidth()/4,temp.getY()));//-joueur..getHeight()/6));
		else
			temp.setPosition(new Position(temp.getX()+joueur.getWidth()/4,temp.getY()));//-joueur..getHeight()/6));
		GameWorld.addEntiteOneShot(temp, rotation);
		//affiche la hitbox, pratique pour le debug
		//GameWorld.addEntiteOneShot(epee, 0);
	}
	
	/**
	 * l'attaque 2 de l'arme, exactement comme la premiere sauf que la rotation est dans
	 * l'autre sens et que le coupest 2 fois plus lent et inflige plus de degats
	 */
	@Override
	public void attaque2(Joueur joueur)
	{
		joueur.dureeAttaque=GameWorld.TEMPS_RAFRAICHISSEMENT*35;
		double dureeAttaqueTotale=(joueur.dureeAttaque/joueur.getVitesseAttaque());
		double rotation = 0;
		if(joueur.droite)
		{
			rotation =90 - 180  +90*((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale);
		}
		else
			rotation =180- 90*((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale);
		double largeurEpee = 6*joueur.inventory.getArme().getHB().getWidth()/10;
		double hauteurEpee = joueur.inventory.getArme().getHB().getHeight()/6;
		double xEpee = joueur.getX();
		if(joueur.droite)
		{ //deplace l'epee a droite, et la met plus loin quand elle est au centre que sur les bords
			xEpee+=(55*largeurEpee/100 -Math.abs( (0.5-((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale))* (largeurEpee/3) )); 
		}
		else
		{ //deplace l'epee a gauche, et la met plus loin quand elle est au centre que sur les bords
			xEpee-=(largeurEpee/2  -Math.abs( (0.5-((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale))* (largeurEpee/3) )); 
		}
		//notre position + 
		double yEpee = joueur.getY()-(2*joueur.getHeight()/10)*joueur.inventory.getArme().getHeight() + ((double)joueur.tempsAttaqueActuelle/dureeAttaqueTotale)*(4*joueur.getHeight()/10)*joueur.inventory.getArme().getHeight();
		
		ObstacleInvisible epee = new ObstacleInvisible("coup", xEpee, yEpee, largeurEpee, hauteurEpee);
		//comme c'est l'attaque lente, je multiplie les degats par 1.5
		int ajoutDegats = (int)(joueur.getAttaqueTotale()*0.8);
		joueur.inventory.getArme().setDegats(joueur.inventory.getArme().getDegats()+ajoutDegats);
		joueur.updateDegatsArme();
		List<Monstre> lesMonstres = GameWorld.getMonstres();
		for(Entite e : lesMonstres)
		{
			if(Hitbox.collision(e.getHB(), epee.getHB()))
			{
				joueur.attaquer((EtreVivant)e);
			}
		}
		joueur.inventory.getArme().setDegats(joueur.inventory.getArme().getDegats()-ajoutDegats);
		joueur.updateDegatsArme();
		Arme temp = joueur.inventory.getArme();
		if(joueur.droite)
			temp.setPosition(new Position(temp.getX()-joueur.getWidth()/4,temp.getY()));//-joueur..getHeight()/6));
		else
			temp.setPosition(new Position(temp.getX()+joueur.getWidth()/4,temp.getY()));//-joueur..getHeight()/6));
		GameWorld.addEntiteOneShot(temp, rotation);
	}
	
	/**
	 * n'est pas magique
	 */
	@Override
	public boolean getArmeMagique()
	{
		return false;
	}
	
	/**
	 * coute 0 mana
	 */
	public int coutTir()
	{
			return 0;
	}
	
	

}
