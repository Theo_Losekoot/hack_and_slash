public class Poule extends Chasseur
{
	private static double usualWidth=0.8;
	private static double usualHeight=0.8;
	private static int usualLifePoints=10;
	private static int usualAttack=50;
	private static int usualDefense=3;
	private static double usualSpeed=0.015;
	private static double usualKnockback=0.5;

	/**
	 * Constructeur de poule, plutot explicite
	 * @param nom
	 * @param x
	 * @param y
	 * @param pDV
	 * @param pAtk
	 * @param pDef
	 * @param vitesse
	 * @param width
	 * @param height
	 * @param _recul
	 */
	public Poule(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width,
			double height, double _recul, boolean _estUnBoss)
	{
		super(nom, x, y, pDV, pAtk, pDef, vitesse, width, height, Images.Entites.Poule.val(), _recul, _estUnBoss);
	}
	
	/**
	 * determine le deplacement des poules, elles sont un peu stupides mais sinon ca va
	 */
	@Override
	public void augmenterVecteur()
	{
		Vector newNorm = new Vector(this.getVitesse());
		newNorm.setX(GameWorld.getJoueur().getX()-this.getX());
		newNorm.setY(GameWorld.getJoueur().getY()-this.getY());
		newNorm = newNorm.normalised();
		vecteurAvancer.setX(vecteurAvancer.getX()+newNorm.getX()*this.getVitesse());
		vecteurAvancer.setY(vecteurAvancer.getY()+newNorm.getY()*this.getVitesse());
		
	}
	
	@Override
	public void mourir()
	{
		GameWorld.getJoueur().addXp(1);
		super.mourir();
	}

	/**
	 * lance l'attaque (si il y en a une ? on sait pas encore)
	 */
	public void attaquer()
	{
		// vecteurAvancer.setX(0);
		// vecteurAvancer.setY(0);

		if (!frappeActuellement())
		{
			tempsAttaqueActuelle = 0;
		}
	}
	public void attaquer(EtreVivant a)
	{
		a.prendreDegats(this.pointsDAttaque, position.getX(), position.getY(), this.getRecul());
		Anim.addEntiteDisp(new EntiteTimer("explosion" + String.valueOf(this.getNom()), this.getX(), this.getY(),
				this.getWidth()*3, this.getHeight()*3,false, 350, Images.Entites.Explosion.val()));
		mourir();
	}

	
	/**
	 * bha du coup pour l'instant c'est vide
	 */
	@Override
	public void actualiserCoup()
	{

	}
	
	public void actualiserTour()
	{
		
	}
	
	
	public static double getUsualWidth()
	{
		return usualWidth;
	}
	public static double getUsualHeight()
	{
		return usualHeight;
	}

	public static int getUsualLifePoints()
	{
		return usualLifePoints;
	}

	public static int getUsualAttack()
	{
		return usualAttack;
	}

	public static int getUsualDefense()
	{
		return usualDefense;
	}

	public static double getUsualSpeed()
	{
		return usualSpeed;
	}
	public static double getUsualKnockback()
	{
		return usualKnockback;
	}
}
