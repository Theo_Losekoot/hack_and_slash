import java.util.List;

public class Bombe extends Consommable
{

	// Les dommages inflig�s par la bombe
	private int dommages;
	private int tempsAvantExplosion;
	private int tempsExplosion;
	//bombe posee ?
	private boolean onFire;
	protected double recul;
	
	/**
	 * Constructeur de bombe a partir de parametres
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param isObstacle
	 * @param nb de bombes 
	 * @param val : prix de l'objet
	 */
	public Bombe(String nom, double x, double y, double width, double height, boolean isObstacle, int nb, int val)
	{
		//Consommable : nom, positionX, positionY, largeur, hauteur
		//obstacle? si on se cogne dessus, nombre d'objets, quel objet
		super(nom, x, y, width, height, isObstacle, nb, Images.Entites.Bombe.val(), val);
		dommages=200;
		tempsAvantExplosion=1500;
		tempsExplosion=500;
		onFire=false;
		recul=1.5;
		genererTexteDescriptif();

	}
	
	/**
	 * Constructeur de bombe a partir d'une autre bombe
	 * @param p l'autre bombe
	 */
	public Bombe(Bombe p)
	{
		super(p.getNom(), p.getX(), p.getY(), p.getHB().getWidth(), p.getHB().getHeight(), p.isObstacle(), p.getNombre(), p.getNumEntite(), p.getValeurItem());
		dommages=p.dommages; 
		tempsAvantExplosion=1500;
		tempsExplosion=500;
		onFire=false;
		recul=1.5;
		genererTexteDescriptif();

	}

	//la meme que les autres classes
	public void genererTexteDescriptif()
	{
		texteDescriptif[0] = "Categorie : Bombes";
		texteDescriptif[1] = "Peut exploser et    causer " + this.dommages + " degats !";
	}
	
	/**
	 * si la bombe est bien unique, y met feu
	 */
	public void utiliser()
	{
		if(this.getNombre()<=1)
		{
			this.onFire=true;
		}
	}
	/**
	 * update une bombe en feu, et si son heure est venue, la fait exploser.
	 */
	public void updateBombe()
	{
		if(onFire)
		{
			tempsAvantExplosion-=GameWorld.TEMPS_RAFRAICHISSEMENT;
			if(tempsAvantExplosion<=0)
			{
				explose();
			}
		}
	}
	
	/**
	 * inflige les degats aux monstres et envoie l'animation de l'explosion a Anim, tout en supprimant la bombre
	 */
	public void explose()
	{
		//supprime la bombe
		GameWorld.removeEntite(this);
		//cree animation pour l'explosion
		Anim.addEntiteDisp(new EntiteTimer("explosion" + String.valueOf(this.getNom()), this.getX(), this.getY(),
				this.getWidth()*4, this.getHeight()*4,false, this.tempsExplosion, Images.Entites.Explosion.val()));
		//grossis la hitbox, pour toucher des gens
		this.getHB().setHeight(this.getHB().getHeight()*4);
		this.getHB().setWidth(this.getHB().getWidth()*4);
		//inflige les degats
		List<Monstre> lesMonstres = GameWorld.getMonstres();
		for(int i=0; i<lesMonstres.size(); i++)
		{
			Monstre leMonstre = lesMonstres.get(i);
			if(Hitbox.collision(this.getHB(), leMonstre.getHB()))
				leMonstre.prendreDegats(this.dommages, this.getX(), this.getY(), recul);
		}

	}
	
	//la bombe est allumee ?
	public boolean getOnFire()
	{
		return onFire;
	}
	
	public String toString()
	{
		return (super.toString() + " on fire : " + this.getOnFire() + "  temps avant explosion : " + this.tempsAvantExplosion); 
	}
	
}
