import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public abstract class EtreVivant extends EntiteAnimee
{
	/**
	 * Constructeur d'EtreVivant
	 * @param nom
	 * @param x
	 * @param y
	 * @param pDV
	 * @param pAtk
	 * @param pDef
	 * @param vitesse
	 * @param width
	 * @param height
	 * @param entite : quelle entite c'est, pour afficher la bonne image
	 * @param _recul le recul qu'il inflige
	 */
	public EtreVivant(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width,
			double height, int entite, double _recul)
	{
		// EntiteAnimee : nom, positionX, positionY, largeur, hauteur, obstacle?
		// etreVivant?, quelle Entite?, rotationContinue
		super(nom, x, y, width, height, false, true, entite, 0);
		pointsDeVie = pDV;
		pointsDAttaque = pAtk;
		pointsDeDefense = pDef;
		vitesseP = vitesse;
		vecteurAvancer = new Vector(vitesseP);
		immunite = 0;
		//on suppose que le personnage est cree avec toute sa vie
		pointsDeVieMax = pDV;
		droite = true;
		dureeAttaque = 350;
		//pour na pas ataquer des que le jeu se lance
		tempsAttaqueActuelle = 2 * dureeAttaque;
		recul=_recul;
	}

	// les points de vie du personnage
	protected int pointsDeVie;
	protected int pointsDeVieMax;

	// Le nombre de d�gats inflig�s aux monstres
	protected int pointsDAttaque;

	// La r�duction des d�gats subis par les monstres
	protected int pointsDeDefense;

	// vecteur de mouvement
	protected Vector vecteurAvancer;

	// vitesse du personnage
	protected double vitesseP;
	
	// le joueur a besoin d'une orientation, d'un vecteur representant ses
	// deplacements, d'une vitesse,
	protected int orientation = 0;

	// Temps pendant lequel le joueur ne pourra plus etre frappe
	protected long immunite = 0;

	// Sert a l'animation
	protected boolean droite;

	// temps d'attaque
	protected int dureeAttaque;

	// depuis combien de tempsl l'attaque est lancee
	protected int tempsAttaqueActuelle;

	// quelle attaque est lancee
	protected int quelleAttaque;
	
	//pour gerer le recul
	protected double recul;


	public abstract void mourir();

	public int getAttaque()
	{
		return pointsDAttaque;
	}

	public int getDefenseTotale()
	{
		return pointsDeDefense;
	}

	public double getVitesse()
	{
		return vitesseP;
	}

	public boolean isAlive()
	{
		return pointsDeVie > 0;
	}
	/**
	 * attaque un etreVivant, a redefinir car elle depend de l'arme etc
	 * @param e
	 */
	public abstract void attaquer(EtreVivant e);

	// L'attaque d'agresseur est de atk
	/**
	 * Prendre des degats, cree une immunite, et fais subir du recul
	 * @param atk
	 * @param x
	 * @param y
	 */
	public int prendreDegats(int atk, double x, double y, double multiplicateurRecul)
	{
		int vie = this.pointsDeVie;
		//si n'est pas frappable actuellement 
		if (!frappable())
			return 0;
		atk-=this.getDefenseTotale();
		//si le monstre a moins d'attaque qu'on a de defense, il fait quand meme 1 de degats 1 fois sur 5.(et inversement)
		if(atk<0 && ThreadLocalRandom.current().nextInt(0,5)==0)
			atk=1;
		else if(atk<0)
			atk=0;
		//System.out.println(this + " prend " + atk + "degats");
		pointsDeVie -= atk;
		if(atk>0)
		{
			//affiche le texte de degats, si il y a degats
			afficherTexte(String.valueOf(atk), this.getX(), this.getY(), 0, 500);
			immunite = 200;
		}
		//sinon diminue le recul
		else
			multiplicateurRecul/=2;
		//le subit
		subirRecul(x, y, multiplicateurRecul);
		if(pointsDeVie<0)
			pointsDeVie=0;
		return vie-pointsDeVie;
	}

	/**
	 *  permet d'afficher le nombre de degats infliges ou recus en rouge, ou le soin en vert
	 * @param degats
	 * @param posX
	 * @param posY
	 * @param couleur : la couleur d'affichage
	 */
	public void afficherTexte(String texte, double posX, double posY, int couleur, int duree)
	{
			Anim.addTextDisp(new TexteTemporaire("text"+String.valueOf(posX), posX, posY,
					this.getWidth(), this.getHeight(), false, duree, texte, couleur));		
	}
	
	/**
	 * met a jour l'immunite, donc diminue d'un temps de rafraichissement, car appele a chaque rafraichissement
	 */
	protected void updateImmunite()
	{
		immunite -= GameWorld.TEMPS_RAFRAICHISSEMENT;
	}

	/**
	 * cree un effet de recul, dans la bonne direction
	 * 
	 * @param x
	 *            : l'emplacement en X de l'adversaire
	 * @param y
	 *            : l'emplacement en Y de l'adversaire
	 */
	public void subirRecul(double x, double y, double multiplicateur)
	{
		Vector recul = new Vector(this.vitesseP);
		recul.setX(this.getX() - x);
		recul.setY(this.getY() - y);
		recul = recul.normalised();
		//fais reculer le monstre/joueur dans la position opposee a celle ou il a ete frappe
		recul.setX((recul.getX()/2)*(multiplicateur));
		recul.setY((recul.getY()/2)*(multiplicateur));
		//applique le recul
		vecteurAvancer.setX(vecteurAvancer.getX() + recul.getX());
		vecteurAvancer.setY(vecteurAvancer.getY() + recul.getY());
	}

	/**
	 * verifie si l'etreVivant ne se cogne pas avec un obstacle
	 * @return true si il y a collision, false sinon
	 */
	public boolean collisionObstacle()
	{
		boolean collision = false;
		for (Entite a : GameWorld.getEntites().values().stream().filter(x -> !(x instanceof Monstre)).collect(Collectors.toList()))
		{
			if (a.isObstacle() && a.getNom() != this.getNom())
			{
				if (Hitbox.collision(a.getHB(), this.getHB()))
				{
					collision = true;
				}
			}
		}
		return collision;
	}

	// methode permettant l'application du vecteur a notre personnage et la
	// modification
	// de son orientation, ainsi que la mise a jour du vecteur.
	public void seDeplacer()
	{
		avancer();
		vecteurAvancer.ralentir();
	}

	/**
	 * la fonction principale, qui va appeler toutes les autres, a redefinir suivant le type d'etre vivant.
	 */
	public abstract void jouer();

	/**
	 * avance en utilisant le vecteurAvancer, en faisant attention aux collisions.
	 */
	protected void avancer()
	{
		////for(int i=1; i<4; i++)
		{
			double modifX = vecteurAvancer.getX();
			double modifY = vecteurAvancer.getY();
			position.setX((position.getX() + modifX));
			position.setY((position.getY() + modifY));
			updateHB();
			if (!collisionObstacle())
				return;
			// si collision, essaie de dectecter quelle coordonnee fait
			// une collision pour l'enlever, sinon annule le move
			else
			{
				// enleve le vecteur x
				position.setX((position.getX() - modifX));
				updateHB();
				// si encore collision, remet le vecteur x et enleve le vecteur y
				if (collisionObstacle())
				{
					position.setX((position.getX() + modifX));
					position.setY((position.getY() - modifY));
					updateHB();
				}
				// si encore collision, re-enleve le vecteur x
				if (collisionObstacle())
				{
					position.setX((position.getX() - modifX));
					updateHB();
				}
			}
		}
	}

	/**
	 * augmente la variable d'animation, pratique pour les animations
	 */
	@Override
	protected void updateVarAnim()
	{
		numeroAnimation[0]++;
		if(numeroAnimation[0]>1000000)
			numeroAnimation[0]=0;
	}


	/**
	 * l'etre vivant est-il en train de frapper ?
	 * @return
	 */
	public boolean frappeActuellement()
	{
		return tempsAttaqueActuelle <= (double)dureeAttaque/this.getVitesseAttaque();
	}
	/**
	 * stoppe l'etre vivant, pratique dans l'utilisation de pieges ou de fonction de frappe.
	 */
	public void stopper()
	{
		this.vecteurAvancer.setX(0);
		this.vecteurAvancer.setY(0);
	}
	
	public double getRecul()
	{
		return recul;
	}
	public int getPVMax()
	{
		return pointsDeVieMax;
	}
	public double getVitesseAttaque()
	{
		return 1;
	}
	
	/**
	 * peut etre frappe
	 * @return true si il peut, false sinon
	 */
	public boolean frappable()
	{
		return immunite<0;
	}
}
