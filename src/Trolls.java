import java.util.concurrent.ThreadLocalRandom;

public class Trolls extends Chasseur
{
	private static double usualWidth=2.5;
	private static double usualHeight=2.5;
	private static int usualLifePoints=75;
	private static int usualAttack=30;
	private static int usualDefense=20;
	private static double usualSpeed=0.01;
	private static double usualKnockback=1.0;
	
	private int dureeEntreDeplacement;
	private int tempsAvantProchainDeplacement;
	private boolean joueurRepere;
	private Position positionVisee;
	
	
	public Trolls(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width, double height, double _recul, boolean boss)
	{
		super(nom,x,y,pDV,pAtk,pDef,vitesse,width,height, Images.Entites.Trolls.val(), _recul, boss);
		joueurRepere=false;
		dureeEntreDeplacement=3000;
		tempsAvantProchainDeplacement=0;
	}
	
	
	public void attaquer(EtreVivant a)
	{
		a.prendreDegats(this.pointsDAttaque, position.getX(), position.getY(), this.getRecul());
		joueurRepere=false;
	}
	
	@Override
	public int prendreDegats(int atk, double x, double y, double multiplicateurRecul)
	{
		joueurRepere=true;
		return super.prendreDegats(atk, x, y, multiplicateurRecul);
	}


	/**
	 * lance l'attaque (si il y en a une ? on sait pas encore)
	 */
	public void attaquer()
	{
		// vecteurAvancer.setX(0);
		// vecteurAvancer.setY(0);
		if (!frappeActuellement())
		{
			tempsAttaqueActuelle = 0;
		}
	}

	/**
	 * bha du coup pour l'instant c'est vide
	 */
	@Override
	public void actualiserCoup()
	{

	}
	
	/**
	 * fais des deplacements plus ou moins aleatoires lorsque le joueur est loin,et si le joueur est trop pres du troll,
	 * a une chance de s'enerver et de foncer sur le joueur jusqu'a l'avoir touche
	 */
	@Override
	public void augmenterVecteur()
	{
		double distanceJoueur = GameWorld.getDistanceJoueur(this.getX(), this.getY());
		//environ une chance sur 200 de reperer le joueur, est appele 30 fois par secondes, donc repere le joueur en 7q secondes environ
		//(dans les faits c'est un peu plus rapide)
		if(distanceJoueur<7*GameWorld.getWidthGrille()/12)
		{
			if(ThreadLocalRandom.current().nextInt(0,1000)%200==0)
				joueurRepere=true;
		}
		
		Vector newNorm = new Vector(this.getVitesse());

		if(joueurRepere)
		{
			newNorm.setX(GameWorld.getJoueur().getX()-this.getX());
			newNorm.setY(GameWorld.getJoueur().getY()-this.getY());
			newNorm = newNorm.normalised();
			vecteurAvancer.setX(vecteurAvancer.getX()+newNorm.getX()*this.getVitesse()*4);
			vecteurAvancer.setY(vecteurAvancer.getY()+newNorm.getY()*this.getVitesse()*4);
			return;
		}
		else if(tempsAvantProchainDeplacement<=0)
		{
			tempsAvantProchainDeplacement=dureeEntreDeplacement;
			positionVisee= new Position(
					this.getX()+ ThreadLocalRandom.current().nextInt(-GameWorld.getWidthGrille()/10,GameWorld.getWidthGrille()/10),
					this.getY() + (ThreadLocalRandom.current().nextInt(-GameWorld.getHeightGrille()/10, GameWorld.getHeightGrille()/10)));
			
		}
		else 
			tempsAvantProchainDeplacement-=GameWorld.TEMPS_RAFRAICHISSEMENT;
		
		if(tempsAvantProchainDeplacement>dureeEntreDeplacement/2)
		{
			newNorm.setX(positionVisee.getX()-this.getX());
			newNorm.setY(positionVisee.getY()-this.getY());
			newNorm = newNorm.normalised();
			vecteurAvancer.setX(vecteurAvancer.getX()+newNorm.getX()*this.getVitesse());
			vecteurAvancer.setY(vecteurAvancer.getY()+newNorm.getY()*this.getVitesse());
		}

	}
	
	
	@Override

	public void dessine()
	{
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(this.quelleEntite, this.numeroAnimation, this.vecteurAvancer, this.droite, this.frappeActuellement()),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();
		//dessine une barre de vie
		StdDraw.setPenColor(StdDraw.RED);
		if(estUnBoss)
		StdDraw.filledRectangle((double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()-this.getHeight()/2) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				(double)(this.getPointsDeVie()/Trolls.getUsualLifePoints()/(double)5) /GameWorld.getWidthGrille(), (double)(this.getHeight()/30) /GameWorld.getHeightGrille());
	
	}
	
	
	@Override
	public void mourir()
	{
		GameWorld.getJoueur().addXp(5);
		super.mourir();
	}
	
	public static double getUsualWidth()
	{
		return usualWidth;
	}
	public static double getUsualHeight()
	{
		return usualHeight;
	}

	public static int getUsualLifePoints()
	{
		return usualLifePoints;
	}

	public static int getUsualAttack()
	{
		return usualAttack;
	}

	public static int getUsualDefense()
	{
		return usualDefense;
	}

	public static double getUsualSpeed()
	{
		return usualSpeed;
	}
	public static double getUsualKnockback()
	{
		return usualKnockback;
	}
	

	
}
