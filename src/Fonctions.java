/*
 * Some basic functions 
 */
public class Fonctions
{
	/**
	 * fait une pause du temps desire, moins le temps du debut de tour, permet un rafraichissement fluide.
	 * @param tempsDepart : temps de debut de tour
	 * @param duree le temps de pause (avant soustraction)
	 */
	public static void pause(long tempsDepart, int duree)
	{	
		int tempsPause = duree - (int)(System.nanoTime()-tempsDepart)/1000000;
		//System.out.println("temps de pause : " + tempsPause);
		if(tempsPause<0)
			tempsPause=0;
		if(tempsPause>20)
			tempsPause=10;
		StdDraw.pause(tempsPause);
	}
	
	/**
	 * fait une simple puase
	 * @param duree
	 */
	public static void pause(int duree)
	{
		if(duree<0)
			duree=0;
		StdDraw.pause(duree);
	}
	
}
