import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Lapin extends Chasseur
{
	private static double usualWidth=1.5;
	private static double usualHeight=1.5;
	private static int usualLifePoints=12;
	private static int usualAttack=10;
	private static int usualDefense=5;
	private static double usualSpeed=0.02;
	private static double usualKnockback=0.3;
	/**
	 * Constructeur de Lapin, plutot clair
	 * 
	 * @param nom
	 * @param x
	 * @param y
	 * @param pDV
	 * @param pAtk
	 * @param pDef
	 * @param vitesse
	 * @param width
	 * @param height
	 */
	public Lapin(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width,
			double height, double _recul, boolean estUnBoss)
	{
		super(nom, x, y, pDV, pAtk, pDef, vitesse, width, height, Images.Entites.Lapin.val(), _recul, estUnBoss);
	}
	
	/**
	 * determine le deplacement des lapins au fil du temps, donc l'IA des lapins.
	 */
	public void augmenterVecteur()
	{
		// System.out.println("heeey : " + this.getNom());
		Vector nouveauVect = new Vector(this.getVitesse());
		
		int distanceMaxX = GameWorld.getWidthGrille();
		int distanceMaxY = GameWorld.getHeightGrille();
		int distanceMinX = GameWorld.getWidthGrille() / 3;
		int distanceMinY = GameWorld.getHeightGrille() / 3;
		List<Lapin> autresLapins = new ArrayList<Lapin>();
		autresLapins = GameWorld.getMonstres().stream().filter(Lapin.class::isInstance).map(Lapin.class::cast)
				.collect(Collectors.toList());

		for (int i = 0; i < autresLapins.size(); i++)
		{
			double xEntreLapins = this.getX() - autresLapins.get(i).getX();
			double yEntreLapins = this.getY() - autresLapins.get(i).getY();

			// si ce lapin est a droite d'un autre et trop proche, le faire s'ecarter a droite
			if (xEntreLapins < distanceMinX && xEntreLapins > 0)
			{
				nouveauVect.setX(nouveauVect.getX() + (distanceMinX - xEntreLapins) * ((double)distanceMaxX/distanceMinX));
			}
			// si ce lapin est a gauche d'un autre et trop proche, le faire s'ecarter a gauche
			else if (-xEntreLapins < distanceMinX && xEntreLapins < 0)
			{
				nouveauVect.setX(nouveauVect.getX() - (distanceMinX + xEntreLapins) * ((double)distanceMaxX/distanceMinX));
			}
			// si ce lapin est a droite d'un autre et a distance correcte, le faire se rapprocher a gauche
			else if (xEntreLapins < distanceMaxX && xEntreLapins > 0)
			{
				nouveauVect.setX(nouveauVect.getX() - (distanceMaxX - xEntreLapins));
			}
			// si ce lapin est a gauche d'un autre et a distance correcte, le faire se rapprocher a droite
			else if (-xEntreLapins < distanceMaxX && xEntreLapins < 0)
			{
				nouveauVect.setX(nouveauVect.getX() + (distanceMaxX + xEntreLapins));
			}


			// si ce lapin est au dessus d'un autre et trop proche, le faire s'ecarter vers le haut
			if (yEntreLapins < distanceMinY && yEntreLapins > 0)
			{
				nouveauVect.setY(nouveauVect.getY() + (distanceMinY) * ((double)distanceMaxY/distanceMinY));
			}
			// si ce lapin est en dessous d'un autre et trop proche, le faire s'ecarter vers le bas
			else if (-yEntreLapins < distanceMinY && yEntreLapins < 0)
			{
				nouveauVect.setY(nouveauVect.getY() - distanceMinY * ((double)distanceMaxY/distanceMinY));
			}
			// si ce lapin est au dessus d'un autre et a distance correcte, le faire se rapprocher vers le bas
			else if (yEntreLapins < distanceMaxY && yEntreLapins > 0)
			{
				nouveauVect.setY(nouveauVect.getY() - (distanceMaxY - yEntreLapins));
			}
			// si ce lapin est en dessous d'un autre et a distance correcte, le faire se rapprocher vers le haut
			else if (-yEntreLapins < distanceMaxY && yEntreLapins < 0)
			{
				nouveauVect.setY(nouveauVect.getY() + (distanceMaxY + yEntreLapins));
			}
		}
		//normaliser ce vecteur, pour revenir a des valeurs normales
		Vector newNorm = nouveauVect.normalised();
		
		//Compte pour 1/4 du deplacement des lapins
		vecteurAvancer.setX(vecteurAvancer.getX() + this.getVitesse() * newNorm.getX()/4);
		vecteurAvancer.setY(vecteurAvancer.getY() + this.getVitesse() * newNorm.getY()/4);
		
		//si les lapins voient le joueur, foncer sur lui(enfin s'en rapprocher)
		if ((Math.abs(this.getX() - GameWorld.getJoueur().getX()) < GameWorld.getWidthGrille() / 3) &&
				(Math.abs(this.getY() - GameWorld.getJoueur().getY()) < GameWorld.getHeightGrille() / 2 ))
		{
			newNorm.setX(GameWorld.getJoueur().getX() - this.getX());
			newNorm.setY(GameWorld.getJoueur().getY() - this.getY());

		}

		//normaliser le nouveau vecteur
		newNorm = newNorm.normalised();
		//ajouter tout ca au vecteur avancer, la partie joueur compte pour 3/4 du deplacement des lapins (s'ils le voient
		//, sinon c'est 100% du deplaceent pour la premiere partie de l'algorithme)
		vecteurAvancer.setX(vecteurAvancer.getX() + this.getVitesse() * 7*newNorm.getX()/10);
		vecteurAvancer.setY(vecteurAvancer.getY() + this.getVitesse() * 7*newNorm.getY()/10);
	}

	/**
	 * lance l'attaque (si il y en a une ? on sait pas encore)
	 */
	public void attaquer()
	{
		// vecteurAvancer.setX(0);
		// vecteurAvancer.setY(0);
		if (!frappeActuellement())
		{
			tempsAttaqueActuelle = 0;
		}
	}

	@Override
	public void mourir()
	{
		GameWorld.getJoueur().addXp(2);
		super.mourir();
	}
	
	

	public void dessine()
	{
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(this.quelleEntite, this.numeroAnimation, this.vecteurAvancer, this.droite, this.frappeActuellement()),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();
		//dessine une barre de vie
		StdDraw.setPenColor(StdDraw.RED);
		if(estUnBoss)
		StdDraw.filledRectangle((double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()-this.getHeight()/2) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				(double)(this.getPointsDeVie()/Lapin.getUsualLifePoints()/(double)10) /GameWorld.getWidthGrille(), (double)(this.getHeight()/30) /GameWorld.getHeightGrille());
	
	}
	
	
	/**
	 * bha du coup pour l'instant c'est vide
	 */
	@Override
	public void actualiserCoup()
	{

	}
	
	public static double getUsualWidth()
	{
		return usualWidth;
	}
	public static double getUsualHeight()
	{
		return usualHeight;
	}

	public static int getUsualLifePoints()
	{
		return usualLifePoints;
	}

	public static int getUsualAttack()
	{
		return usualAttack;
	}

	public static int getUsualDefense()
	{
		return usualDefense;
	}

	public static double getUsualSpeed()
	{
		return usualSpeed;
	}
	public static double getUsualKnockback()
	{
		return usualKnockback;
	}
}
