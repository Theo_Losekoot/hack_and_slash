// Classe vector pour gerer un vecteur de vitesse des etres vivants.
	public class Vector
	{
		private double vitesseX,vitesseY;
		private double critereArret;
		Vector(double vitessePersonnage)
		{
			vitesseX=0;
			vitesseY=0;
			critereArret=vitessePersonnage/2;
		}
		public double getY() { return vitesseY; }
		public double getX() { return vitesseX; }
		public void setX(double x) { vitesseX=x; }
		public void setY(double y) { vitesseY=y; }
		public double getCritereArret() { return critereArret; }
		/**
		 * 
		 * @return Vector : le vecteur normalise sur lequel on a applique cette methode.
		 */
		public Vector normalised()
		{
			Vector retour = new Vector(this.getCritereArret()*2);
			double norme = Math.sqrt(vitesseX*vitesseX + vitesseY*vitesseY);
			if(norme!=0)
			{
				retour.setX(vitesseX/norme);
				retour.setY(vitesseY/norme);
			}
			return retour;
		}
		
		//diminue le vecteur, imite les frottements de l'air et du sol.
		public void ralentir()
		{
			vitesseX*=0.9;
			vitesseY*=0.9;
			if(vitesseX < critereArret && vitesseX > -critereArret)
				vitesseX=0;
			if(vitesseY < critereArret && vitesseY > -critereArret)
				vitesseY=0;
		}
	}
	
