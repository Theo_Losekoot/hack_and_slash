import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Maps
{
	private static HashMap<String ,ArrayList<Entite>> entitesMaps = new HashMap<String, ArrayList<Entite>>();

	public enum Map
	{
		Base(0), Foret(1), Marais(2), Grange(3),  Montagnes(4), Chateau(5);
		
	  private int valeur;
	  private Map(int val)
	  {
	    this.valeur = val;
	  }
	  public int val()
	  {
		  return valeur;
	  }
	}
		
	//definit la largeur de la map(en nombre d'images)
	public static int getLargeurMap(int quelleMap)
	{
		if(quelleMap==Map.Base.val())
			return 2;
		else if (quelleMap==Map.Foret.val())
			return 6;
		else if (quelleMap==Map.Marais.val())
			return 6;
		else if (quelleMap==Map.Grange.val())
			return 6;
		else if (quelleMap==Map.Montagnes.val())
			return 6;
		else if (quelleMap==Map.Chateau.val())
			return 6;
		else
			return 1;
	}
	
	public static int getHauteurMap(int quelleMap)
	{
		if(quelleMap==Map.Base.val())
			return 2;
		else if (quelleMap==Map.Foret.val())
			return 1;
		else if (quelleMap==Map.Marais.val())
			return 1;
		else if (quelleMap==Map.Grange.val())
			return 1;
		else if (quelleMap==Map.Montagnes.val())
			return 1;
		else if (quelleMap==Map.Chateau.val())
			return 1;
		else
			return 1;
	}
	
	/**
	 * calcule le decalage des elements devant etre affiches, pour qu'ils soient toujours bien en
	 * place par rapport a la fenetre et au monde
	 * (les 2/3 et 3/2 dans les 3 fonctions suivantes sont pour l'adaptation des
	 * images (600px) a l'ecran (900px)
	 * @param xPerso : la position du perso (entre 0 et la taille de la map)
	 * @param quelleMap : la map choisie, pour connaitre la taille
	 * @return : le decalage
	 */
	public static double decalageX(double xPerso, int quelleMap)
	{
		//rapporte les coordonnees du perso a la taille de l'image
		xPerso=xPerso/GameWorld.getWidthGrille();
	
		int largeurMap=getLargeurMap(quelleMap);
		
		//calcule le decalage des objets
		if(quelleMap!=Map.Base.val())
		{
			if(xPerso<0.5)
			{
				return 0;
			}
			else if(3*xPerso/2>largeurMap+(double)1/4)
			{
				return 2*largeurMap/3 -(double)1/3;
			}
			else
			{
				return (double)xPerso-(double)1/2;
			}
		}
		else
		{
			if(xPerso<0.5)
			{
				return 0;
			}
			else if(xPerso>largeurMap-(double)1/2)
			{
				return largeurMap -1;
			}
			else
			{
				return (double)xPerso-(double)1/2;
			}
		}
	}
	
	public static double decalageY(double yPerso, int quelleMap)
	{
		//rapporte les coordonnees du perso a la taille de l'image
		yPerso=yPerso/GameWorld.getHeightGrille();
	
		int hauteurMap=getHauteurMap(quelleMap);
		
		if(quelleMap!=Map.Base.val())
			return 0;
		//calcule le decalage des objets
		if(yPerso<0.5)
		{
			return 0;
		}
		else if(yPerso>hauteurMap-0.5)
		{
			return hauteurMap -1;
		}
		else
		{
			return (double)yPerso-0.5;
		}
	}
	
	/**
	 * Suivant la coordonnee et la map, choisit quelles images ont besoin d'etre affichees
	 * @param xPerso : coordonnees du perso
	 * @param quelleMap : la map ou l'on est
	 * @return les 3 images a afficher
	 */
	public static Image[] imagesMap(double xPerso, int quelleMap)
	{
		double decalageX = decalageX(xPerso, quelleMap);
		//cherche la largeur de la map (en nombre d'images)
		double largeurMap = getLargeurMap(quelleMap);
		Image[] retourAutresMap= new Image[3];
		for(Image i : retourAutresMap)
			i = null;
		
		//on fonction de la map, va chercher les trois images a afficher
		if(quelleMap==Map.Base.val())
		{
			return Images.getBase();
		}
		else if(quelleMap==Map.Foret.val())
		{
			retourAutresMap[0]=Images.getForet()[(int)(3*decalageX/2)];
			//condition pour ne pas sortir du tableau
			if(((int)3*decalageX/2)+(double)1/2 <= largeurMap)
				retourAutresMap[1]=Images.getForet()[(int)(3*decalageX/2)+1];
			if(((int)3*decalageX/2)+1 <= largeurMap)
				retourAutresMap[2]=Images.getForet()[(int)(3*decalageX/2)+2];
			return retourAutresMap;
		}
		else if(quelleMap==Map.Chateau.val())
		{
			retourAutresMap[0]=Images.getChateau()[(int)(3*decalageX/2)];
			//condition pour ne pas sortir du tableau
			if(((int)3*decalageX/2)+(double)1/2 <= largeurMap)
				retourAutresMap[1]=Images.getChateau()[(int)(3*decalageX/2)+1];
			if(((int)3*decalageX/2)+1 <= largeurMap)
				retourAutresMap[2]=Images.getChateau()[(int)(3*decalageX/2)+2];
			return retourAutresMap;
		}
		else if(quelleMap==Map.Marais.val())
		{
			retourAutresMap[0]=Images.getMarais()[(int)(3*decalageX/2)];
			//condition pour ne pas sortir du tableau
			if(((int)3*decalageX/2)+(double)1/2 <= largeurMap)
				retourAutresMap[1]=Images.getMarais()[(int)(3*decalageX/2)+1];
			if(((int)3*decalageX/2)+1 <= largeurMap)
				retourAutresMap[2]=Images.getMarais()[(int)(3*decalageX/2)+2];
			return retourAutresMap;
		}
		else if(quelleMap==Map.Grange.val())
		{
			retourAutresMap[0]=Images.getGrange()[(int)(3*decalageX/2)];
			//condition pour ne pas sortir du tableau
			if(((int)3*decalageX/2)+(double)1/2 <= largeurMap)
				retourAutresMap[1]=Images.getGrange()[(int)(3*decalageX/2)+1];
			if(((int)3*decalageX/2)+1 <= largeurMap)
				retourAutresMap[2]=Images.getGrange()[(int)(3*decalageX/2)+2];
			return retourAutresMap;
		}
		else if(quelleMap==Map.Foret.val())
		{
			retourAutresMap[0]=Images.getForet()[(int)(3*decalageX/2)];
			//condition pour ne pas sortir du tableau
			if(((int)3*decalageX/2)+(double)1/2 <= largeurMap)
				retourAutresMap[1]=Images.getForet()[(int)(3*decalageX/2)+1];
			if(((int)3*decalageX/2)+1 <= largeurMap)
				retourAutresMap[2]=Images.getForet()[(int)(3*decalageX/2)+2];
			return retourAutresMap;
		}
		return retourAutresMap;

	}
	
	/**
	 * dessine la map, grace aux trois images et au decalage calcules precedemment
	 * @param xPerso
	 * @param quelleMap
	 */
	public static void dessinerMap(double xPerso, double yPerso, int quelleMap)
	{
		Image[] map = imagesMap(xPerso, quelleMap);
		double decalageX = decalageX(xPerso, quelleMap);
		double decalageY = decalageY(yPerso, quelleMap);

		
		xPerso/=(double)GameWorld.getWidthGrille();
		yPerso/=(double)GameWorld.getHeightGrille();

		
		//Calcule les 4 centres des images, puis les affiche.
		if(quelleMap==Map.Base.val())
		{
			//Image 0, en bas a gauche
			double centreXIm1 = (double)1/2 -decalageX;
			double centreYIm1 = (double)1/2 -decalageY;

			//image 1, en bas a droite
			double centreXIm2 = (double)3/2 -decalageX;
			double centreYIm2 = (double)1/2 -decalageY;

			//image 2, en haut a gauche
			double centreXIm3 = (double)1/2 -decalageX;
			double centreYIm3 = (double)3/2 -decalageY;

			
			//image 3, en haut a droite
			double centreXIm4 = (double)3/2 -decalageX;
			double centreYIm4 = (double)3/2 -decalageY;
		/*	
		 * juste du debug
			System.out.println("decalageX : " + decalageX + "  decalage Y : " + decalageY);
		System.out.println("Image 1 : " + centreXIm1 + ", " +centreYIm1 +
				"   Image 2 : " + centreXIm2 + ", " +centreYIm2 +
				"   Image 3 : " + centreXIm3 + ", " +centreYIm3 +
				"   Image 4 : " + centreXIm4 + ", " +centreYIm4);
				*/
			//dessine les 4 images constituant la zone de jeu
			StdDraw.picture(centreXIm1,
					centreYIm1,
					map[0],
					1,
					1,
					0); 
			StdDraw.picture(centreXIm2,
					centreYIm2,
					map[1],
					1,
					1,
					0); 
			StdDraw.picture(centreXIm3,
					centreYIm3,
					map[2],
					1,
					1,
					0); 
			StdDraw.picture(centreXIm4,
					centreYIm4,
					map[3],
					1,
					1,
					0); 
		}
		else
		{
			//le centre des images sont positionnes de telle maniere :
			// la moitie de l'image + le decalage en fonction de quelle image ils representent
			// - le decalage en (Double) pour les voir defiler
			double centreIm1 = (double)1/3 + (double)2*((int)(3*decalageX/2))/3 -decalageX;
			double centreIm2 = (double)3/3 + (double)2*((int)(3*decalageX/2))/3 -decalageX;
			double centreIm3 = (double)5/3 + (double)2*((int)(3*decalageX/2))/3 -decalageX;

			StdDraw.picture(centreIm1,
					0.5,
					map[0],
					(double)2/3,
					1,
					0); 
			StdDraw.picture(centreIm2,
					0.5,
					map[1],
					(double)2/3,
					1,
					0); 
			StdDraw.picture(centreIm3,
					0.5,
					map[2],
					(double)2/3,
					1,
					0); 
		}
	}

	public static void initMaps()
	{
		entitesMaps= new HashMap<String ,ArrayList<Entite>>();
		entitesMaps.put("Base", new ArrayList<Entite>());
		entitesMaps.put("Foret", new ArrayList<Entite>());
		entitesMaps.put("Marais", new ArrayList<Entite>());
		entitesMaps.put("Montagnes", new ArrayList<Entite>());
		entitesMaps.put("Chateau", new ArrayList<Entite>());
		entitesMaps.put("Grange", new ArrayList<Entite>());
	}
	
	public static void construitMap(int quelleMap)
	{		
		initMaps();
		if(quelleMap==Maps.Map.Foret.val())
		{
			initForet();
		}
		else if(quelleMap==Maps.Map.Base.val())
		{
			initBase();
		}
		else if(quelleMap==Maps.Map.Chateau.val())
		{
			initChateau();
		}
		else if(quelleMap==Maps.Map.Marais.val())
		{
			initMarais();
		}
		else if(quelleMap==Maps.Map.Grange.val())
		{
			initGrange();
		}
		
	}
	
	public static void initMarais()
	{
		entitesMaps.put("Marais",new ArrayList<Entite>());
		//definit la largeur, hauteur du monde, ainsi que la largeur et hauteur a laquelle
		//le joueur n'a pas le droit d'aller
		double largeurMonde = GameWorld.getWidthGrille() * (2*((double)(getLargeurMap(Map.Marais.val())+1)/3));
		double hauteurMonde = GameWorld.getHeightGrille();
		double impossibleDroit=0;
		double impossibleHaut=(double)3*hauteurMonde/10;
		
		ObstacleInvisible obstaclesInvisibles[] = new ObstacleInvisible[4];
		
		obstaclesInvisibles[0] = new ObstacleInvisible("murGauche",-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[1] = new ObstacleInvisible("murDroit",largeurMonde-impossibleDroit-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[2] = new ObstacleInvisible("murHaut",largeurMonde/2,(hauteurMonde-impossibleHaut)+1,largeurMonde*1.2,(impossibleHaut/2)+2);
		obstaclesInvisibles[3] = new ObstacleInvisible("murBas",largeurMonde/2,-1,largeurMonde,2);
		
		//Fait apparaitre des arbres plus ou moins aleatoirement sur la map

		
		Decors pilier =  new Decors("Pilier", (90*largeurMonde/100), (50*hauteurMonde/100), (4*largeurMonde/100), (40*hauteurMonde/100), false, Images.Entites.Pilier.val() );
		pilier.getHB().setPosYM(pilier.getHB().getPosYM()-4*pilier.getHeight()/10);
		ObstacleInvisible obstaclePilier = new ObstacleInvisible("pilierHB",(890*largeurMonde/1000), (37*hauteurMonde/100), (20*largeurMonde/1000), (11*hauteurMonde/100));

		
		//genere des Paysans
		int nombreMonstre=15;
		Monstre listeMonstres[] = new Trolls[nombreMonstre];
		for(int i=0; i<listeMonstres.length; i++)
		{
			Trolls leTroll;
				double xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Marais.val())-1.7)));
				double yMonstre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.3));
				leTroll = new Trolls("trollmechant", xMonstre, yMonstre, Trolls.getUsualLifePoints(), Trolls.getUsualAttack(),
						Trolls.getUsualDefense(), Trolls.getUsualSpeed(),Trolls.getUsualWidth(), Trolls.getUsualHeight(), Trolls.getUsualKnockback(), false);
			listeMonstres[i] = leTroll;
		}
		//genere des pieges
		int nombrePieges=15;
		Monstre listePieges[] = new Piege[nombrePieges];
		for(int i=0; i<listePieges.length; i++)
		{
			Piege lePiege;
			double xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Marais.val())-1.7)));
			double yMonstre = ThreadLocalRandom.current().nextDouble(3,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.4));
			lePiege = new Piege("vilainPiege", xMonstre, yMonstre, Piege.getUsualLifePoints(),Piege.getUsualAttack(),Piege.getUsualDefense(),Piege.getUsualWidth(),Piege.getUsualHeight(), Images.Entites.Pique.val());

			
			listePieges[i] = lePiege;
		}
		
		for(int i=0; i<obstaclesInvisibles.length; i++)
		{
			entitesMaps.get("Marais").add(obstaclesInvisibles[i]);
		}

		entitesMaps.get("Marais").add(pilier);
		entitesMaps.get("Marais").add(obstaclePilier);

		for(int i=0; i<listeMonstres.length; i++)
		{
			entitesMaps.get("Marais").add(listeMonstres[i]);
		}	
		for(int i=0; i<listePieges.length; i++)
		{
			entitesMaps.get("Marais").add(listePieges[i]);
		}	
		
	}
	
	public static void initGrange()
	{
		entitesMaps.put("Grange",new ArrayList<Entite>());
		//definit la largeur, hauteur du monde, ainsi que la largeur et hauteur a laquelle
		//le joueur n'a pas le droit d'aller
		double largeurMonde = GameWorld.getWidthGrille() * (2*((double)(getLargeurMap(Map.Grange.val())+1)/3));
		double hauteurMonde = GameWorld.getHeightGrille();
		double impossibleDroit=0;
		double impossibleHaut=(double)3*hauteurMonde/10;
		
		ObstacleInvisible obstaclesInvisibles[] = new ObstacleInvisible[4];
		
		obstaclesInvisibles[0] = new ObstacleInvisible("murGauche",-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[1] = new ObstacleInvisible("murDroit",largeurMonde-impossibleDroit-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[2] = new ObstacleInvisible("murHaut",largeurMonde/2,(hauteurMonde-impossibleHaut)+1,largeurMonde*1.2,(impossibleHaut/2)+2);
		obstaclesInvisibles[3] = new ObstacleInvisible("murBas",largeurMonde/2,-1,largeurMonde,2);
		
		//Fait apparaitre des arbres plus ou moins aleatoirement sur la map

		
		Decors pilier =  new Decors("Pilier", (90*largeurMonde/100), (50*hauteurMonde/100), (4*largeurMonde/100), (40*hauteurMonde/100), false, Images.Entites.Pilier.val() );
		pilier.getHB().setPosYM(pilier.getHB().getPosYM()-4*pilier.getHeight()/10);
		ObstacleInvisible obstaclePilier = new ObstacleInvisible("pilierHB",(890*largeurMonde/1000), (37*hauteurMonde/100), (20*largeurMonde/1000), (11*hauteurMonde/100));

		
		//genere des Paysans
		int nombreMonstre=30;
		Monstre listeMonstres[] = new Paysan[nombreMonstre];
		for(int i=0; i<listeMonstres.length; i++)
		{
			Paysan lePaysan;
				double xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Grange.val())-1.7)));
				double yMonstre = ThreadLocalRandom.current().nextDouble(3,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.4));
				lePaysan = new Paysan("paysanVilain", xMonstre, yMonstre, Paysan.getUsualLifePoints(), Paysan.getUsualAttack(),
						Paysan.getUsualDefense(), Paysan.getUsualSpeed(),Paysan.getUsualWidth(), Paysan.getUsualHeight(), Paysan.getUsualKnockback());
			listeMonstres[i] = lePaysan;
		}
		//genere des pieges
		int nombrePieges=15;
		Monstre listePieges[] = new Piege[nombrePieges];
		for(int i=0; i<listePieges.length; i++)
		{
			Piege lePiege;
			double xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Grange.val())-1.7)));
			double yMonstre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.3));
			lePiege = new Piege("vilainPiege", xMonstre, yMonstre, Piege.getUsualLifePoints(),Piege.getUsualAttack(),Piege.getUsualDefense(),Piege.getUsualWidth(),Piege.getUsualHeight(), Images.Entites.Pique.val());

			listePieges[i] = lePiege;
		}
		
		for(int i=0; i<obstaclesInvisibles.length; i++)
		{
			entitesMaps.get("Grange").add(obstaclesInvisibles[i]);
		}

		entitesMaps.get("Grange").add(pilier);
		entitesMaps.get("Grange").add(obstaclePilier);

		for(int i=0; i<listeMonstres.length; i++)
		{
			entitesMaps.get("Grange").add(listeMonstres[i]);
		}	
		for(int i=0; i<listePieges.length; i++)
		{
			entitesMaps.get("Grange").add(listePieges[i]);
		}	
		
	}
	
	public static void initChateau()
	{
		entitesMaps.put("Chateau",new ArrayList<Entite>());
		//definit la largeur, hauteur du monde, ainsi que la largeur et hauteur a laquelle
		//le joueur n'a pas le droit d'aller
		double largeurMonde = GameWorld.getWidthGrille() * (2*((double)(getLargeurMap(Map.Chateau.val())+1)/3));
		double hauteurMonde = GameWorld.getHeightGrille();
		double impossibleDroit=0;
		double impossibleHaut=(double)3*hauteurMonde/10;
		
		ObstacleInvisible obstaclesInvisibles[] = new ObstacleInvisible[4];
		
		obstaclesInvisibles[0] = new ObstacleInvisible("murGauche",-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[1] = new ObstacleInvisible("murDroit",largeurMonde-impossibleDroit-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[2] = new ObstacleInvisible("murHaut",largeurMonde/2,(hauteurMonde-impossibleHaut)+1,largeurMonde*1.2,(impossibleHaut/2)+2);
		obstaclesInvisibles[3] = new ObstacleInvisible("murBas",largeurMonde/2,-1,largeurMonde,2);
		
		//Fait apparaitre des arbres plus ou moins aleatoirement sur la map

		
		Decors pilier =  new Decors("Pilier", (90*largeurMonde/100), (50*hauteurMonde/100), (4*largeurMonde/100), (40*hauteurMonde/100), false, Images.Entites.Pilier.val() );
		pilier.getHB().setPosYM(pilier.getHB().getPosYM()-4*pilier.getHeight()/10);
		ObstacleInvisible obstaclePilier = new ObstacleInvisible("pilierHB",(890*largeurMonde/1000), (37*hauteurMonde/100), (20*largeurMonde/1000), (11*hauteurMonde/100));

		
		//genere des monstres au hasard
		int nombreMonstre=30;
		Monstre listeMonstres[] = new Monstre[nombreMonstre];
		for(int i=0; i<listeMonstres.length; i++)
		{
			double xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Marais.val())-1.7)));
			
			double yMonstre = ThreadLocalRandom.current().nextDouble(3,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.4));
			int random = ThreadLocalRandom.current().nextInt(0,4);
			System.out.println(random);
			if(random==0)
				listeMonstres[i] = new Trolls("TrollVilain", xMonstre, yMonstre, Trolls.getUsualLifePoints(), Trolls.getUsualAttack(),
						Trolls.getUsualDefense(), Trolls.getUsualSpeed(),Trolls.getUsualWidth(), Trolls.getUsualHeight(), Trolls.getUsualKnockback(), false);
			else if(random==1)
				listeMonstres[i] = new Paysan("paysanVilain", xMonstre, yMonstre, Paysan.getUsualLifePoints(), Paysan.getUsualAttack(),
						Paysan.getUsualDefense(), Paysan.getUsualSpeed(),Paysan.getUsualWidth(), Paysan.getUsualHeight(), Paysan.getUsualKnockback());
			else if (random==2)
				listeMonstres[i] = new Lapin("lapinVilain", xMonstre, yMonstre, Lapin.getUsualLifePoints(), Lapin.getUsualAttack(),
						Lapin.getUsualDefense(), Lapin.getUsualSpeed(),Lapin.getUsualWidth(), Lapin.getUsualHeight(), Lapin.getUsualKnockback(), false);
			else if (random==3)
				listeMonstres[i] = new PaysanInvocateur("jacquiwscse",xMonstre, yMonstre,
						PaysanInvocateur.getUsualLifePoints(), PaysanInvocateur.getUsualAttack(),
						PaysanInvocateur.getUsualDefense(), PaysanInvocateur.getUsualSpeed(), PaysanInvocateur.getUsualWidth(),
						PaysanInvocateur.getUsualHeight(), PaysanInvocateur.getUsualKnockback(), 10, PaysanInvocateur.getUsualFrenquency(), false);
		}
		//genere des pieges
		int nombrePieges=15;
		Monstre listePieges[] = new Piege[nombrePieges];
		for(int i=0; i<listePieges.length; i++)
		{
			Piege lePiege;
			double xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Chateau.val())-1.7)));
			double yMonstre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.3));
			lePiege = new Piege("vilainPiege", xMonstre, yMonstre, Piege.getUsualLifePoints(),Piege.getUsualAttack(),Piege.getUsualDefense(),Piege.getUsualWidth(),Piege.getUsualHeight(), Images.Entites.Pique.val());

			listePieges[i] = lePiege;
		}
		
		for(int i=0; i<obstaclesInvisibles.length; i++)
		{
			entitesMaps.get("Chateau").add(obstaclesInvisibles[i]);
		}

		entitesMaps.get("Chateau").add(pilier);
		entitesMaps.get("Chateau").add(obstaclePilier);

		for(int i=0; i<listeMonstres.length; i++)
		{
			entitesMaps.get("Chateau").add(listeMonstres[i]);
		}	
		for(int i=0; i<listePieges.length; i++)
		{
			entitesMaps.get("Chateau").add(listePieges[i]);
		}	
		
	}
	
	public static void initForet()
	{
		entitesMaps.put("Foret",new ArrayList<Entite>());
		//definit la largeur, hauteur du monde, ainsi que la largeur et hauteur a laquelle
		//le joueur n'a pas le droit d'aller
		double largeurMonde = GameWorld.getWidthGrille() * (2*((double)(getLargeurMap(Map.Foret.val())+1)/3));
		double hauteurMonde = GameWorld.getHeightGrille();
		double impossibleDroit=GameWorld.getWidthGrille()/6;
		double impossibleHaut=(double)3*hauteurMonde/10;
		
		ObstacleInvisible obstaclesInvisibles[] = new ObstacleInvisible[4];
		
		obstaclesInvisibles[0] = new ObstacleInvisible("murGauche",-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[1] = new ObstacleInvisible("murDroit",largeurMonde-impossibleDroit-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[2] = new ObstacleInvisible("murHaut",largeurMonde/2,(hauteurMonde-impossibleHaut)+1,largeurMonde*1.2,(impossibleHaut/2)+2);
		obstaclesInvisibles[3] = new ObstacleInvisible("murBas",largeurMonde/2,-1,largeurMonde,2);
		
		//Fait apparaitre des arbres plus ou moins aleatoirement sur la map
		int nombreArbres=ThreadLocalRandom.current().nextInt(10,20);
		Decors obstaclesVisibles[] = new Decors[nombreArbres+1];
		for(int i=0; i<nombreArbres; i++)
		{
			double xArbre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille(),GameWorld.getWidthGrille()*((getLargeurMap(Map.Foret.val())-2.5)));
			double yArbre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut));
			double largeur = GameWorld.getWidthGrille() / 6;
			double hauteur = GameWorld.getHeightGrille() / 3;
			obstaclesVisibles[i] = new Decors("arbre", xArbre, yArbre, largeur, hauteur, true, Images.Entites.ArbreForet.val());
			obstaclesVisibles[i].getHB().setHeight(obstaclesVisibles[i].getHB().getHeight()/18);
			obstaclesVisibles[i].getHB().setPosYM(obstaclesVisibles[i].getHB().getPosYM()-((double)6.5*obstaclesVisibles[i].getHB().getHeight()));
			obstaclesVisibles[i].getHB().setWidth(obstaclesVisibles[i].getHB().getWidth()/4);
			
		}
		
		obstaclesVisibles[nombreArbres] = new Decors("Pilier", (90*largeurMonde/100), (50*hauteurMonde/100), (4*largeurMonde/100), (40*hauteurMonde/100), false, Images.Entites.Pilier.val() );
		obstaclesVisibles[nombreArbres].getHB().setPosYM(obstaclesVisibles[nombreArbres].getHB().getPosYM()-4*obstaclesVisibles[nombreArbres].getHeight()/10);
		ObstacleInvisible obstaclePilier = new ObstacleInvisible("pilierHB",(890*largeurMonde/1000), (37*hauteurMonde/100), (20*largeurMonde/1000), (11*hauteurMonde/100));

		
		//genere des lapins
		int nombreLapin=30;
		Monstre listeMonstres[] = new Lapin[nombreLapin];
		for(int i=0; i<listeMonstres.length; i++)
		{
			boolean collisionLapinArbre=false;
			Lapin leLapin;
			//pour ne pas que les lapins aparaissent dans les arbres
			do
			{
				collisionLapinArbre=false;
				double xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Foret.val())-1.7)));
				double yMonstre = ThreadLocalRandom.current().nextDouble(3,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.4));
				leLapin = new Lapin("lapinVilain", xMonstre, yMonstre, Lapin.getUsualLifePoints(), Lapin.getUsualAttack(),
						Lapin.getUsualDefense(), Lapin.getUsualSpeed(),Lapin.getUsualWidth(), Lapin.getUsualHeight(),
						Lapin.getUsualKnockback(), false);
				for(int j=0; j<obstaclesVisibles.length; j++)
				{
					if(Hitbox.collision(leLapin.getHB(), obstaclesVisibles[j].getHB()))
						collisionLapinArbre=true;
				}
			} while(collisionLapinArbre);

			listeMonstres[i] = leLapin;
		}
		//genere des pieges
		int nombrePieges=25;
		Monstre listePieges[] = new Piege[nombrePieges];
		for(int i=0; i<listePieges.length; i++)
		{
			boolean collisionPiegeArbre=false;
			Piege lePiege;
			//pour ne pas que les pieges aparaissent dans les arbres
			do
			{
				collisionPiegeArbre=false;
				double xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Foret.val())-1.7)));
				double yMonstre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.3));
				lePiege = new Piege("vilainPiege", xMonstre, yMonstre, Piege.getUsualLifePoints(),Piege.getUsualAttack(),Piege.getUsualDefense(),Piege.getUsualWidth(),Piege.getUsualHeight(), Images.Entites.Pique.val());
				for(int j=0; j<obstaclesVisibles.length; j++)
				{
					if(Hitbox.collision(lePiege.getHB(), obstaclesVisibles[j].getHB()))
						collisionPiegeArbre=true;
				}
			}while(collisionPiegeArbre);
			listePieges[i] = lePiege;
		}
		
		for(int i=0; i<obstaclesInvisibles.length; i++)
		{
			entitesMaps.get("Foret").add(obstaclesInvisibles[i]);
		}
		for(int i=0; i<obstaclesVisibles.length; i++)
		{
			entitesMaps.get("Foret").add(obstaclesVisibles[i]);
		}	
		for(int i=0; i<listeMonstres.length; i++)
		{
			entitesMaps.get("Foret").add(listeMonstres[i]);
		}	
		for(int i=0; i<listePieges.length; i++)
		{
			entitesMaps.get("Foret").add(listePieges[i]);
		}	
		entitesMaps.get("Foret").add(obstaclePilier);
		
	}
	
	public static void initBase()
	{
		double largeurMonde = GameWorld.getWidthGrille() * 2;
		double hauteurMonde = GameWorld.getHeightGrille() *2;
		double impossibleDroit=0;
		double impossibleHaut=(34*(double)GameWorld.getHeightGrille()/100);
		
		ObstacleInvisible obstaclesInvisibles[] = new ObstacleInvisible[12];
		
		obstaclesInvisibles[0] = new ObstacleInvisible("murGaucheHB",-1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[1] = new ObstacleInvisible("murDroitHB",largeurMonde-impossibleDroit+1,hauteurMonde/2,2,hauteurMonde*1.2);
		obstaclesInvisibles[2] = new ObstacleInvisible("murHautHB",largeurMonde/2,(hauteurMonde-impossibleHaut)+1,largeurMonde*1.2,(impossibleHaut/2)+2);
		obstaclesInvisibles[3] = new ObstacleInvisible("murBasHB",largeurMonde/2,-1,largeurMonde,2);
		obstaclesInvisibles[4] = new ObstacleInvisible("BoxDroiteBijoutierHB", (43*largeurMonde/100), (55*hauteurMonde/100), (4*largeurMonde/100), (5*hauteurMonde/100) );
		obstaclesInvisibles[5] = new ObstacleInvisible("BoxGaucheBijoutierHB", (53*largeurMonde/100), (55*hauteurMonde/100), (4*largeurMonde/100), (5*hauteurMonde/100) );
		obstaclesInvisibles[6] = new ObstacleInvisible("ForgeronHB", (24*largeurMonde/100), (43*hauteurMonde/100), (8*largeurMonde/100), (9*hauteurMonde/100));
		obstaclesInvisibles[7] = new ObstacleInvisible("FontaineHB", (51*largeurMonde/100), (33*hauteurMonde/100), (11*largeurMonde/100), (10*hauteurMonde/100));
		obstaclesInvisibles[8] = new ObstacleInvisible("MoulinHB", (91*largeurMonde/100), (13*hauteurMonde/100), (10*largeurMonde/100), (11*hauteurMonde/100));
		obstaclesInvisibles[9] = new ObstacleInvisible("LeVieuxHB", (44*largeurMonde/100), (30*hauteurMonde/100), (2*largeurMonde/100), (4*hauteurMonde/100));
		obstaclesInvisibles[10] = new ObstacleInvisible("Arbre2HB", (31*largeurMonde/100), (61*hauteurMonde/100), (5*largeurMonde/100), (4*hauteurMonde/100));
		obstaclesInvisibles[11] = new ObstacleInvisible("BjoutierHB", (48*largeurMonde/100), (57*hauteurMonde/100), (8*largeurMonde/100), (9*hauteurMonde/100));

		
		Decors decors[] = new Decors[4];
		decors[0] = new Decors("Forgeron", (25*largeurMonde/100), (45*hauteurMonde/100), (10*largeurMonde/100), (15*hauteurMonde/100), false, Images.Entites.Forgeron.val() );
		decors[1] = new Decors("Moulin", (91*largeurMonde/100), (24*hauteurMonde/100), (15*largeurMonde/100), (35*hauteurMonde/100), false, Images.Entites.Moulin.val() );
		decors[1].getHB().setPosYM(decors[1].getHB().getPosYM()-decors[1].getHeight()/3);
		decors[2] = new Decors("LeVieux", (44*largeurMonde/100), (29*hauteurMonde/100), (2*largeurMonde/100), (4*hauteurMonde/100), false, Images.Entites.Vieux.val() );
		decors[3] = new Decors("Arbre2", (32*largeurMonde/100), (70*hauteurMonde/100), (15*largeurMonde/100), (28*hauteurMonde/100), false, Images.Entites.Arbre2.val() );
		decors[3].getHB().setPosYM(decors[3].getHB().getPosYM()-3*decors[3].getHeight()/10);

		Marchand leBijoutier = new Marchand("Bijoutier", (48*largeurMonde/100), (59*hauteurMonde/100), (13*largeurMonde/100), (16*hauteurMonde/100), false, Images.Entites.Bijoutier.val() );
		leBijoutier.getHB().setPosYM(leBijoutier.getY()-leBijoutier.getHB().getHeight()/2);
		leBijoutier.getHB().setHeight(leBijoutier.getHeight()/2);
		leBijoutier.getHB().setWidth(leBijoutier.getWidth()/2);
		
		
		PorteMagique monde1 = new PorteMagique("PorteM1", (7.14*largeurMonde/100), (85*hauteurMonde/100), (6*largeurMonde/100) ,
												(14*hauteurMonde/100), false, Maps.Map.Foret.val(), false);
		entitesMaps.get("Base").add(monde1);
		PorteMagique monde2 = new PorteMagique("PorteM2", (21*largeurMonde/100), (85*hauteurMonde/100), (6*largeurMonde/100) ,
				(14*hauteurMonde/100), false, Maps.Map.Marais.val(), false);
		entitesMaps.get("Base").add(monde2);
		PorteMagique monde3 = new PorteMagique("PorteM3", (78.5*largeurMonde/100), (85*hauteurMonde/100), (6*largeurMonde/100) ,
				(14*hauteurMonde/100), false, Maps.Map.Grange.val(), false);
		entitesMaps.get("Base").add(monde3);
		PorteMagique monde4 = new PorteMagique("PorteM4", (92.5*largeurMonde/100), (85*hauteurMonde/100), (6*largeurMonde/100) ,
				(14*hauteurMonde/100), false, Maps.Map.Foret.val(), false);
		entitesMaps.get("Base").add(monde4);
		
		PorteMagique boss = new PorteMagique("PorteBoss", (49*largeurMonde/100), (88*hauteurMonde/100), (17*largeurMonde/100) ,
				(25*hauteurMonde/100), false, Maps.Map.Chateau.val(), false);
		entitesMaps.get("Base").add(boss);
		
		monde1.getHB().setPosYM(monde1.getY()+monde1.getHeight()/25);
		monde2.getHB().setPosYM(monde2.getY()+monde2.getHeight()/25);
		monde3.getHB().setPosYM(monde3.getY()+monde3.getHeight()/25);
		monde4.getHB().setPosYM(monde4.getY()+monde4.getHeight()/25);
		boss.getHB().setPosYM(boss.getY()+boss.getHeight()/10);
		
		EntiteAnimee helice = new EntiteAnimee("helice",(91*largeurMonde/100), (33*hauteurMonde/100), (25*largeurMonde/100), (25*largeurMonde/100), false, false, Images.Entites.Helice.val(), 0.5);
		helice.getHB().setPosYM(0);
		entitesMaps.get("Base").add(helice);
		EntiteAnimee fontaine = new EntiteAnimee( "fontaine", (51*largeurMonde/100), (34*hauteurMonde/100), (14*largeurMonde/100), (18*hauteurMonde/100), false, false, Images.Entites.Fontaine.val(),0);
		entitesMaps.get("Base").add(fontaine);
		
		for(int i=0; i<obstaclesInvisibles.length; i++)
		{
			entitesMaps.get("Base").add(obstaclesInvisibles[i]);
		}
		for(int i=0; i<decors.length; i++)
		{
			entitesMaps.get("Base").add(decors[i]);
		}
		entitesMaps.get("Base").add(leBijoutier);
		
	}
	
	public static ArrayList<Entite> getEntitesMap(int quelleMap)
	{
		if(quelleMap==Map.Base.val())
			return entitesMaps.get("Base");
		else if(quelleMap==Map.Foret.val())
			return entitesMaps.get("Foret");
		else if(quelleMap==Map.Marais.val())
			return entitesMaps.get("Marais");
		else if(quelleMap==Map.Montagnes.val())
			return entitesMaps.get("Montagnes");
		else if(quelleMap==Map.Chateau.val())
			return entitesMaps.get("Chateau");
		else if(quelleMap==Map.Grange.val())
			return entitesMaps.get("Grange");
		else
			return new ArrayList<Entite>();
	}
	
	/**
	 * Ajoute les monstres du niveau au niveau, en fonction de la map actuelle, du nombre de monstres
	 * deja presents et du nombre de monstres deja tues
	 * @param laMap
	 * @param monstres
	 * @param posXJoueur
	 * @param nombreDejaTues
	 */
	public static void ajouterMonstres(int laMap, ArrayList<Monstre> monstres, double posXJoueur, int nombreDejaTues)
	{
		if(laMap==Map.Base.val())
			return;
		//ajoute des monstres dans la foret au fur a mesure, et fais apparaitre un boss.
		else if (laMap==Map.Foret.val())
		{
			if(monstres.stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()).size()<30 && nombreDejaTues<70)
			{
				double hauteurMonde = GameWorld.getHeightGrille();
				double impossibleHaut=(double)3*hauteurMonde/10;
				double xMonstre;
				do
					xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Foret.val())-1.7)));
				while (Math.abs(xMonstre-posXJoueur)<GameWorld.getWidthGrille()/2);
				
				double yMonstre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.3));
				double largeur = Lapin.getUsualWidth();
				double hauteur = Lapin.getUsualHeight();
				monstres.add(new Lapin("lapinVilain", xMonstre, yMonstre, Lapin.getUsualLifePoints(), Lapin.getUsualAttack(),
						Lapin.getUsualDefense(), Lapin.getUsualSpeed(),largeur, hauteur, Lapin.getUsualKnockback(), false));
			}
			//fait apparaitre le boss
			else if(monstres.stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()).size()<5 && !GameWorld.bossApparu())
			{
				for(int i=0; i<monstres.size(); i++)
				{
					if(monstres.get(i) instanceof Lapin)
					{
						Lapin leLapin = (Lapin)monstres.get(i--);
						leLapin.deceder();
					}
					else if(monstres.get(i) instanceof Piege)
					{
						Piege lePiege = (Piege)monstres.get(i--);
						lePiege.deceder();
					}
				}

				monstres.clear();
				Lapin leboss = new Lapin("lapinVilain", GameWorld.getWidthGrille()*4, GameWorld.getHeightGrille()/3,
						Lapin.getUsualLifePoints()*15, Lapin.getUsualAttack()*3, Lapin.getUsualDefense()*3,
						Lapin.getUsualSpeed()*1.8,Lapin.getUsualWidth()*4, Lapin.getUsualHeight()*4, Lapin.getUsualKnockback()*1.5, true);
				leboss.ajouterItemTresor(new Cle("laCleMarais", 0, 0, 1, 1, Maps.Map.Marais.val() ));
				int random = ThreadLocalRandom.current().nextInt(0,4);
				Item leLoot;
				if(random==0)
					leLoot=new Bijoux("bijou cool1", leboss.getX(), leboss.getY(), 50, 1,1,
								Bijoux.TypesBonus.chance.val(), Images.Entites.bijouChance.val(), 30);
				else if(random==1)
					leLoot=new Bijoux("bijou cool2", leboss.getX(), leboss.getY(), 20, 1,1,
								Bijoux.TypesBonus.attaque.val(), Images.Entites.bijouDegats.val(), 30);
				else if(random==2)
					leLoot=new Bijoux("bijou cool3", leboss.getX(), leboss.getY(), 10, 1,1,
								Bijoux.TypesBonus.defense.val(), Images.Entites.bijouDefense.val(), 30);
				else
					leLoot=new Bijoux("bijou cool4", leboss.getX(), leboss.getY(), 50, 1,1,
								Bijoux.TypesBonus.gainXp.val(), Images.Entites.bijouGainXP.val(), 30);
				leboss.ajouterItemTresor(leLoot);
				leboss.ajouterItemTresor(new Money("thunes", 0,0,2,2,50));
				entitesMaps.get("Foret").add(leboss);

				monstres.add(leboss);
				
				System.out.println("TROP FORT ! ");
				GameWorld.bossApparait();
			}
			GameWorld.syncMonstresEntites();
		}
		else if(laMap==Map.Marais.val())
		{

			if(monstres.stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()).size()<15 && nombreDejaTues<30)
			{
				double hauteurMonde = GameWorld.getHeightGrille();
				double impossibleHaut=(double)3*hauteurMonde/10;
				double xMonstre;
				do
					xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Marais.val())-1.7)));
				while (Math.abs(xMonstre-posXJoueur)<GameWorld.getWidthGrille()/2);
				
				double yMonstre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.3));
				monstres.add(new Trolls("TrollVilain", xMonstre, yMonstre, Trolls.getUsualLifePoints(), Trolls.getUsualAttack(),
						Trolls.getUsualDefense(), Trolls.getUsualSpeed(),Trolls.getUsualWidth(), Trolls.getUsualHeight(), Trolls.getUsualKnockback(), false));
			}

			//fait apparaitre le boss
			else if(monstres.stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()).size()<5 && !GameWorld.bossApparu())
			{
				for(int i=0; i<monstres.size(); i++)
				{
					if(monstres.get(i) instanceof Trolls)
					{
						Trolls leTroll= (Trolls)monstres.get(i--);
						leTroll.deceder();
					}
					else if(monstres.get(i) instanceof Piege)
					{
						Piege lePiege = (Piege)monstres.get(i--);
						lePiege.deceder();
					}
				}
	
				monstres.clear();
				Trolls leboss = new Trolls("boss2", GameWorld.getWidthGrille()*4, GameWorld.getHeightGrille()/3,
						Trolls.getUsualLifePoints()*5, Trolls.getUsualAttack()*2,
				(int)(Trolls.getUsualDefense()*1.5), Trolls.getUsualSpeed()*2, Trolls.getUsualWidth()*0.5,
				Trolls.getUsualHeight()*0.5, Trolls.getUsualKnockback()*2, true);
				Item leLoot = new Bijoux("bijou cool0", leboss.getX(), leboss.getY(), 50, 1,1,
						Bijoux.TypesBonus.vitesseAttaque.val(), Images.Entites.bijouVitesseAttaque.val(), 100);
					leboss.ajouterItemTresor(leLoot);
					leboss.ajouterItemTresor(new Money("thunes", 0,0,2,2,100));

				
				entitesMaps.get("Marais").add(leboss);
				leboss.ajouterItemTresor(new Cle("laCleGrange", 0, 0, 1, 1, Maps.Map.Grange.val()));
				monstres.add(leboss);
				System.out.println("TROP FORT ! ");
				GameWorld.bossApparait();
				
			}
			GameWorld.syncMonstresEntites();
		}
		else if (laMap==Map.Chateau.val())
		{

			if(monstres.stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()).size()<40 && nombreDejaTues<100)
			{
				double hauteurMonde = GameWorld.getHeightGrille();
				double impossibleHaut=(double)3*hauteurMonde/10;
				double xMonstre;
				do
					xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Chateau.val())-1.7)));
				while (Math.abs(xMonstre-posXJoueur)<GameWorld.getWidthGrille()/2);
				
				double yMonstre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.3));
				int random = ThreadLocalRandom.current().nextInt(0,3);
				if(ThreadLocalRandom.current().nextInt(0,5)==1) //pour ne pas trop avoir de paysan invocateur
					random++;
				if(random==0)
					monstres.add(new Trolls("TrollVilain", xMonstre, yMonstre, Trolls.getUsualLifePoints(), (int)(Trolls.getUsualAttack()*1.4),
							Trolls.getUsualDefense(), Trolls.getUsualSpeed(),Trolls.getUsualWidth()*1.4, Trolls.getUsualHeight()*1.4, Trolls.getUsualKnockback(), false));
				else if(random==1)
					monstres.add(new Paysan("paysanVilain", xMonstre, yMonstre, Paysan.getUsualLifePoints(), Paysan.getUsualAttack(),
							Paysan.getUsualDefense(), Paysan.getUsualSpeed(),Paysan.getUsualWidth(), Paysan.getUsualHeight(), Paysan.getUsualKnockback()));
				else if (random==2)
					monstres.add(new Lapin("lapinVilain", xMonstre, yMonstre, Lapin.getUsualLifePoints(), Lapin.getUsualAttack(),
							Lapin.getUsualDefense(), Lapin.getUsualSpeed(),Lapin.getUsualWidth(), Lapin.getUsualHeight(), Lapin.getUsualKnockback(), false));
				else if (random==3)
					monstres.add(new PaysanInvocateur("jacquiwscse",xMonstre, yMonstre,
							PaysanInvocateur.getUsualLifePoints(), PaysanInvocateur.getUsualAttack(),
							PaysanInvocateur.getUsualDefense(), PaysanInvocateur.getUsualSpeed(), PaysanInvocateur.getUsualWidth(),
							PaysanInvocateur.getUsualHeight(), PaysanInvocateur.getUsualKnockback(), 10, PaysanInvocateur.getUsualFrenquency(), false));

			}

			//fait apparaitre le boss
			else if(monstres.stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()).size()<5 && !GameWorld.bossApparu())
			{
				for(int i=0; i<monstres.size(); i++)
				{
					if(monstres.get(i) instanceof Monstre)
					{
						Monstre leMonstre = (Monstre)monstres.get(i--);
						leMonstre.deceder();
					}
					else if(monstres.get(i) instanceof Piege)
					{
						Piege lePiege = (Piege)monstres.get(i--);
						lePiege.deceder();
					}
				}
	
				monstres.clear();
				Dragon bossFinal = new Dragon("DRAGOOON", GameWorld.getWidthGrille()*3, GameWorld.getHeightGrille()/2,
						Dragon.getUsualLifePoints(), Dragon.getUsualAttack(),
				Dragon.getUsualDefense(), Dragon.getUsualSpeed(), Dragon.getUsualWidth(),
				Dragon.getUsualHeight(), Dragon.getUsualKnockback(), Dragon.getUsualDurationAttack());
				PaysanInvocateur garde1= new PaysanInvocateur("jacquiwscse",GameWorld.getWidthGrille()/3, GameWorld.getHeightGrille()/4,
						PaysanInvocateur.getUsualLifePoints(), PaysanInvocateur.getUsualAttack(),
						PaysanInvocateur.getUsualDefense(), PaysanInvocateur.getUsualSpeed(), PaysanInvocateur.getUsualWidth(),
						PaysanInvocateur.getUsualHeight(), PaysanInvocateur.getUsualKnockback(), 10, PaysanInvocateur.getUsualFrenquency()*0.5, false);
				PaysanInvocateur garde2= new PaysanInvocateur("jacquiwscse",GameWorld.getWidthGrille()/3, GameWorld.getHeightGrille()/4,
						PaysanInvocateur.getUsualLifePoints(), PaysanInvocateur.getUsualAttack(),
						PaysanInvocateur.getUsualDefense(), PaysanInvocateur.getUsualSpeed(), PaysanInvocateur.getUsualWidth(),
						PaysanInvocateur.getUsualHeight(), PaysanInvocateur.getUsualKnockback(), 10, PaysanInvocateur.getUsualFrenquency()*0.5, false);
						
				bossFinal.ajouterItemTresor(new Money("thunes", 0,0,2,2,5000));
				
				entitesMaps.get("Chateau").add(bossFinal);
				entitesMaps.get("Chateau").add(garde1);
				entitesMaps.get("Chateau").add(garde2);
				
				monstres.add(bossFinal);
				monstres.add(garde1);
				monstres.add(garde2);

				System.out.println("TROP FORT ! ");
				GameWorld.bossApparait();
				
			}
			GameWorld.syncMonstresEntites();
		}
		else if(laMap==Map.Grange.val())
		{
			if(monstres.stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()).size()<30 && nombreDejaTues<50)
			{
				double hauteurMonde = GameWorld.getHeightGrille();
				double impossibleHaut=(double)3*hauteurMonde/10;
				double xMonstre;
				do
					xMonstre = ThreadLocalRandom.current().nextDouble(GameWorld.getWidthGrille()/2,GameWorld.getWidthGrille()*((getLargeurMap(Map.Grange.val())-1.7)));
				while (Math.abs(xMonstre-posXJoueur)<GameWorld.getWidthGrille()/2);
				
				double yMonstre = ThreadLocalRandom.current().nextDouble(1,(int)(GameWorld.getHeightGrille()-impossibleHaut*1.3));
				monstres.add(new Paysan("paysanVilain", xMonstre, yMonstre, Paysan.getUsualLifePoints(), Paysan.getUsualAttack(),
						Paysan.getUsualDefense(), Paysan.getUsualSpeed(),Paysan.getUsualWidth(), Paysan.getUsualHeight(), Paysan.getUsualKnockback()));
			}

			//fait apparaitre le boss
			else if(monstres.stream().filter(x -> !(x instanceof Piege)).collect(Collectors.toList()).size()<5 && !GameWorld.bossApparu())
			{
				for(int i=0; i<monstres.size(); i++)
				{
					if(monstres.get(i) instanceof Paysan)
					{
						Paysan lePaysan = (Paysan)monstres.get(i--);
						lePaysan.deceder();
					}
					else if(monstres.get(i) instanceof Piege)
					{
						Piege lePiege = (Piege)monstres.get(i--);
						lePiege.deceder();
					}
				}
	
				monstres.clear();
				PaysanInvocateur jacquiea = new PaysanInvocateur("jacquiwscse",GameWorld.getWidthGrille()*4, 11*GameWorld.getHeightGrille()/30,
				PaysanInvocateur.getUsualLifePoints(), PaysanInvocateur.getUsualAttack(),
				PaysanInvocateur.getUsualDefense(), PaysanInvocateur.getUsualSpeed(), PaysanInvocateur.getUsualWidth(),
				PaysanInvocateur.getUsualHeight(), PaysanInvocateur.getUsualKnockback(), 10, PaysanInvocateur.getUsualFrenquency(), false);
				PaysanInvocateur jacquie1 = new PaysanInvocateur("jacquiesc", GameWorld.getWidthGrille()*4, 9*GameWorld.getHeightGrille()/30,
				PaysanInvocateur.getUsualLifePoints(), PaysanInvocateur.getUsualAttack(),
				PaysanInvocateur.getUsualDefense(), PaysanInvocateur.getUsualSpeed(), PaysanInvocateur.getUsualWidth(),
				PaysanInvocateur.getUsualHeight(), PaysanInvocateur.getUsualKnockback(), 10, PaysanInvocateur.getUsualFrenquency(), false);
				PaysanInvocateur leboss = new PaysanInvocateur("jacquiwcse2", GameWorld.getWidthGrille()*4, GameWorld.getHeightGrille()/3,
				PaysanInvocateur.getUsualLifePoints()*5, PaysanInvocateur.getUsualAttack()*2,
				(int)(PaysanInvocateur.getUsualDefense()*1.5), PaysanInvocateur.getUsualSpeed()*0.8, PaysanInvocateur.getUsualWidth()*1.5,
				PaysanInvocateur.getUsualHeight()*1.5, PaysanInvocateur.getUsualKnockback(), 15, PaysanInvocateur.getUsualFrenquency()*1.5, true);
				entitesMaps.get("Grange").add(leboss);
				entitesMaps.get("Grange").add(jacquiea);
				entitesMaps.get("Grange").add(jacquie1);
				leboss.ajouterItemTresor(new Cle("laClefChateau", 0, 0, 1, 1, Maps.Map.Chateau.val() ));
				Item leLoot;

				leLoot = new Bijoux("bijou cool9", leboss.getX(), leboss.getY(), 2, 1,1,
					Bijoux.TypesBonus.volVie.val(), Images.Entites.bijouVolVie.val(), 30);
				leboss.ajouterItemTresor(leLoot);
				leboss.ajouterItemTresor(new Money("thunes", 0,0,2,2,250));

				monstres.add(leboss);
				monstres.add(jacquiea);
				monstres.add(jacquie1);
				System.out.println("TROP FORT ! ");
				GameWorld.bossApparait();
				
			}
			GameWorld.syncMonstresEntites();
		}
	}
	
}