public class Decors extends EntiteAnimee
{	
	//utile pour ne pas instancier deux ojets avec le meme nom.
	private static int numeroInstanciation=0;
	
	/**
	 * Constructeur des decors.
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param isObstacle
	 * @param entite
	 */
	public Decors(String nom, double x, double y, double width, double height, boolean isObstacle, int entite)
	{
		//Entite : nom, positionX, positionY, largeur, hauteur, obstacle?, 
		//c'est un etre vivant ? , quelle entite c'est?, rotationContinuelle
		super(nom+String.valueOf(numeroInstanciation), x, y, width, height, isObstacle, false, entite, 0);
		numeroInstanciation++;
	}

}
