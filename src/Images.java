/*
 * Une classe Images pour stocker toutes les images, afin de ne pas avoir à les recréer et
 * les redimensionner ? a chaque utilisation.
 */

import java.awt.Image;

public class Images
{
	
	/**
	 * 
	 * Pour que ce soit un peu moins sale au moment de l'appel
	 * Permet de se retrouver plus facilement parmi les images a afficher
	 */
		public enum Entites
		{
			  Perso(0), //perso
			  Lapin(1), Goblin(2), Majordome(3),Paysan(4), PaysanInvocateur(5), Piege(6), Trolls(7), //ennemis
			  Arme(8), Armure(9), Bijoux(10), Bombe(11), Cle(12), Money(19), LifePotion(20), ManaPotion(33), //items
			  ArbreForet(13), Moulin(14), Bijoutier(15), Forgeron(16), //decors
			  Fontaine(17), ArbreCentre(18), PorteMagique(21), Mur(22), Invisible(23), //decors
			  Helice(24), Vieux(25), Arbre2(26), Error(34), Pilier(38), //decors
			  Inventaire(28), Baston(29), Distance(30), Mage(31), Surbrillance(32), //inventaire
			  Pique(35), Explosion(36), Sang(37), Bar(39), Poule(40),
			  Epee1(41),Epee2(42),Epee3(43),Epee4(44),Epee5(45),Epee6(46),EpeeCrakey(50),
			  Arc1(55) , Arc2(56), Arc3(57),
			  Baton1(64), Baton2(65), Baton3(66),
			  Sort1(70), Armure1(71), Armure2(72), Armure3(73), Armure4(74), Armure5(75),
			  Armure6(76), Armure7(77), Armure8(78), Armure9(79), Armure10(80), 
			  Porte(51), PorteFermee(52), PorteBoss(53), PorteBossFermee(54),
			  bijouChance(81), bijouDefense(82), bijouDegats(83), bijouMana(84), bijouKnockback(85), 
			  bijouVie(86), bijouVitesse(87), bijouVitesseAttaque(88), bijouVolVie(89), bijouGainXP(90),
			  guiMarchand(91), acheterSel(92), vendreSel(93), Dragon(94), Fleche(95), EmblemeMago(96),
			  EmblemeDist(97), EmblemeBast(98), SelectMage(99), SelectArcher(100), SelectBourrin(101),
			  ChoixArbre(102);
			  
			//actuellement : 102 max
		  private int valeur;
		  private Entites(int val)
		  {
		    this.valeur = val;
		  }
		  public int val()
		  {
			  return valeur;
		  }
		}

	//toutes les images a garder en memoire, pas si lourd que ca (contrairement a ce qu'il parait)
	private static Image piege[];
	private static Image fleche;
	private static Image perso0,perso1,perso2,perso3,perso4,perso5, persoFrappeDroite;
	private static Image perso10,perso11,perso12,perso13,perso14,perso15, persoFrappeGauche;
	private static Image base[], foret[];
	private static Image error;
	private static Image arbre1;
	private static Image bombe;
	private static Image bijoutier;
	private static Image arbre2;
	private static Image forgeron;
	private static Image leVieux;
	private static Image fontaine[];
	private static Image moulin;
	private static Image helice;
	private static Image inventaire;
	private static Image lifePotion;
	private static Image manaPotion;
	private static Image mage;
	private static Image baston;
	private static Image distance;
	private static Image surbrillance;
	private static Image pique[];
	private static Image explosion[];
	private static Image lapin[];
	private static Image sang;
	private static Image pilier;
	private static Image money;
	private static Image bar;
	private static Image[] poule;
	private static Image[] epees;
	private static Image[] arcs;
	private static Image[] batons;
	private static Image[] sort1;
	private static Image[] paysan;
	private static Image porte, porteFermee, porteBoss, porteBossFermee;
	private static Image[] grange;
	private static Image[] marais;
	private static Image[] chateau;
	private static Image[] armures;
	private static Image[] bijoux;
	private static Image guiMarchand, acheter_sel, vendre_sel;
	private static Image[] troll;
	private static Image[] dragon;
	private static Image clef;
	private static Image flecheDroite, flecheGauche;
	private static Image[] emblemes;
	private static Image selectArcher;
	private static Image selectMage;
	private static Image selectBourrin;
	private static Image choixClasse;
	
	/**
	 * charge toutes les images, pour ne pas avoir a le faire apres.
	 */
	public static void init()
	{
		try
		{
			perso0 = StdDraw.getImage("img/Sprites/Personnage_Principal/0.png");
			perso1 = StdDraw.getImage("img/Sprites/Personnage_Principal/1.png");
			perso2 = StdDraw.getImage("img/Sprites/Personnage_Principal/2.png");
			perso3 = StdDraw.getImage("img/Sprites/Personnage_Principal/3.png");
			perso4 = StdDraw.getImage("img/Sprites/Personnage_Principal/4.png");
			perso5 = StdDraw.getImage("img/Sprites/Personnage_Principal/5.png");
			persoFrappeDroite = StdDraw.getImage("img/Sprites/Personnage_Principal/0.png");
			perso10 = StdDraw.getImage("img/Sprites/Personnage_Principal/10.png");
			perso11 = StdDraw.getImage("img/Sprites/Personnage_Principal/11.png");
			perso12 = StdDraw.getImage("img/Sprites/Personnage_Principal/12.png");
			perso13 = StdDraw.getImage("img/Sprites/Personnage_Principal/13.png");
			perso14 = StdDraw.getImage("img/Sprites/Personnage_Principal/14.png");
			perso15 = StdDraw.getImage("img/Sprites/Personnage_Principal/15.png");
			persoFrappeGauche = StdDraw.getImage("img/Sprites/Personnage_Principal/10.png");
			error = StdDraw.getImage("img/Sprites/error.png");
				lapin = new Image[6];
				poule = new Image[6];
				paysan = new Image[6];
				troll = new Image[6];
				dragon = new Image[24];
			chargerMap(Maps.Map.Base.val());
			chargerMonstre(Images.Entites.Lapin.val());
			//chargerMonstre(Images.Entites.Poule.val());
			chargerMonstre(Images.Entites.Paysan.val());
			bombe = StdDraw.getImage("img/Sprites/Explosion/bomb.png");
						helice = StdDraw.getImage("img/Map/Base/Sprites/helice.png");
			inventaire = StdDraw.getImage("img/Sprites/Inventaire/Inventaire.png");
			lifePotion = StdDraw.getImage("img/Sprites/Inventaire/LifePotion.png");
			manaPotion = StdDraw.getImage("img/Sprites/Inventaire/ManaPotion.png");
			mage = StdDraw.getImage("img/Sprites/Inventaire/Mago.png");
			baston = StdDraw.getImage("img/Sprites/Inventaire/Bast.png");
			distance = StdDraw.getImage("img/Sprites/Inventaire/Dist.png");
			surbrillance = StdDraw.getImage("img/Sprites/Inventaire/Surbrillance.png");
			explosion = new Image[14];
			explosion[0] = StdDraw.getImage("img/Sprites/Explosion/Ex1.png");
			explosion[1] = StdDraw.getImage("img/Sprites/Explosion/Ex2.png");
			explosion[2] = StdDraw.getImage("img/Sprites/Explosion/Ex3.png");
			explosion[3] = StdDraw.getImage("img/Sprites/Explosion/Ex4.png");
			explosion[4] = StdDraw.getImage("img/Sprites/Explosion/Ex5.png");
			explosion[5] = StdDraw.getImage("img/Sprites/Explosion/Ex6.png");
			explosion[6] = StdDraw.getImage("img/Sprites/Explosion/Ex7.png");
			explosion[7] = StdDraw.getImage("img/Sprites/Explosion/Ex8.png");
			explosion[8] = StdDraw.getImage("img/Sprites/Explosion/Ex9.png");
			explosion[9] = StdDraw.getImage("img/Sprites/Explosion/Ex10.png");
			explosion[10] = StdDraw.getImage("img/Sprites/Explosion/Ex11.png");
			explosion[11] = StdDraw.getImage("img/Sprites/Explosion/Ex12.png");
			explosion[12] = StdDraw.getImage("img/Sprites/Explosion/Ex13.png");
			explosion[13] = StdDraw.getImage("img/Sprites/Explosion/Ex14.png");
			clef = StdDraw.getImage("img/Sprites/Kley.png");
			
			pique = new Image[2];
			pique[0] = StdDraw.getImage("img/Sprites/Ennemis/Pique/pic-pic_cachey.png");
			pique[1] = StdDraw.getImage("img/Sprites/Ennemis/Pique/pic-pic_sorti.png");
			//fleche = StdDraw.getImage("");
			sang = StdDraw.getImage("img/Sprites/Ennemis/sprotch.png");
			pilier= StdDraw.getImage("img/Map/Foret/Pillar.png");
			money = StdDraw.getImage("img/Sprites/Inventaire/Coin.png");
			bar = StdDraw.getImage("img/Sprites/b4r_2_v13.png");
			epees = new Image[7];
			epees[0]= StdDraw.getImage("img/Sprites/Armes/Epees/1.png");
			epees[1]= StdDraw.getImage("img/Sprites/Armes/Epees/2.png");
			epees[2]= StdDraw.getImage("img/Sprites/Armes/Epees/3.png");
			
			epees[3]= StdDraw.getImage("img/Sprites/Armes/Epees/4.png");
			epees[4]= StdDraw.getImage("img/Sprites/Armes/Epees/5.png");
			epees[5]= StdDraw.getImage("img/Sprites/Armes/Epees/6.png");
			
			epees[6]= StdDraw.getImage("img/Sprites/Armes/Epees/crakey.png");
			
			arcs = new Image[3];
			arcs[0]= StdDraw.getImage("img/Sprites/Armes/Arcs/1.png");
			arcs[1]= StdDraw.getImage("img/Sprites/Armes/Arcs/2.png");
			arcs[2]= StdDraw.getImage("img/Sprites/Armes/Arcs/3.png");

			batons = new Image[3];
			batons[0]= StdDraw.getImage("img/Sprites/Armes/Batons/1.png");
			batons[1]= StdDraw.getImage("img/Sprites/Armes/Batons/2.png");
			batons[2]= StdDraw.getImage("img/Sprites/Armes/Batons/3.png");
			
			sort1 = new Image[8];
			sort1[0]= StdDraw.getImage("img/Sprites/Armes/Zap/Base/1.png");
			sort1[1]= StdDraw.getImage("img/Sprites/Armes/Zap/Base/2.png");
			sort1[2]= StdDraw.getImage("img/Sprites/Armes/Zap/Base/3.png");
			sort1[3]= StdDraw.getImage("img/Sprites/Armes/Zap/Base/4.png");
			sort1[4]= StdDraw.getImage("img/Sprites/Armes/Zap/Base_droite/1.png");
			sort1[5]= StdDraw.getImage("img/Sprites/Armes/Zap/Base_droite/2.png");
			sort1[6]= StdDraw.getImage("img/Sprites/Armes/Zap/Base_droite/3.png");
			sort1[7]= StdDraw.getImage("img/Sprites/Armes/Zap/Base_droite/4.png");

			armures = new Image[10];
			armures[0] = StdDraw.getImage("img/Sprites/Armures/peon.png");
			armures[1] = StdDraw.getImage("img/Sprites/Armures/CAC/1.png");
			armures[2] = StdDraw.getImage("img/Sprites/Armures/CAC/2.png");
			armures[3] = StdDraw.getImage("img/Sprites/Armures/CAC/3.png");
			armures[4] = StdDraw.getImage("img/Sprites/Armures/Dist/1.png");
			armures[5] = StdDraw.getImage("img/Sprites/Armures/Dist/2.png");
			armures[6] = StdDraw.getImage("img/Sprites/Armures/Dist/3.png");
			armures[7] = StdDraw.getImage("img/Sprites/Armures/Mage/1.png");
			armures[8] = StdDraw.getImage("img/Sprites/Armures/Mage/2.png");
			armures[9] = StdDraw.getImage("img/Sprites/Armures/Mage/3.png");
			
			bijoux= new Image[10];
			bijoux[0] = StdDraw.getImage("img/Sprites/Bijoux/chance.png");
			bijoux[1] = StdDraw.getImage("img/Sprites/Bijoux/defense.png");
			bijoux[2] = StdDraw.getImage("img/Sprites/Bijoux/degats.png");
			bijoux[3] = StdDraw.getImage("img/Sprites/Bijoux/mana.png");
			bijoux[4] = StdDraw.getImage("img/Sprites/Bijoux/recul.png");
			bijoux[5] = StdDraw.getImage("img/Sprites/Bijoux/vie.png");
			bijoux[6] = StdDraw.getImage("img/Sprites/Bijoux/vitesse.png");
			bijoux[7] = StdDraw.getImage("img/Sprites/Bijoux/vitesseAttaque.png");
			bijoux[8] = StdDraw.getImage("img/Sprites/Bijoux/Malediction.png");
			bijoux[9] = StdDraw.getImage("img/Sprites/Bijoux/gainXP.png");
			
			guiMarchand = StdDraw.getImage("img/Sprites/Marchand/Menu.png");
			acheter_sel = StdDraw.getImage("img/Sprites/Marchand/Acheter_sel.png");
			vendre_sel = StdDraw.getImage("img/Sprites/Marchand/Vendre_sel.png");
			
			flecheDroite = StdDraw.getImage("img/Sprites/Armes/Arcs/flecheDroite.png");
			flecheGauche = StdDraw.getImage("img/Sprites/Armes/Arcs/flecheGauche.png");
			
			emblemes = new Image[3];
			emblemes[0] = StdDraw.getImage("img/Sprites/Inventaire/Mago.png");
			emblemes[1] = StdDraw.getImage("img/Sprites/Inventaire/Dist.png");
			emblemes[2] = StdDraw.getImage("img/Sprites/Inventaire/Bast.png");
			
			selectMage = StdDraw.getImage("img/Sprites/Classe/selectMage.png");
			selectArcher = StdDraw.getImage("img/Sprites/Classe/selectArcher.png");
			selectBourrin = StdDraw.getImage("img/Sprites/Classe/selectBourrin.png");
			choixClasse = StdDraw.getImage("img/Sprites/Classe/choix.png");

		}
		catch(Exception e)
		{
			System.out.println("erreur lors de l'ouverture des images : " + e);
		}
	}
	
	public static void chargerMap(int quelleMap)
	{
		
		if(quelleMap==Maps.Map.Foret.val())
		{
			foret=new Image[7];
			foret[0] = StdDraw.getImage("img/Map/Foret/Start.png");
			foret[1] = StdDraw.getImage("img/Map/Foret/Forest1.png");
			foret[2] = StdDraw.getImage("img/Map/Foret/Forest2.png");
			foret[3] = StdDraw.getImage("img/Map/Foret/Forest3.png");
			foret[4] = StdDraw.getImage("img/Map/Foret/Forest4.png");
			foret[5] = StdDraw.getImage("img/Map/Foret/Forest5.png");
			foret[6] = StdDraw.getImage("img/Map/Foret/Bossfight.png");
			arbre1 = StdDraw.getImage("img/Map/Foret/Arbre1.png");
			chargerMonstre(Images.Entites.Lapin.val());

		}
		else if(quelleMap==Maps.Map.Grange.val())
		{
			grange=new Image[7];
			grange[0] = StdDraw.getImage("img/Map/Grange/1.png");
			grange[1] = StdDraw.getImage("img/Map/Grange/2.png");
			grange[2] = StdDraw.getImage("img/Map/Grange/3.png");
			grange[3] = StdDraw.getImage("img/Map/Grange/4.png");
			grange[4] = StdDraw.getImage("img/Map/Grange/5.png");
			grange[5] = StdDraw.getImage("img/Map/Grange/6.png");
			grange[6] = StdDraw.getImage("img/Map/Grange/7.png");
			chargerMonstre(Images.Entites.Paysan.val());
			chargerMonstre(Images.Entites.Poule.val());

		}
		else if(quelleMap==Maps.Map.Marais.val())
		{
			System.out.println("yo");
			marais=new Image[7];
			marais[0] = StdDraw.getImage("img/Map/Marais/1.png");
			marais[1] = StdDraw.getImage("img/Map/Marais/2.png");
			marais[2] = StdDraw.getImage("img/Map/Marais/3.png");
			marais[3] = StdDraw.getImage("img/Map/Marais/4.png");
			marais[4] = StdDraw.getImage("img/Map/Marais/5.png");
			marais[5] = StdDraw.getImage("img/Map/Marais/6.png");
			marais[6] = StdDraw.getImage("img/Map/Marais/7.png");
			chargerMonstre(Images.Entites.Trolls.val());

		}
		else if(quelleMap==Maps.Map.Chateau.val())
		{
			chateau=new Image[7];
			chateau[0] = StdDraw.getImage("img/Map/Chateau/1.png");
			chateau[1] = StdDraw.getImage("img/Map/Chateau/2.png");
			chateau[2] = StdDraw.getImage("img/Map/Chateau/3.png");
			chateau[3] = StdDraw.getImage("img/Map/Chateau/4.png");
			chateau[4] = StdDraw.getImage("img/Map/Chateau/5.png");
			chateau[5] = StdDraw.getImage("img/Map/Chateau/6.png");
			chateau[6] = StdDraw.getImage("img/Map/Chateau/7.png");
			chargerMonstre(Images.Entites.Trolls.val());
			chargerMonstre(Images.Entites.Paysan.val());
			chargerMonstre(Images.Entites.Lapin.val());
			chargerMonstre(Images.Entites.Dragon.val());
			chargerMonstre(Images.Entites.Poule.val());


		}
		else if(quelleMap==Maps.Map.Base.val())
		{
			dechargerMap(Maps.Map.Foret.val());
			dechargerMap(Maps.Map.Marais.val());
			dechargerMap(Maps.Map.Grange.val());
			
			base=new Image[4];
			base[0]  = StdDraw.getImage("img/Map/Base/Hub_botLeft.png");
			base[1]  = StdDraw.getImage("img/Map/Base/Hub_botRight.png");
			base[2]  = StdDraw.getImage("img/Map/Base/Hub_topLeft.png");
			base[3]  = StdDraw.getImage("img/Map/Base/Hub_topRight.png");
			bijoutier = StdDraw.getImage("img/Map/Base/Sprites/Bijoux.png");
			arbre2 = StdDraw.getImage("img/Map/Base/Sprites/Arbre.png");
			forgeron = StdDraw.getImage("img/Map/Base/Sprites/Forgeron.png");
			leVieux = StdDraw.getImage("img/Map/Base/Sprites/Vieux.png");
			moulin = StdDraw.getImage("img/Map/Base/Sprites/Moulin.png");
			fontaine = new Image[3];
			fontaine[0] = StdDraw.getImage("img/Map/Base/Sprites/Fontaine0.png");
			fontaine[1] = StdDraw.getImage("img/Map/Base/Sprites/Fontaine1.png");
			fontaine[2] = StdDraw.getImage("img/Map/Base/Sprites/Fontaine2.png");
			porte =  StdDraw.getImage("img/Map/Base/Sprites/Portouvairte.png");
			porteFermee =  StdDraw.getImage("img/Map/Base/Sprites/PortPaouvairte.png");
			porteBoss =  StdDraw.getImage("img/Map/Base/Sprites/Portboss.png");
			porteBossFermee =  StdDraw.getImage("img/Map/Base/Sprites/PortbossPaouvairte.png");
		}
	}
	
	public static void dechargerMap(int quelleMap)
	{
		if(quelleMap==Maps.Map.Foret.val())
		{
			foret=new Image[7];
			arbre1 = null;
			dechargerMonstre(Images.Entites.Lapin.val());

		}
		else if(quelleMap==Maps.Map.Grange.val())
		{
			grange=new Image[7];
			dechargerMonstre(Images.Entites.Paysan.val());
			dechargerMonstre(Images.Entites.Poule.val());
		}
		else if(quelleMap==Maps.Map.Marais.val())
		{
			marais=new Image[7];
			dechargerMonstre(Images.Entites.Trolls.val());

		}
		else if(quelleMap==Maps.Map.Chateau.val())
		{
			chateau=new Image[7];
			dechargerMonstre(Images.Entites.Trolls.val());
			dechargerMonstre(Images.Entites.Paysan.val());
			dechargerMonstre(Images.Entites.Lapin.val());
			dechargerMonstre(Images.Entites.Dragon.val());
			dechargerMonstre(Images.Entites.Poule.val());

		}
		else if(quelleMap==Maps.Map.Base.val())
		{
			base=new Image[4];
			bijoutier = null;
			arbre2 = null;
			forgeron = null;
			leVieux = null;
			moulin = null;
			fontaine = new Image[3];
		}
	}
	
	public static void chargerMonstre(int quelMonstre)
	{
		if(quelMonstre==Images.Entites.Lapin.val())
		{
			lapin = new Image[6];
			lapin[0] = StdDraw.getImage("img/Sprites/Ennemis/Lapin/Right/lapin0.png");
			lapin[1] = StdDraw.getImage("img/Sprites/Ennemis/Lapin/Right/lapin1.png");
			lapin[2] = StdDraw.getImage("img/Sprites/Ennemis/Lapin/Right/lapin2.png");
			lapin[3] = StdDraw.getImage("img/Sprites/Ennemis/Lapin/Left/lapin0.png");
			lapin[4] = StdDraw.getImage("img/Sprites/Ennemis/Lapin/Left/lapin1.png");
			lapin[5] = StdDraw.getImage("img/Sprites/Ennemis/Lapin/Left/lapin2.png");
		}
		else if(quelMonstre==Images.Entites.Poule.val())
		{
			poule = new Image[6];
			poule[0] = StdDraw.getImage("img/Sprites/Ennemis/Poule/Right1.png");
			poule[1] = StdDraw.getImage("img/Sprites/Ennemis/Poule/Right2.png");
			poule[2] = StdDraw.getImage("img/Sprites/Ennemis/Poule/Right3.png");
			poule[3] = StdDraw.getImage("img/Sprites/Ennemis/Poule/Left1.png");
			poule[4] = StdDraw.getImage("img/Sprites/Ennemis/Poule/Left2.png");
			poule[5] = StdDraw.getImage("img/Sprites/Ennemis/Poule/Left3.png");
		}
		else if(quelMonstre==Images.Entites.Paysan.val())
		{
			paysan = new Image[6];
			paysan[0] = StdDraw.getImage("img/Sprites/Ennemis/Farmer/Right1.png");
			paysan[1] = StdDraw.getImage("img/Sprites/Ennemis/Farmer/Right2.png");
			paysan[2] = StdDraw.getImage("img/Sprites/Ennemis/Farmer/Right3.png");
			paysan[3] = StdDraw.getImage("img/Sprites/Ennemis/Farmer/Left1.png");
			paysan[4] = StdDraw.getImage("img/Sprites/Ennemis/Farmer/Left2.png");
			paysan[5] = StdDraw.getImage("img/Sprites/Ennemis/Farmer/Left3.png");
		}
		else if(quelMonstre == Images.Entites.Trolls.val())
		{
			troll = new Image[6];
			troll[0] = StdDraw.getImage("img/Sprites/Ennemis/Ogre/Right/1.png");
			troll[1] = StdDraw.getImage("img/Sprites/Ennemis/Ogre/Right/2.png");
			troll[2] = StdDraw.getImage("img/Sprites/Ennemis/Ogre/Right/3.png");
			troll[3] = StdDraw.getImage("img/Sprites/Ennemis/Ogre/Left/1.png");
			troll[4] = StdDraw.getImage("img/Sprites/Ennemis/Ogre/Left/2.png");
			troll[5] = StdDraw.getImage("img/Sprites/Ennemis/Ogre/Left/3.png");
		}
		else if(quelMonstre==Images.Entites.Dragon.val())
		{
			dragon = new Image[24];
			dragon[0] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Right/1.png");
			dragon[1] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Right/2.png");
			dragon[2] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Right/3.png");
			dragon[3] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Right/4.png");
			dragon[4] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Right/5.png");
			
			dragon[5] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Left/1.png");
			dragon[6] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Left/2.png");
			dragon[7] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Left/3.png");
			dragon[8] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Left/4.png");
			dragon[9] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/Left/5.png");
			
			dragon[10] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack/1.png");
			dragon[11] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack/2.png");
			dragon[12] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack/3.png");
			dragon[13] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack/4.png");
			dragon[14] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack/5.png");
			dragon[15] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack/6.png");
			dragon[16] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack/7.png");
			
			dragon[17] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack(left)/1.png");
			dragon[18] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack(left)/2.png");
			dragon[19] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack(left)/3.png");
			dragon[20] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack(left)/4.png");
			dragon[21] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack(left)/5.png");
			dragon[22] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack(left)/6.png");
			dragon[23] = StdDraw.getImage("img/Sprites/Ennemis/Agrougrou/He_Attack(left)/7.png");
			
			
		}
	}
	
	public static void dechargerMonstre(int quelMonstre)
	{
		if(quelMonstre==Images.Entites.Lapin.val())
		{
			lapin = new Image[6];
		}
		else if(quelMonstre==Images.Entites.Poule.val())
		{
			poule = new Image[6];
		}
		else if(quelMonstre==Images.Entites.Paysan.val())
		{
			paysan = new Image[6];
		}
		else if(quelMonstre==Images.Entites.Trolls.val())
		{
			troll = new Image[6];
		}
		else if(quelMonstre==Images.Entites.Dragon.val())
		{
			dragon = new Image[24];
		}
	}
	
	public static Image getLeVieux()
	{
		return leVieux;
	}
	public static Image getLifePotion()
	{
		return lifePotion;
	}
	public static Image getManaPotion()
	{
		return manaPotion;
	}
	public static Image getMage()
	{
		return mage;
	}
	public static Image getBaston()
	{
		return baston;
	}
	public static Image getDistance()
	{
		return distance;
	}
	public static Image getSurbrillance()
	{
		return surbrillance;
	}
	public static Image getError()
	{
		return error;
	}
	public static Image getPerso0()
	{
		return perso0;
	}
	public static Image getPerso1()
	{
		return perso1;
	}
	public static Image getPerso2()
	{
		return perso2;
	}
	public static Image getPerso3()
	{
		return perso3;
	}
	public static Image getPerso4()
	{
		return perso4;
	}
	public static Image getPerso5()
	{
		return perso5;
	}
	public static Image getPersoFrappeDroite()
	{
		return persoFrappeDroite;
	}
	public static Image getPerso10()
	{
		return perso10;
	}
	public static Image getPerso11()
	{
		return perso11;
	}
	public static Image getPerso12()
	{
		return perso12;
	}
	public static Image getPerso13()
	{
		return perso13;
	}
	public static Image getPerso14()
	{
		return perso14;
	}
	public static Image getPerso15()
	{
		return perso15;
	}
	public static Image getPersoFrappeGauche()
	{
		return persoFrappeGauche;
	}
	public static Image[] getForet()
	{
		return foret;
	}
	public static Image[] getBase()
	{
		return base;
	}
	public static Image getArbre1()
	{
		return arbre1;
	}
	public static Image[] getPique()
	{
		return pique;
	}
	public static Image getFleche() 
	{
		return fleche;
	}
	public static Image getBombe()
	{
		return bombe;
	}
	public static Image getArbre2()
	{
		return arbre2;
	}
	public static Image getForgeron()
	{
		return forgeron;
	}
	public static Image getBijoutier()
	{
		return bijoutier;
	}
	public static Image[] getFontaine()
	{
		return fontaine;
	}
	public static Image getMoulin()
	{
		return moulin;
	}
	public static Image getVieux()
	{
		return leVieux;
	}

	public static Image getHelice()
	{
		return helice;
	}
	public static Image getInventaire()
	{
		return inventaire;
	}
	public static Image[] getExplosion()
	{
		return explosion;
	}
	public static Image[] getLapin()
	{
		return lapin;
	}
	public static Image getSang()
	{
		return sang;
	}
	public static Image getPilier()
	{
		return pilier;
	}
	public static Image getMoney()
	{
		return money;
	}
	public static Image getBar()
	{
		return bar;
	}
	public static Image[] getPoule()
	{
		return poule;
	}
	public static Image[] getEpees()
	{
		return epees;
	}

	public static Image[] getPiege()
	{
		return piege;
	}

	public static Image[] getPaysan()
	{
		return paysan;
	}

	public static Image getPorte()
	{
		return porte;
	}

	public static Image getPorteFermee()
	{
		return porteFermee;
	}

	public static Image getPorteBoss()
	{
		return porteBoss;
	}

	public static Image getPorteBossFermee()
	{
		return porteBossFermee;
	}

	public static Image[] getArcs()
	{
		return arcs;
	}

	public static Image[] getBatons()
	{
		return batons;
	}

	public static Image[] getSort1()
	{
		return sort1;
	}
	public static Image[] getGrange()
	{
		return grange;
	}
	public static Image[] getMarais()
	{
		return marais;
	}

	public static Image[] getArmures()
	{
		return armures;
	}
	public static Image[] getBijoux()
	{
		return bijoux;
	}
	public static Image getGuiMarchand()
	{
		return guiMarchand;
	}

	public static Image getAcheterSel()
	{
		return acheter_sel;
	}

	public static Image getVendreSel()
	{
		return vendre_sel;
	}
	public static Image[] getChateau()
	{
		return chateau;
	}

	public static Image[] getTroll()
	{
		return troll;
	}

	public static Image[] getDragon()
	{
		return dragon;
	}
	public static Image getClef()
	{
		return clef;
	}

	public static Image getFlecheDroite()
	{
		return flecheDroite;
	}

	public static Image getFlecheGauche()
	{
		return flecheGauche;
	}
	public static Image[] getEmblemes()
	{
		return emblemes;
	}

	public static Image getSelectArcher()
	{
		return selectArcher;
	}

	public static Image getSelectMage()
	{
		return selectMage;
	}

	public static Image getSelectBourrin()
	{
		return selectBourrin;
	}

	public static Image getChoixClasse()
	{
		return choixClasse;
	}
	
}
