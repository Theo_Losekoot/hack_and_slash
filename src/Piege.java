import java.util.List;
import java.util.stream.Collectors;

public class Piege extends Monstre
{
	private static double usualWidth=1;
	private static double usualHeight=1;
	private static int usualLifePoints=10;
	private static int usualAttack=70;
	private static int usualDefense=5;
	
	private boolean aDejaServi;
	/**
	 * Constructeur de Piege, plutot clair
	 * @param nom
	 * @param x
	 * @param y
	 * @param pDV
	 * @param pAtk
	 * @param pDef
	 * @param width
	 * @param height
	 */
	public Piege(String nom, double x, double y, int pDV, int pAtk, int pDef, double width, double height, int entite)
	{
		//Monstre : nom, positionX, positionY, points de vie, points d'ataque, 
		//points de defense,  vitesse, largeur, hauteur
		super(nom, x, y, pDV, pAtk, pDef, 0, width, height, entite, 0);
		aDejaServi=false;
		tempsAttaqueActuelle=5000;
		dureeAttaque = 1000;
	}

	@Override
	public void seDeplacer()
	{
		// un piege ne bouge pas. ehe
	}
	
	/**
	 * joue le tour d'un piege, donc verifie si il est en vie et le fait attaquer si le joueur marche dessus
	 */
	@Override
	public void jouer()
	{
		if(this.getPointsDeVie()<=0)
		{
			mourir();
			return;
		}
		
		updateImmunite();
		actualiserCoup();
		return;
	}
	/*
	public boolean joueurEstDessus()
	{
		return Hitbox.collision(this.getHB(), GameWorld.getJoueur().getHB());
	}
	*/
	
	/**
	 * lance l'animation d'attaque (met tempsAttaqueActuelle a 0), et teleporte le joueur au centre du piege, et utilise le piege
	 */
	@Override
	public void attaquer(EtreVivant a)
	{
		if(!frappeActuellement() && !aDejaServi)
		{
			updateVarAnim();
			a.prendreDegats(this.pointsDAttaque, a.position.getX(), a.position.getY(), 0);
			//pour mettre le perso au centre du piege, sauf si ca le bloque dans un mur.
			double x=a.getX();
			double y=a.getY();
			a.setPosition(new Position(this.getHB().getPosXM(), this.getHB().getPosYM()+this.getHeight()/2));
			boolean bloque=false;
			List<Entite> lesEntites = GameWorld.getEntites().values().stream().collect(Collectors.toList());
			for(int i=0; i<lesEntites.size(); i++)
			{
				Entite m = lesEntites.get(i);
				if(Hitbox.collision(a.getHB(),m.getHB()) && m != this && m!=GameWorld.getJoueur())
					bloque=true;
			}
			if(bloque)
				a.setPosition(new Position(x,y));

			aDejaServi=true;
			tempsAttaqueActuelle=0;
		}
	}
	
	@Override
	public void updateVarAnim()
	{
		numeroAnimation[0]++;
		//met a jour la taille de l'image pour pouvoir agrandir la pique
		this.setHeight(this.getHeight()*2);
		this.setPosition(new Position(this.getX(), this.getY()+this.getHeight()/4));
	}
	/**
	 * actualise le blocage du perso et l'animation, ainsi que la destruction du piege au bout d'un moment
	 */
	@Override 
	public void actualiserCoup()
	{
		if(frappeActuellement())
		{
			if(Hitbox.collision(GameWorld.getJoueur().getHB(), this.getHB()))
			{
				GameWorld.getJoueur().stopper();
			}
		}
		else if(aDejaServi)
		{
			//remet les position a la normale pour afficher la tache de sang au bon endroit
			this.setPosition(new Position(this.getX(), this.getY()-this.getHeight()/4));
			this.setHeight(this.getHeight()/2);	
			mourir();
		}
		tempsAttaqueActuelle+=GameWorld.TEMPS_RAFRAICHISSEMENT;
	}
	
	@Override
	public void dessine()
	{
		if(GameWorld.getDistanceJoueur(this.getX(), this.getY()) < GameWorld.getWidthGrille()/6)
		{
			StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
					(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
					Anim.bonneImage(this.quelleEntite, numeroAnimation),
					(double)this.getWidth()/ GameWorld.getWidthGrille(),
					(double)this.getHeight()/GameWorld.getHeightGrille(),
					0);
		}
	}
	
	
	public static double getUsualWidth()
	{
		return usualWidth;
	}
	public static double getUsualHeight()
	{
		return usualHeight;
	}

	public static int getUsualLifePoints()
	{
		return usualLifePoints;
	}

	public static int getUsualAttack()
	{
		return usualAttack;
	}

	public static int getUsualDefense()
	{
		return usualDefense;
	}

}
