
public abstract class Chasseur extends Monstre
{
	protected boolean estUnBoss;
	public Chasseur(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse,
			double width, double height, int entite, double recul, boolean _estUnBoss)
	{
		//Monstre : nom, positionX, positionY, points de vie, points d'attaque
		//points de defense, vitesse, largeur, hauteur
		super(nom,x,y,pDV,pAtk,pDef,vitesse,width,height, entite, recul);
		estUnBoss=_estUnBoss;
	}
	
	/**
	 * Quad le monstre meurt, verifie si c'est un boss et si c'en est bien un, drop la cle 
	 * pour le monde suivant et cree un portail de retour au hub
	 */
	@Override
	public void mourir()
	{
		if(estUnBoss)
		{
			PorteMagique laPorteHub;
			//ajoute la porte a un endroit ou elle ne teleporte pas instantannement le joueur
				laPorteHub = new PorteMagique("PortaiVersHub", GameWorld.getWidthGrille()*4,GameWorld.getHeightGrille()/5, 2, 4, false, Maps.Map.Base.val(), false);
				if(Hitbox.collision(laPorteHub.getHB(), GameWorld.getJoueur().getHB()))
					laPorteHub = new PorteMagique("PortaiVersHub", GameWorld.getWidthGrille()*4,6*GameWorld.getHeightGrille()/10, 2, 4, false, Maps.Map.Base.val(), false);

			//ajoute la porte aux entites
			GameWorld.addEntite(laPorteHub);
		}
		super.mourir();
		EntiteTimer sang = new EntiteTimer("tache", this.getX(), this.getY(), this.getWidth()*2, this.getHeight()*2, false, 1000*30,Images.Entites.Sang.val());
		sang.getHB().setPosYM(GameWorld.getHeightGrille());
		Anim.addEntiteDisp(sang);
	}
	
	/**
	 * Implemente la methode jouer, qui est appelee a chaque tour de monstre.
	 * appele les differentes fonctions pour faire vivre le monsre, comme l'immunite, les coups le deplacement
	 * et gerer la mort du monstre
	 */
	@Override
	public void jouer()
	{
		if(this.getPointsDeVie()<=0)
		{
			mourir();
			return;
		}
		updateImmunite();
		actualiserCoup();
		seDeplacer();
		return;
	}
	
	/**
	 * inflige directement les degats a un etreVivant.
	 */
	public void attaquer(EtreVivant a)
	{
		a.prendreDegats(this.pointsDAttaque, position.getX(), position.getY(), this.getRecul());
	}
	
	// methode permettant l'application du vecteur au monstre et la modification
	//de son orientation, ainsi que la mise a jour du vecteur.
	public void seDeplacer()
	{
		augmenterVecteur();
		avancer();
		vecteurAvancer.ralentir();
	}
	/**
	 * methode a implementer dans chacune des sous-classes, c'est l'IA des monstres.
	 */
	public abstract void augmenterVecteur();
	
	/**
	 * utilise le vecteurAvancer du monstre pour le faire avance,r tout en gerant les collisions.
	 */
	protected void avancer()
	{
			position.setX((position.getX()+vecteurAvancer.getX()));
			position.setY((position.getY()+vecteurAvancer.getY()));
			updateHB();
			if(!collisionPourMonstres())
				return;
			//si collision, essaie de dectecter quelle coordonnee fait
			//une collision pour l'enlever, sinon annule le move
			else 
			{
				//enleve le vecteur x
				position.setX((position.getX()-vecteurAvancer.getX()));
				updateHB();
				//si encore collision, remet le vecteur x et enleve le vecteur y
				if(collisionPourMonstres())
				{
					position.setX((position.getX()+vecteurAvancer.getX()));
					position.setY((position.getY()-vecteurAvancer.getY()));
					updateHB();
				}
				//si encore collision, re-enleve le vecteur x
				if(collisionPourMonstres())
				{
					position.setX((position.getX()-vecteurAvancer.getX()));
					updateHB();
				}
			}	
	}
	

	public void dessine()
	{
		StdDraw.picture( (double)((position.getX()) / GameWorld.getWidthGrille()) -GameWorld.getDecalageXAffichage(),
				(double)((position.getY()) / GameWorld.getHeightGrille()) -GameWorld.getDecalageYAffichage(),
				Anim.bonneImage(this.quelleEntite, this.numeroAnimation, this.vecteurAvancer, this.droite, this.frappeActuellement()),
				(double)this.getWidth()/ GameWorld.getWidthGrille(),
				(double)this.getHeight()/GameWorld.getHeightGrille(),
				0); 
		updateVarAnim();
		//dessine une barre de vie
	}
	
}
