
public abstract class Consommable extends Item
{
	//le numeroInstance sert a ne pas instancier deux objets avec le meme nom
	private static int numeroInstance=0;
	int nombre;

	/**
	 * Constructeur de la classe Consommable
	 * @param nom
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param isObstacle
	 * @param nb : le nombre d'objets de ce type
	 * @param entite : l'entite, pour l'affichage
	 * @param val : prix
	 */
	public Consommable(String nom, double x, double y, double width, double height, boolean isObstacle, int nb, int entite, int val)
	{
		//Item : nom, positio,X, positionY, largeur, hauteur, c'est un obstacle ?
		//quelle entite c'est ?(potion?)
		super((nom + String.valueOf(numeroInstance)), x, y, width, height, isObstacle, entite, val);
		nombre=nb;
		numeroInstance++;
	}

	/**
	 * utilise une potion
	 * @return true si il en reste, sinon false
	 */
	public boolean utiliserUnite()
	{
		if(nombre>0)
		{
			nombre--;
			return true;
		}
		return false;
	}

	
	public int getNombre()
	{
		return nombre;
	}
	
	public void ajouterStock(int nb)
	{
		nombre+=nb;
	}

	/**
	 * set le nouveau nombre, si il est positif
	 * @param nb le nouveau nombre
	 */
	public void setNombre(int nb)
	{
		if(nb<0)
			nb=0;
		this.nombre=nb;
	}
	public abstract void utiliser();
	
}
