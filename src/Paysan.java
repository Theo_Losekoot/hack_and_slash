
public class Paysan extends Chasseur
{
	
	private static double usualWidth=1.5;
	private static double usualHeight=1.5;
	private static int usualLifePoints=100;
	private static int usualAttack=40;
	private static int usualDefense=20;
	private static double usualSpeed=0.02;
	private static double usualKnockback=0.5;
	
	
	public Paysan(String nom, double x, double y, int pDV, int pAtk, int pDef, double vitesse, double width, double height, double _recul)
	{
		super(nom,x,y,pDV,pAtk,pDef,vitesse,width,height, Images.Entites.Paysan.val(), _recul, false);
	}
	public void attaquer(EtreVivant a)
	{
		a.prendreDegats(this.pointsDAttaque, position.getX(), position.getY(), this.getRecul());
	}
	

	
	
	
	/**
	 * lance l'attaque (si il y en a une ? on sait pas encore)
	 */
	public void attaquer()
	{
		// vecteurAvancer.setX(0);
		// vecteurAvancer.setY(0);
		if (!frappeActuellement())
		{
			tempsAttaqueActuelle = 0;
		}
	}

	/**
	 * bha du coup pour l'instant c'est vide
	 */
	@Override
	public void actualiserCoup()
	{

	}
	
	@Override
	public void augmenterVecteur()
	{
		if(GameWorld.getDistanceJoueur(this.getX(), this.getY()) < 2*GameWorld.getWidthGrille()/3)
		{	
			Vector newNorm = new Vector(this.getVitesse());
			newNorm.setX(GameWorld.getJoueur().getX()-this.getX());
			newNorm.setY(GameWorld.getJoueur().getY()-this.getY());
			newNorm = newNorm.normalised();
			vecteurAvancer.setX(vecteurAvancer.getX()+newNorm.getX()*this.getVitesse());
			vecteurAvancer.setY(vecteurAvancer.getY()+newNorm.getY()*this.getVitesse());
		}
	}
	
	public static double getUsualWidth()
	{
		return usualWidth;
	}
	public static double getUsualHeight()
	{
		return usualHeight;
	}

	public static int getUsualLifePoints()
	{
		return usualLifePoints;
	}

	public static int getUsualAttack()
	{
		return usualAttack;
	}

	public static int getUsualDefense()
	{
		return usualDefense;
	}

	public static double getUsualSpeed()
	{
		return usualSpeed;
	}
	public static double getUsualKnockback()
	{
		return usualKnockback;
	}
	


	@Override
	public void mourir()
	{
		GameWorld.getJoueur().addXp(10);
		super.mourir();
	}
	
}
